<?php
function waterMark($original, $watermark, $placement = 'bottom=5,right=5', $destination = null){
   $original = urldecode($original);
   $info_o = @getImageSize($original);
   if (!$info_o) return false;
   $info_w = @getImageSize($watermark);
   if (!$info_w) return false;

   list ($vertical, $horizontal) = preg_split('/\,/', $placement,2);
   list($vertical, $sy) = preg_split('/\=/', trim($vertical),2);
   list($horizontal, $sx) = preg_split('/\=/', trim($horizontal),2);

   switch (trim($vertical)){
      case 'bottom':
         $y = $info_o[1] - $info_w[1] - (int)$sy;
         break;
      case 'middle':
         $y = ceil($info_o[1]/2) - ceil($info_w[1]/2) + (int)$sy;
         break;
      default:
         $y = (int)$sy;
         break;
      }

   switch (trim($horizontal)){
      case 'right':
         $x = $info_o[0] - $info_w[0] - (int)$sx;
         break;
      case 'center':
         $x = ceil($info_o[0]/2) - ceil($info_w[0]/2) + (int)$sx;
         break;
      default:
         $x = (int)$sx;
         break;
      }

   header("Content-Type: ".$info_o['mime']);

   $original = @imageCreateFromString(file_get_contents($original));
   $watermark = @imageCreateFromString(file_get_contents($watermark));
   $out = imageCreateTrueColor($info_o[0],$info_o[1]);

   imageCopy($out, $original, 0, 0, 0, 0, $info_o[0], $info_o[1]);

//��� ������ ������ ����������� � ������� ����� ��������� Watermark
// $info_o[0] > 250 - ������ ����������� ������ ���� ������ 250 px
// $info_o[1] > 250 - ������ ����������� ������ ���� ������ 250 px

   if( ($info_o[0] > 20) && ($info_o[1] > 20) ){
   imageCopy($out, $watermark, $x, $y, 0, 0, $info_w[0], $info_w[1]);
   }

   switch ($info_o[2]){
      case 1:
         imageGIF($out);
         break;
      case 2:
         imageJPEG($out);
         break;
      case 3:
         imagePNG($out);
         break;
         }

   imageDestroy($out);
   imageDestroy($original);
   imageDestroy($watermark);

   return true;
   }

 if ($_GET['pers'] != ""){
 $img = "main/persona/".$_GET['pers'];
 $mark = "watermark_b.png";
 $raz = "bottom=5,right=5";

 }elseif ($_GET['perst'] != ""){
 $img = "main/persona/thumbs/".$_GET['perst'];
 $mark = "watermark_s.png";
 $raz = "bottom=1,right=1";

 }elseif ($_GET['pers250'] != ""){
 $img = "main/persona/250/".$_GET['pers250'];
 $mark = "watermark_b.png";
 $raz = "bottom=5,right=5";

 }elseif ($_GET['eskiz'] != ""){
 $img = "main/eskiz/".$_GET['eskiz'];
 $mark = "watermark_b.png";
 $raz = "bottom=5,right=5";

 }elseif ($_GET['eskizt'] != ""){
 $img = "main/eskiz/thumbs/".$_GET['eskizt'];
 $mark = "watermark_s.png";
 $raz = "bottom=1,right=1";

 }elseif ($_GET['eskiz250'] != ""){
 $img = "main/eskiz/250/".$_GET['eskiz250'];
 $mark = "watermark_b.png";
 $raz = "bottom=5,right=5";

 }elseif ($_GET['poster'] != ""){
 $img = "main/poster/".$_GET['poster'];
 $mark = "watermark_b.png";
 $raz = "bottom=5,right=5";

 }elseif ($_GET['postert'] != ""){
 $img = "main/poster/thumbs/".$_GET['postert'];
 $mark = "watermark_b.png";
 $raz = "bottom=5,right=5";

 }elseif ($_GET['persph'] != ""){
 $img = "main/personaphoto/".$_GET['persph'];
 $mark = "watermark_b.png";
 $raz = "bottom=5,right=5";

 }elseif ($_GET['perspht'] != ""){
 $img = "main/personaphoto/thumbs/".$_GET['perspht'];
 $mark = "watermark_b.png";
 $raz = "bottom=5,right=5";
 }
waterMark($img, $mark, $raz);
?>