<div id="osgraph" style="border:green 1px solid; border-radius:10px;">Loading...</div>

<?php
$link = mysql_connect("localhost", "root", "");
mysql_select_db("megamagnet", $link);

$users = mysql_num_rows(mysql_query("SELECT * FROM tbl_user where OS!=''", $link));

function oscount($os){
	global $link;
	$result = mysql_query("SELECT * FROM tbl_user where OS LIKE '%".$os."%'", $link);
	echo mysql_num_rows($result);
}

function ospercent($os){
	global $link,$users;
	$result = mysql_query("SELECT * FROM tbl_user where OS LIKE '%".$os."%'", $link);
	echo round(mysql_num_rows($result)/$users*100,1);
}

?>

<script type="text/javascript">

	var osChart = new JSChart('osgraph', 'pie');
	osChart.setDataArray([['A', <?php ospercent('Windows XP');?>],['B', <?php ospercent('Windows 7');?>],['C', <?php ospercent('Windows Vista');?>],['D', <?php ospercent('Mac OS X');?>],['E', <?php ospercent('Linux');?>],['F', <?php ospercent('Ubuntu');?>]]);
	osChart.colorize(['#99CDFB','#3366FB','#0000FA','#F8CC00','#F89900','#F76600']);
	osChart.setSize(450, 250);
	osChart.setTitle('������������ �������');
	osChart.setTitleFontFamily('Tahoma');
	osChart.setTitleFontSize(14);
	osChart.setTitleColor('#0F0F0F');
	osChart.setPieRadius(100);
	osChart.setPieValuesColor('#FFFFFF');
	osChart.setPieValuesFontSize(10);
	osChart.setPiePosition(120, 120);
	osChart.setShowXValues(false);
	osChart.setLegend('#99CDFB', 'Windows XP (<?php oscount("Windows XP");?>)');
	osChart.setLegend('#3366FB', 'Windows 7 (<?php oscount("Windows 7");?>)');
	osChart.setLegend('#0000FA', 'Windows Vista (<?php oscount("Windows Vista");?>)');
	osChart.setLegend('#F8CC00', 'Mac OS X (<?php oscount("Mac OS X");?>)');
	osChart.setLegend('#F89900', 'Linux (<?php oscount("Linux");?>)');
	osChart.setLegend('#F76600', 'Ubuntu (<?php oscount("Ubuntu");?>)');
	osChart.setLegendShow(true);
	osChart.setLegendFontFamily('Tahoma');
	osChart.setLegendFontSize(10);
	osChart.setLegendPosition(300, 80);
	osChart.setPieAngle(50);
	osChart.set3D(true);
	osChart.draw();

</script>

<?php
mysql_close($link);
?>