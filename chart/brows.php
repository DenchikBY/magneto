<div id="browsgraph" style="float:left; border:green 1px solid; border-radius:10px;">Loading...</div>

<?php
$link = mysql_connect("localhost", "root", "");
mysql_select_db("megamagnet", $link);

$users = mysql_num_rows(mysql_query("SELECT * FROM tbl_user where BROWS!=''", $link));

function browscount($brows){
	global $link;
	$result = mysql_query("SELECT * FROM tbl_user where BROWS LIKE '%".$brows."%'", $link);
	echo mysql_num_rows($result);
}

function browspercent($brows){
	global $link,$users;
	$result = mysql_query("SELECT * FROM tbl_user where BROWS LIKE '%".$brows."%'", $link);
	echo round(mysql_num_rows($result)/$users*100,1);
}

?>

<script type="text/javascript">

	var browsChart = new JSChart('browsgraph', 'pie');
	browsChart.setDataArray([['A', <?php browspercent('opera');?>],['B', <?php browspercent('Mozilla Firefox');?>],['C', <?php browspercent('Internet Explorer');?>],['D', <?php browspercent('Chrome');?>],['E', <?php browspercent('Safari');?>],['F', <?php browspercent('Netscape');?>]]);
	browsChart.colorize(['#99CDFB','#3366FB','#0000FA','#F8CC00','#F89900','#F76600']);
	browsChart.setSize(450, 250);
	browsChart.setTitle('��������');
	browsChart.setTitleFontFamily('Tahoma');
	browsChart.setTitleFontSize(14);
	browsChart.setTitleColor('#0F0F0F');
	browsChart.setPieRadius(100);
	browsChart.setPieValuesColor('#FFFFFF');
	browsChart.setPieValuesFontSize(10);
	browsChart.setPiePosition(120, 120);
	browsChart.setShowXValues(false);
	browsChart.setLegend('#99CDFB', 'Opera (<?php browscount("Opera");?>)');
	browsChart.setLegend('#3366FB', 'Mozilla Firefox (<?php browscount("Mozilla Firefox");?>)');
	browsChart.setLegend('#0000FA', 'Internet Explorer (<?php browscount("Internet Explorer");?>)');
	browsChart.setLegend('#F8CC00', 'Google Chrome (<?php browscount("Chrome");?>)');
	browsChart.setLegend('#F89900', 'Safari (<?php browscount("Safari");?>)');
	browsChart.setLegend('#F76600', 'Netscape (<?php browscount("Netscape");?>)');
	browsChart.setLegendShow(true);
	browsChart.setLegendFontFamily('Tahoma');
	browsChart.setLegendFontSize(10);
	browsChart.setLegendPosition(300, 80);
	browsChart.setPieAngle(50);
	browsChart.set3D(true);
	browsChart.draw();

</script>

<?php
mysql_close($link);
?>