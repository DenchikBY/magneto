<?php
/*
WIRTEL*(:)* 2008
�� ������� ������ ���������� ������ � ������ �� ����� ����� � XML-������ ������� Gismeteo.ru. ������ �����������, ��� ���������� ��������, 4 ���� � ����� (2.30, 8.30, 14.30, 20.30 ��� �� ������� �������). ��������� ��������� ������ � ������, ����� �������� ����� �������. ����������� �������� ��������� �������, ���������� ������ ������� ID ������(��������:03161_1.xml).
*/
class Weather {
	var $city;
	var $weather;
	var $encode = 'windows-1251'; // ��������� � ������� ����� ���������� ������
	var $patchimg = 'main/color/images/weather/'; //����� � ���������. �� ������ �� ������� �������� ���������� � ����������, ����� ������ ������ ����� �������� ���������, �� ����� ���������� �������� � ������ ������ ��� ����� ����� �������� ������ $cloudiness � $precipitation, � ������ �� ������ �������� ������ ��� ������ (��������:<img src=".$this->patchimg."0".(($n=='0' || $n=='4')?'_night':'').".gif,, �������� �� <img src=".$this->patchimg."Sunny.gif)
	var $patchcache = 'cache/cache_weather/'; //���������� ��� �������� ���-������, ������ ������� ������ ���� � ���������� (��������: ��� Windows 'X:/cache/wth/' ��� *Unix '/usr/www/site/cache/wth/')
	//������������ ��� ������������� ����, �.�. ���������� ���������� ���������, ��� �������� �� �������� ��� ���-������� � ������� �� ��������,  � ����� �� ������� ������ �������, �.�. �������� ����� ��������� � ����� �����������. ������������ ���� ����� �� �������������.
	function Connect($url, $encode){
		$url = 'http://informer.gismeteo.ru/xml/'.$url; //���������� URL
		$content = @file_get_contents($url); //������� �������� ������� � PHP 5.x, �������� �������� �� ��������� ��������� ��������� allow_url_fopen � php.ini ��������� � �������� � ����� .htaccess �� ���������
		$content = str_replace(array(''.chr(13).'',''.chr(10).'',''.chr(9).'','\n','\r','/',' '),array(''),$content); //�������� ��� �������� �� ������, �������.
		
		$this->city=$this->pCity($content); //����� ���������� $city �������� ������
		$this->weather=$this->Parser($content); //����� ���������� $weather ������� ������
	}
	//������������ � �������������� ����, �.�. ������ ����� ����������� � ����. ��� ����� ������� �������. 
	function ConnectCache($url, $expire =7200){
		$mtime = 0;
		$file_cacheid = $this->patchcache . md5($url); //������ ��������� � ����������� � ������������ ������
		if($file_cacheid) $s=true; else $s=false; // ��������� ��������� �� ����������
        if(!file_exists($file_cacheid)) $s=false; //��������� ���� �� ��� �������� ���-���� 
        if(!($mtime = @filemtime($file_cacheid))) $s=false; //���� ��� ���������� ������� ��������/��������� ������ �� ���������� false
        if(($mtime + $expire) < time()){ //��������� ������� �� ����, ����������� � ����������� �� ������������ $expire �� ��������� ��� ����� 7200, �.�. ������ ����� ����������� ������ 2 ����
            @unlink($file_cacheid); //���� ������� ������� ���.
            $s=false;
        }
        else {
           $s=true;
        }
		if ((!$s)) { //���� ������� ����, �� ������ �����
			if(file_exists($file_cacheid))
				@unlink($file_cacheid); //������� ���, ���� �� �����
			$url = 'http://informer.gismeteo.ru/xml/'.$url; //���������� URL
			$content = file_get_contents($url);
			$content = str_replace(array(''.chr(11).'',''.chr(13).'',''.chr(10).'',''.chr(9).'','\n','\r','/',' '),array(''),$content);
			$this->city=$this->pCity($content);
			$this->weather=$this->Parser($content);
			if($fp = @fopen($file_cacheid, 'w')) {
                fwrite($fp, ''.$this->city.'|'.$this->weather.'|'); //���������� � ���� � ���� ����� �����|������|, ���� �������� �������� �������� � ������ ��������, ������� ������ ������ �� �����.
                fclose($fp); 
            }
            else {
                die('Unable to write cache.');
            }

		} else {
			$fp = @fopen($file_cacheid, 'r'); //��������� ���-���� 
			$p = explode('|',fread($fp, filesize($file_cacheid))); //C����� ������� �� ������ |, ������ ����
            fclose($fp);
			
			if ($this->encode !== 'windows-1251'){// ���� ��������� �� windows-1251 � ��������� ���������������
			$this->city = iconv ("windows-1251", $this->encode, $p[0]); //����� ���������� $city �������� ������ �� ���-����� � ������ ����������� ���������
			$this->weather = iconv ("windows-1251", $this->encode, $p[1]); //����� ���������� $weather ������ ������ �� ���-����� � ������ ����������� ���������
		}
				else{// ���� ��������� windows-1251
			$this->city = $p[0]; //����� ���������� $city �������� ������ �� ���-�����
			$this->weather = $p[1]; //����� ���������� $weather ������ ������ �� ���-�����
		}
		
		
		
		}
	}
	//������ �������� ������
	function pCity($content){
		$str = "<TOWNindex=\"(.*)\"sname=\"(.*)\"latitude=\"(.*)\"longitude=\"(.*)\">";
		if (eregi($str,$content,$out)){
			return urldecode($out[2]);
		} else return 'Unknow';
	}
	//������ ������
	function Parser($content){
		if ($content){
			$str='<FORECASTday="([0-9]{1,2})"month="([0-9]{1,2})"year="([0-9]{4})"hour="([0-9]{1,2})"tod="([0-9]{1})"predict="([0-9]{1,3})"weekday="([0-9]{1})"><PHENOMENAcloudiness="([0-3])"precipitation="([0-9]{1,2})"rpower="([0-1])"spower="([0-1])"><PRESSUREmax="([0-9]{1,3})"min="([0-9]{1,3})"><TEMPERATUREmax="([-,0-9]{1,3})"min="([-,0-9]{1,3})"><WINDmin="([0-9]{1,3})"max="([0-9]{1,3})"direction="([0-9]{1})"><RELWETmax="([0-9]{1,3})"min="([0-9]{1,3})"><HEATmin="([-,0-9]{1,3})"max="([-,0-9]{1,3})"><FORECAST>'.
			'<FORECASTday="([0-9]{1,2})"month="([0-9]{1,2})"year="([0-9]{4})"hour="([0-9]{1,2})"tod="([0-9]{1})"predict="([0-9]{1,3})"weekday="([0-9]{1})"><PHENOMENAcloudiness="([0-3])"precipitation="([0-9]{1,2})"rpower="([0-1])"spower="([0-1])"><PRESSUREmax="([0-9]{1,3})"min="([0-9]{1,3})"><TEMPERATUREmax="([-,0-9]{1,3})"min="([-,0-9]{1,3})"><WINDmin="([0-9]{1,3})"max="([0-9]{1,3})"direction="([0-9]{1})"><RELWETmax="([0-9]{1,3})"min="([0-9]{1,3})"><HEATmin="([-,0-9]{1,3})"max="([-,0-9]{1,3})"><FORECAST>'.
			'<FORECASTday="([0-9]{1,2})"month="([0-9]{1,2})"year="([0-9]{4})"hour="([0-9]{1,2})"tod="([0-9]{1})"predict="([0-9]{1,3})"weekday="([0-9]{1})"><PHENOMENAcloudiness="([0-3])"precipitation="([0-9]{1,2})"rpower="([0-1])"spower="([0-1])"><PRESSUREmax="([0-9]{1,3})"min="([0-9]{1,3})"><TEMPERATUREmax="([-,0-9]{1,3})"min="([-,0-9]{1,3})"><WINDmin="([0-9]{1,3})"max="([0-9]{1,3})"direction="([0-9]{1})"><RELWETmax="([0-9]{1,3})"min="([0-9]{1,3})"><HEATmin="([-,0-9]{1,3})"max="([-,0-9]{1,3})"><FORECAST>';
			if (eregi($str,$content,$out)){
				return $this->arr($out);
			} else return ' ������ � ������ ����� gismeteo.ru! ';
		} else return '������! ���������� � '.$url.'';
	}
	//����� �������� ��� ������� ���� �������

	function imgcloud($n){
		$n=ceil($n);
		$cloudiness=array(
		'0' => "<img height=30 width=30 border=0 src=".$this->patchimg."0".(($n=='0' || $n=='4')?'_night':'').".gif style=\"float:left;margin-top:5px;\" alt=\"����\" title=\"����\">", //��� ������ ����, ���� � �������� ����� ".$this->patchimg." , ���� ���������� ������� ����. ������ �������� �������� ".(($n=='0' || $n=='4')?'_night':'')." ��������� ��� ��� � �������� ��������� _night, �.�. �������� ������������� ������ � ������ ����� �����.
		'1' => "<img height=30 width=30 border=0 src=".$this->patchimg."1".(($n=='0' || $n=='4')?'_night':'').".gif style=\"float:left;margin-top:7px;\" alt=\"�����������\" title=\"�����������\">", //��� ������ �����������, ����� �� ��������, ��� ������ ����������  � ���� title=\" ����������� \", ���� �� �� ������� ������������ �������� ������ ������� ��� ��� ������� <img src=".$this->patchimg."1".(($n=='0' || $n=='4')?'_night':'').".gif style=\"float:left;margin-top:7px;\" alt=\"�����������\" title=\"�����������\"> �������� ������ ���� ����� �����������.
		'2' => "<img height=30 width=30 border=0 src=".$this->patchimg."2".(($n=='0' || $n=='4')?'_night':'').".gif style=\"float:left;margin-top:7px;\" alt=\"�������\" title=\"�������\">",
		'3' => "<img height=30 width=30 border=0 src=".$this->patchimg."3.gif style=\"float:left;margin-top:5px;\" alt=\"��������\" title=\"��������\">");
		
		return $cloudiness;
	}
	//����� �������� ��� ������� ���� �������
	function imgprecip($n){
		$n=ceil($n);
		$precipitation=array(
		'4' => "<img height=30 width=30 border=0 src=".$this->patchimg."4.gif style=\"float:left;margin-top:5px;\" alt=\"�����\" title=\"�����\">" ,
		'5' => "<img height=30 width=30 border=0 src=".$this->patchimg."5.gif style=\"float:left;margin-top:5px;\" alt=\"������\" title=\"������\">",
		'6' => "<img height=30 width=30 border=0 src=".$this->patchimg."6.gif style=\"float:left;margin-top:5px;\" alt=\"����\" title=\"����\">",
		'7' => "<img height=30 width=30 border=0 src=".$this->patchimg."6.gif style=\"float:left;margin-top:5px;\" alt=\"����\" title=\"����\">",
		'8' => "<img height=30 width=30 border=0 src=".$this->patchimg."8.gif style=\"float:left;margin-top:5px;\" alt=\"�����\" title=\"�����\">",
		'9' => "<img height=30 width=30 border=0 src=".$this->patchimg."9".(($n=='0' || $n=='4')?'_night':'').".gif style=\"float:left;margin-top:5px;\" alt=\"��� ������\" title=\"��� ������\">",
		'10' => "<img height=30 width=30 border=0 src=".$this->patchimg."10".(($n=='0' || $n=='4')?'_night':'').".gif style=\"float:left;margin-top:5px;\" alt=\"����\" title=\"����\">");
		
		return $precipitation;
	}
	
	function arr($out){
		$month_array = array( '1' => '������' , '2' => '�������' , '3' => '�����' , '4' => '������' , '5' => '���' , '6' => '����' , '7' => '����' ,
		'8' => '�������' , '9' => '��������' , '10' => '�������' , '11' => '������' , '12' => '�������' );
		$tod = array( '0' => '�����' , '1' => '�����' , '2' => '����' , '3' => '�������' , '4' => '�����' );
		$rpower = array( '0' =>' �������� �����/����' , '1' => ' �����/����' );
		$spower = array( '0' =>' �������� �����', '1' => ' �����' );
		$direction = array( '0' => '��������' , '1' => '������-���������' , '2' => '���������' , '3' => '���-���������' , '4' => '�����' , '5' => '���-��������' ,
		'6' => '��������' , '7' => '������-��������' );
		$direction = array( '0' => '��������' , '1' => '������-���������' , '2' => '���������' , '3' => '���-���������' , '4' => '�����' , '5' => '���-��������' ,
		'6' => '��������' , '7' => '������-��������' );
		$weekday = array( '1' => '�����������' , '2' => '�����������' , '3' => '�������' , '4' => '�����' , '5' => '�������' , '6' => '�������' , '7' => '�������' );
		
		$day1 = $out[1]; //���� ����� DD
		$month1 = $month_array[ceil($out[2])]; //���� ����� �������� ������, ������ ��������� � $month_array ����
		$tod1 = $tod[ceil($out[5])]; //����� ����� � ���� ����, ����, ����,  �����, ����, ������ ��������� � $tod  ����
		$weekday1 = $weekday[$out[7]]; // �������� ��� ������ �����������, ����������� .. �������,  ������ ��������� � $weekday ����
				
		$rpower1 = $rpower[$out[10]]; //��� �������, ������ ��������� � $rpower
		$spower1 = $spower[$out[11]]; //��� �������, ������ ��������� � $spower
		if (($out[8] < 4) and ($out[8] >= 0) and ($out[9]=='10')) {
			$cloudiness=$this->imgcloud($out[5]); //����� ������ �������� ������ ��������� ������� imgcloud()
			$cloudiness1 = $cloudiness[$out[8]]; //����� ��������
		} else {
			$precipitation=$this->imgprecip($out[5]); //����� ������ �������� ������ ��������� ������� imgprecip()
			$precipitation1 = $precipitation[$out[9]]; //����� ��������
		}
				
		$pressureMAX1 = $out[12]; //����������� ����������� ��������, � ��.��.��.
		$pressureMIN1 = $out[13]; //������������ ����������� ��������, � ��.��.��.
		$tempMIN1 = $out[15]; //����������� ����������� �������, � �������� �������
		$tempMAX1 = $out[14]; //������������ ����������� �������, � �������� �������
		$windMIN1 = $out[16]; //����������� �������� ������� �������� �����, ��� �������
		$windMAX1 = $out[17]; //������������ �������� ������� �������� �����, ��� �������
		$direction1 = $direction[$out[18]]; //����������� ����� � ������, 0 - ��������, 1 - ������-���������,  � �.�., ������ ��������� � $direction ����
		$relwenMIN1 = $out[20]; //����������� ������������� ��������� �������, � %
		$relwenMAX1 = $out[19]; //������������ ������������� ��������� �������, � %
		$heatMIN1 = $out[21]; //����������� ������� - ����������� ������� �� �������� ������� �� ������ ��������, ���������� �� �����
		$heatMAX1 = $out[22]; //������������ ������� - ����������� ������� �� �������� ������� �� ������ ��������, ���������� �� �����
		//���� �� �������� ������� ���������� ������� ��������� �����, �.�. ����� �������� 3 ��������� �� ����� �����, ��������  ���� ������� ���� ������� � ���� �������.
		$day2 = $out[23];
		$month2 = $month_array[ceil($out[24])];
		$tod2 = $tod[ceil($out[27])];
		$weekday2 = $weekday[$out[29]];
				
		$rpower2 = $rpower[$out[32]];
		$spower2 = $spower[$out[33]];
				
		if (($out[30] < 4) and ($out[30] >= 0) and ($out[31]=='10')) { 
			$cloudiness=$this->imgcloud($out[27]);
			$cloudiness2 = $cloudiness[$out[30]];
		} else {
			$precipitation=$this->imgprecip($out[27]);
			$precipitation2 = $precipitation[$out[31]];
		}
				
		$pressureMIN2 = $out[35];
		$pressureMAX2 = $out[34];
		$tempMIN2 = $out[37];
		$tempMAX2 = $out[36];
		$windMIN2 = $out[38];
		$windMAX2 = $out[39];
		$direction2 = $direction[$out[40]];
		$relwenMIN2 = $out[42];
		$relwenMAX2 = $out[41];
		$heatMIN2 = $out[43]; 
		$heatMAX2 = $out[44]; 

		$day3 = $out[45];
		$month3 = $month_array[ceil($out[46])];
		$tod3 = $tod[ceil($out[49])];
		$weekday3 = $weekday[$out[51]];
				
		$rpower3 = $rpower[$out[54]];
		$spower3 = $spower[$out[55]];
				
		if (($out[52] < 4) and ($out[52] >= 0) and ($out[53]=='10')) {
			$cloudiness=$this->imgcloud($out[49]);
			$cloudiness3 = $cloudiness[$out[52]];
		} else {
			$precipitation=$this->imgprecip($out[49]);
			$precipitation3 = $precipitation[$out[53]];
		}
				
		$pressureMIN3 = $out[57];
		$pressureMAX3 = $out[56];
		$tempMIN3 = $out[59];
		$tempMAX3 = $out[58];
		$windMIN3 = $out[60];
		$windMAX3 = $out[61];
		$direction3 = $direction[$out[62]];
		$relwenMIN3 = $out[64];
		$relwenMAX3 = $out[63];
		$heatMIN3 = $out[65];
		$heatMAX3 = $out[66];
		
		/* 
		�������� �������� ���� �������, �������� ���������� ����� ��� $relwenMIN1, $relwenMIN2,$relwenMIN3 � ���� ����� ���� $relwenMAX1.. � ��
		����� �������� ������ ���������� ������� ��� �� ����� ��� �������� ��� �� ������.
		������ � ������ ���� �������� ��� ������ �� �� ����� �����, �.�. � ������.
		*/
		$content .= "<div style=\"border-top:1px dashed #999999;\">".$cloudiness1." ".$precipitation1." ".$day1." ".$month1.", ".$weekday1."<br />".$tod1." <span title=\"������� ".$heatMIN1."..".$heatMAX1."�C\">".$tempMIN1."�C...".$tempMAX1."�C</span><br /><font size=1>����� ".$windMIN1."-".$windMAX1."�/c ".$direction1."</font></div>";
		$content .= "<div style=\"border-top:1px dashed #999999;border-bottom:1px dashed #999999;\">".$cloudiness2." ".$precipitation2." ".$day2." ".$month2.", ".$weekday2."<br />".$tod2." <span title=\"������� ".$heatMIN2."..".$heatMAX2."�C\">".$tempMIN2."�C...".$tempMAX2."�C</span><br /><font size=1>����� ".$windMIN2."-".$windMAX2."�/c ".$direction2."</font></div>";
		$content .= "<div style=\"border-bottom:1px dashed #999999;\">".$cloudiness3." ".$precipitation3." ".$day3." ".$month3.", ".$weekday3."<br />".$tod3." <span title=\"������� ".$heatMIN3."..".$heatMAX3."�C\">".$tempMIN3."�C...".$tempMAX3."�C</span><br /><font size=1>����� ".$windMIN3."-".$windMAX3."�/c ".$direction3."</font></div>";
	
		return $content; //������� ���� �������

	
	}
}
?>
