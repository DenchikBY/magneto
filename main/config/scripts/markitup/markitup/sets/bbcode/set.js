myBbcodeSettings = {
  nameSpace: "bbcode",
  markupSet: [
	  {name:'������', openWith:'[b]', closeWith:'[/b]'}, 
	  {separator:'-'},      
	  {name:'������', openWith:'[i]', closeWith:'[/i]'}, 
	  {separator:'-'},
      {name:'������������', openWith:'[u]', closeWith:'[/u]'}, 
	  {separator:'-'},
      {name:'�����������', openWith:'[s]', closeWith:'[/s]'},	  
	  {separator:'-'},
	  {name:'��������� �����', openWith:'[left]', closeWith:'[/left]'},
	  {separator:'-'},
	  {name:'��������� �� ������', openWith:'[center]', closeWith:'[/center]'},
	  {separator:'-'},	  
	  {name:'��������� ������', openWith:'[right]', closeWith:'[/right]'},
	  {separator:'-'},
      {name:'�������� �����', openWith:'[hr]', closeWith:''},
	  {separator:'-'},
      {name:'�������� �����������', openWith:'[img]', closeWith:'[/img]'}, 
	  {separator:'-'},
      {name:'�������� ������', openWith:'[url=��������]', closeWith:'[/url]'},
	  {separator:'-'},
      {name:'���� ������', dropMenu: [
          {name:'������', openWith:'[color=yellow]', closeWith:'[/color]', className:"col1-1" },
          {name:'����������', openWith:'[color=orange]', closeWith:'[/color]', className:"col1-2" },
          {name:'�������', openWith:'[color=red]', closeWith:'[/color]', className:"col1-3" },
          {name:'�����', openWith:'[color=blue]', closeWith:'[/color]', className:"col2-1" },
          {name:'���������', openWith:'[color=purple]', closeWith:'[/color]', className:"col2-2" },
          {name:'�������', openWith:'[color=green]', closeWith:'[/color]', className:"col2-3" },
          {name:'�����', openWith:'[color=white]', closeWith:'[/color]', className:"col3-1" },
          {name:'�����', openWith:'[color=gray]', closeWith:'[/color]', className:"col3-2" },
          {name:'������', openWith:'[color=black]', closeWith:'[/color]', className:"col3-3" }
      ]},
	  {separator:'-'},	  
      {name:'������ ������', dropMenu :[
          {name:'���������', openWith:'[size=small]', closeWith:'[/size]' },
          {name:'����������', openWith:'[size=normal]', closeWith:'[/size]' },
          {name:'�������', openWith:'[size=big]', closeWith:'[/size]' }		  
      ]},
	  {separator:'-'},
      {name:'������', openWith:'[quote]', closeWith:'[/quote]'},
	  {separator:'-'},
      {name:'�������', openWith:'[spoiler]', closeWith:'[/spoiler]'},
	  {separator:'-'},
      {name:'�������', dropMenu: [
          {name:'����������: ����', openWith:'[b]���� ��������:[/b] \n[b]����� ��������:[/b] \n[b]����:[/b] \n[b]�������:[/b] \n[b]�����:[/b] \n[b]����� ������� � ��������:[/b] \n[b]������ �����:[/b] \n[b]��������� �����:[/b] \n[b]������ ������:[/b] \n\n[b]���������:[/b] \n\n\n[b]������ �� ��, ���:[/b] \n\n\n[b]������������:[/b]\n' },
          {name:'����������: ���������� ������', openWith:'[b]���� ��������:[/b] \n[b]�����:[/b] \n\n[b]� ������:[/b] \n\n\n[b]�������:[/b]\n' },
          {name:'����������: ����������', openWith:'[b]���� ��������:[/b] \n[b]������ ������:[/b] \n\n[b]��������:[/b] \n\n\n[b]������������:[/b]\n\n\n[b]����������� ����:[/b]\n[url][/url]' }
      ]},
	  {separator:'-'},
   ]
}