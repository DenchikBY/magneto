                       $(document).ready(function(){
                         $('a.imgpreview').imgPreview({
                           containerID: 'imgPreviewWithStyles',
                           srcAttr: 'rel',
                           imgCSS: {
                             width: 100
                           },
                           onShow: function(link){
                             $(link).stop().animate({opacity:0.4});
                             if (ie === undefined){
                                $('img', this).stop().css({opacity:0});
                             }
                             $('<span>' + $(link).text() + '<\/span>').appendTo(this);
                           },
                           onLoad: function(){
                             $(this).animate({opacity:1}, 500);
                           },
                           onHide: function(link){
                             $(link).stop().animate({opacity:1});
                             $('span', this).remove();
                           }
                         });
                       })