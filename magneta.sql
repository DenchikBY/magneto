-- phpMyAdmin SQL Dump
-- version 4.4.13.1
-- http://www.phpmyadmin.net
--
-- Хост: localhost:3306
-- Время создания: Окт 03 2015 г., 04:39
-- Версия сервера: 5.6.26
-- Версия PHP: 7.0.0RC3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `magneta`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tblf_comment`
--

CREATE TABLE IF NOT EXISTS `tblf_comment` (
  `CODE` int(11) NOT NULL,
  `POSTCODE` int(11) DEFAULT NULL,
  `WHOADD` int(11) DEFAULT NULL,
  `DATEADD` datetime DEFAULT NULL,
  `OPIS` text
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `tblf_comment`
--

INSERT INTO `tblf_comment` (`CODE`, `POSTCODE`, `WHOADD`, `DATEADD`, `OPIS`) VALUES
(1, 1, 2, '2011-01-23 12:56:46', 'хороший форум)'),
(3, 3, 2, '2011-02-09 22:31:59', '11111'),
(4, 1, 2, '2011-02-10 16:37:13', '[SM_02]');

-- --------------------------------------------------------

--
-- Структура таблицы `tblf_kat`
--

CREATE TABLE IF NOT EXISTS `tblf_kat` (
  `CODE` int(5) NOT NULL,
  `NAZV` char(100) DEFAULT NULL,
  `POSITION` int(5) DEFAULT '1',
  `OPIS` varchar(100) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `tblf_kat`
--

INSERT INTO `tblf_kat` (`CODE`, `NAZV`, `POSITION`, `OPIS`) VALUES
(1, '123', 2, NULL),
(2, '456', 1, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tblf_post`
--

CREATE TABLE IF NOT EXISTS `tblf_post` (
  `CODE` int(11) NOT NULL,
  `ISMODER` int(1) DEFAULT NULL,
  `NAZV` varchar(250) DEFAULT NULL,
  `NAZVOPIS` varchar(250) DEFAULT NULL,
  `OPIS` text,
  `DATEADD` datetime DEFAULT NULL,
  `WHOADD` int(5) DEFAULT NULL,
  `DATEMODER` datetime DEFAULT NULL,
  `WHOMODER` int(5) DEFAULT NULL,
  `SUBKAT` int(11) DEFAULT NULL,
  `VIEW` int(255) DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `tblf_post`
--

INSERT INTO `tblf_post` (`CODE`, `ISMODER`, `NAZV`, `NAZVOPIS`, `OPIS`, `DATEADD`, `WHOADD`, `DATEMODER`, `WHOMODER`, `SUBKAT`, `VIEW`) VALUES
(1, 1, '123', '456', 'трям', '2010-01-20 16:24:00', 2, '2010-01-20 16:24:00', 2, 1, 102),
(3, 0, '1111111', '1111111111', '111111111111111', '2011-02-07 17:02:50', 2, '2011-02-07 17:02:50', 2, 1, 15);

-- --------------------------------------------------------

--
-- Структура таблицы `tblf_subkat`
--

CREATE TABLE IF NOT EXISTS `tblf_subkat` (
  `CODE` int(5) NOT NULL,
  `KATCODE` int(11) DEFAULT '0',
  `NAZV` char(100) DEFAULT NULL,
  `POSITION` int(5) DEFAULT '1',
  `OPIS` varchar(100) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `tblf_subkat`
--

INSERT INTO `tblf_subkat` (`CODE`, `KATCODE`, `NAZV`, `POSITION`, `OPIS`) VALUES
(1, 1, 'Обсуждения', 1, 'Какието обсуждения');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_banip`
--

CREATE TABLE IF NOT EXISTS `tbl_banip` (
  `CODE` int(11) NOT NULL,
  `USERIP` varchar(15) DEFAULT NULL,
  `DATEADD` datetime DEFAULT NULL,
  `WHOADD` int(11) DEFAULT NULL,
  `OPIS` varchar(200) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `tbl_banip`
--

INSERT INTO `tbl_banip` (`CODE`, `USERIP`, `DATEADD`, `WHOADD`, `OPIS`) VALUES
(18, '10.14.21.181', '2010-02-17 21:48:04', 2, '------------'),
(19, '10.6.33.221', '2010-06-01 15:58:17', 2, 'Удаление категории'),
(20, '10.14.21.180', '2010-06-10 20:38:16', 2, '------------'),
(21, '10.14.13.220', '2010-11-05 17:35:41', 2, 'Маты в комментариях');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_base`
--

CREATE TABLE IF NOT EXISTS `tbl_base` (
  `CODE` int(11) NOT NULL,
  `ISMODER` int(1) DEFAULT '0',
  `KAT` int(11) DEFAULT NULL,
  `SUBKAT1` int(11) DEFAULT NULL,
  `SUBKAT2` int(11) DEFAULT NULL,
  `SUBKAT3` int(11) DEFAULT NULL,
  `NAZV` varchar(250) DEFAULT NULL,
  `RELEASE2` varchar(9) DEFAULT NULL,
  `COUNTRY` varchar(200) DEFAULT NULL,
  `IMDB` varchar(200) DEFAULT NULL,
  `RAZMER` int(11) DEFAULT '0',
  `OPIS` text CHARACTER SET cp1251 COLLATE cp1251_ukrainian_ci,
  `POSTER` varchar(100) DEFAULT NULL,
  `VOTES` int(11) DEFAULT NULL,
  `DATEADD` datetime DEFAULT NULL,
  `WHOADD` int(5) DEFAULT NULL,
  `DATEMODER` datetime DEFAULT NULL,
  `WHOMODER` int(5) DEFAULT NULL,
  `ISCOMMENT` int(1) DEFAULT NULL,
  `DATEEDIT` datetime DEFAULT NULL,
  `WHOEDIT` int(11) DEFAULT NULL,
  `LIMITRANG` int(11) DEFAULT NULL,
  `ISDOPEDIT` int(1) DEFAULT NULL,
  `WHODOPEDIT` int(11) DEFAULT NULL,
  `WHYDOPEDIT` varchar(200) DEFAULT NULL,
  `DOWN` int(11) DEFAULT NULL,
  `DOPF1` varchar(1000) DEFAULT NULL,
  `DOPF2` varchar(1000) DEFAULT NULL,
  `DOPF3` varchar(1000) DEFAULT NULL,
  `DOPF4` varchar(1000) DEFAULT NULL,
  `DOPF5` varchar(1000) DEFAULT NULL,
  `KINOPOISK` varchar(200) DEFAULT NULL,
  `ORNAZV` varchar(250) DEFAULT NULL,
  `SUBKAT4` int(11) DEFAULT '0',
  `SUBKAT5` int(11) DEFAULT '0',
  `RAT` int(1) DEFAULT '0',
  `VIEW` int(255) DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=5006 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_bots`
--

CREATE TABLE IF NOT EXISTS `tbl_bots` (
  `CODE` int(11) NOT NULL,
  `NAZV` varchar(100) DEFAULT NULL,
  `SIG` varchar(200) DEFAULT NULL,
  `VISITS` int(11) DEFAULT NULL,
  `STATUS` int(11) DEFAULT '0',
  `TIME` int(25) DEFAULT NULL,
  `URL` varchar(200) DEFAULT NULL,
  `IP` varchar(15) DEFAULT NULL,
  `URI` varchar(256) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `tbl_bots`
--

INSERT INTO `tbl_bots` (`CODE`, `NAZV`, `SIG`, `VISITS`, `STATUS`, `TIME`, `URL`, `IP`, `URI`) VALUES
(52, '- NoName -', 'undefined', 0, 0, 0, '', '', ''),
(3, 'Yandex Dyatel', 'Win16; Dyatel', 0, 0, NULL, NULL, NULL, NULL),
(4, 'Yandex Pictures', 'YandexImages/', 0, 0, NULL, NULL, NULL, NULL),
(5, 'Yandex MMedia', 'Win16; M', 0, 0, NULL, NULL, NULL, NULL),
(6, 'Yandex Favicons', 'Win16; F', 0, 0, NULL, NULL, NULL, NULL),
(7, 'Yandex Direct', 'YaDirectBot/', 0, 0, NULL, NULL, NULL, NULL),
(8, 'YandexBot', 'YandexBot', 0, 0, 0, '', '', ''),
(9, 'Google RSS', 'Feedfetcher-Google', 0, 0, NULL, NULL, NULL, NULL),
(10, 'GoogleBot', 'Googlebot/', 0, 0, 0, '', '', ''),
(11, 'Google', 'google', 0, 0, NULL, NULL, '', NULL),
(12, 'Begun Partners', 'Begun', 0, 0, NULL, NULL, NULL, NULL),
(13, 'Begun Robot Crawler', 'Begun Robot Crawler', 0, 0, 0, '', '', ''),
(14, 'MSN', 'msnbot/', 0, 0, 0, '', '', ''),
(16, 'WebCrawler', 'WebCrawler', 0, 0, NULL, NULL, NULL, NULL),
(17, 'ZyBorg', 'ZyBorg', 0, 0, NULL, NULL, NULL, NULL),
(18, 'AltaVista', 'scooter', 0, 0, NULL, NULL, NULL, NULL),
(19, 'StackRambler', 'StackRambler', 0, 0, NULL, NULL, NULL, NULL),
(20, 'Aport', 'aport', 0, 0, NULL, NULL, NULL, NULL),
(21, 'Lycos', 'lycos', 0, 0, NULL, NULL, NULL, NULL),
(22, 'Fast Search', 'fast', 0, 0, NULL, NULL, NULL, NULL),
(23, 'Rambler', 'rambler', 0, 0, NULL, NULL, NULL, NULL),
(24, 'Yahoo!!!', 'Yahoo! Slurp/', 0, 0, 0, '', '', ''),
(25, 'Twiceler', 'Twiceler', 0, 0, 0, '', '', ''),
(27, 'MSNbot Media', 'msnbot-media/', 0, 0, NULL, NULL, NULL, NULL),
(28, 'W3C [Validator]', 'Validator', 0, 0, NULL, NULL, NULL, NULL),
(29, 'Google [AdsBot]', 'AdsBot-Google', 0, 0, NULL, NULL, NULL, NULL),
(30, 'Google [Adsense]', 'Mediapartners-Google', 0, 0, NULL, NULL, NULL, NULL),
(31, 'Baidu', 'Baiduspider+', 0, 0, NULL, NULL, NULL, NULL),
(32, 'Mail.Ru', 'Mail.Ru/', 0, 0, NULL, NULL, NULL, NULL),
(33, 'Alexa', 'ia_archiver', 0, 0, NULL, NULL, NULL, NULL),
(34, 'Microsoft WebDAV', 'Microsoft-WebDAV-MiniRedir', 0, 0, NULL, NULL, NULL, NULL),
(35, 'CatchBot', 'CatchBot/2.0', 0, 0, NULL, NULL, NULL, NULL),
(36, 'WordPress', 'WordPress/', 0, 0, NULL, NULL, NULL, NULL),
(37, 'iGde', 'igdeSpyder', 0, 0, NULL, NULL, NULL, NULL),
(38, 'Yandex.Direct', 'Direct/', 0, 0, NULL, NULL, NULL, NULL),
(39, 'W3 [Sitesearch]', 'W3 SiteSearch Crawler', 0, 0, NULL, NULL, NULL, NULL),
(40, 'DobroBot', 'DobroBot', 0, 0, NULL, NULL, NULL, NULL),
(41, 'FriendFeedBot', 'FriendFeedBot/', 0, 0, NULL, NULL, NULL, NULL),
(42, 'GoldenSpider', 'GoldenSpider/', 0, 0, NULL, NULL, NULL, NULL),
(43, 'Libra [China]', 'librabot/', 0, 0, NULL, NULL, NULL, NULL),
(44, 'OMG Crawler', 'OMGCrawler', 0, 0, NULL, NULL, NULL, NULL),
(45, 'Boomerang', 'Boomerang/', 0, 0, NULL, NULL, NULL, NULL),
(46, 'NetCraft', 'NetcraftSurveyAgent/', 0, 0, NULL, NULL, NULL, NULL),
(47, 'BlogScope', 'BlogScope/', 0, 0, NULL, NULL, NULL, NULL),
(48, 'BlogPulse', 'BlogPulse', 0, 0, NULL, NULL, NULL, NULL),
(49, 'Snap Shots', 'Snapbot/', 0, 0, NULL, NULL, NULL, NULL),
(50, 'Tele-House', 'Dolphin/', 0, 0, NULL, NULL, NULL, NULL),
(51, 'Majestic-12', 'MJ12bot/', 0, 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_botslog`
--

CREATE TABLE IF NOT EXISTS `tbl_botslog` (
  `CODE` int(11) NOT NULL,
  `BASECODE` int(11) DEFAULT NULL,
  `TIME` int(25) DEFAULT NULL,
  `URI` varchar(256) DEFAULT NULL,
  `IP` varchar(15) DEFAULT NULL,
  `URL` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_chat`
--

CREATE TABLE IF NOT EXISTS `tbl_chat` (
  `CODE` int(11) NOT NULL,
  `WHOADD` int(11) DEFAULT NULL,
  `DATEADD` datetime DEFAULT NULL,
  `OPIS` varchar(250) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1003 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_comment`
--

CREATE TABLE IF NOT EXISTS `tbl_comment` (
  `CODE` int(11) NOT NULL,
  `ISMODER` int(1) DEFAULT NULL,
  `BASECODE` int(11) DEFAULT NULL,
  `WHOADD` int(11) DEFAULT NULL,
  `DATEADD` datetime DEFAULT NULL,
  `DATEEDIT` datetime DEFAULT NULL,
  `OPIS` text,
  `BADVOTE` int(1) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1735 DEFAULT CHARSET=cp1251 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_conf`
--

CREATE TABLE IF NOT EXISTS `tbl_conf` (
  `CODE` int(11) NOT NULL,
  `PARAM` char(100) COLLATE cp1251_ukrainian_ci DEFAULT NULL,
  `VALUESTR` char(200) COLLATE cp1251_ukrainian_ci DEFAULT NULL,
  `VALUEINT` int(1) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=247 DEFAULT CHARSET=cp1251 COLLATE=cp1251_ukrainian_ci ROW_FORMAT=FIXED;

--
-- Дамп данных таблицы `tbl_conf`
--

INSERT INTO `tbl_conf` (`CODE`, `PARAM`, `VALUESTR`, `VALUEINT`) VALUES
(1, 'SCHEME', 'green', NULL),
(2, 'ISCOMMENT', '1', 1),
(3, 'ISREGISTER', '0', 0),
(4, 'ISVALIDEMAIL', '0', 0),
(5, 'NETSITE', 'http://10.23.4.195/', NULL),
(6, 'NETNAME', 'MegaMagnet', NULL),
(7, 'ISWORK', '0', 1),
(8, 'WITHMODER', '1', 1),
(9, 'MINLINKTODOWN', '-500', 0),
(11, 'ONELOGINPERIP', '1', 1),
(12, 'VERSION', '1.9.6', NULL),
(14, 'ONLINETIME', '1', NULL),
(16, 'POPULARDAY', '90', NULL),
(17, 'COMMENTPERPAGE', '20', NULL),
(18, 'PERPAGE', '50', NULL),
(19, 'OFFLINEMES', 'Сайт отключен на технические работы', NULL),
(20, 'RATINGFORCOMMENT', '-500', NULL),
(21, 'RATINGFORAVATAR', '50', NULL),
(22, 'RATINGFORLINK', '-100', NULL),
(23, 'USESTAT', '1', 1),
(24, 'ISREPLACE', '1', 0),
(25, 'ISNEWS', '1', 1),
(26, 'ISONLINE', '1', 1),
(28, 'ISCHAT', '1', 1),
(29, 'MAXUSERCOUNT', '13', 0),
(30, 'MAXUSERDATE', '2009-08-07 19:42:37', NULL),
(31, 'ISTOPMODER', '1', 1),
(32, 'ISSHOWLINKTOUNREG', '0', 0),
(33, 'RAT_DOWN', '1', NULL),
(34, 'RAT_VOTES', '3', NULL),
(35, 'RAT_LINKS', '15', NULL),
(36, 'LANG_SYST', 'ru', NULL),
(37, 'RAT_PERSONA', '10', NULL),
(38, 'IS_FIRST_PERS', '1', 1),
(39, 'RSS_COUNT', '25', NULL),
(40, 'CHATUPD', '30', NULL),
(41, 'DCAGENT_LINK', '/files/FlylinkDC-r390-build-2436.exe', NULL),
(42, 'DCAGENT_NAZV', 'FlyLinkDC', NULL),
(43, 'IS_SHOW_DCAGENT', '1', 1),
(44, 'MINTOSEARCH', '3', NULL),
(45, 'ISADDALTLINKS', '1', 1),
(46, 'RATFORLINKS', '10', NULL),
(47, 'RAT_ALTLINKS', '5', NULL),
(48, 'MAXSIZEPOSTER', '1024', NULL),
(49, 'MASIZEAVATAR', '200', NULL),
(50, 'MAXSIZESCREEN', '1536', NULL),
(51, 'IS_COLORCHANGE', '1', 0),
(52, 'ALSORAZD', '10', NULL),
(53, 'PROJECTNAME', 'MegaMagnet - Гомельский Портал DC++', NULL),
(54, 'ISSHOPWORK', '0', 0),
(55, 'ISPERSENABLED', '1', 1),
(56, 'LINKSONTOPIC', '20', NULL),
(57, 'CPINSHAT', 'cp1251', NULL),
(58, 'ISTINY_TOPIC', '1', 1),
(59, 'ISREFLEX_AVA', '1', 1),
(60, 'ISTUBE', '0', 1),
(61, 'ISRNDTOPICLEFT', '1', 1),
(62, 'ISGETINET_POSTER', '1', 1),
(63, 'ISGETINET_SCREEN', '1', 1),
(64, 'ISOBYAVMAIN', '1', 1),
(65, 'ISSORTDOPF', '1', 1),
(66, 'ISFINDTOP', '1', 1),
(67, 'ISFINDTOP_2', '0', 0),
(68, 'ISFINDLEFT', '0', 0),
(69, 'ISPHOTOPERS', '1', 1),
(70, 'HOWMANYSCR', '6', NULL),
(71, 'ISCROSSETC', '1', 1),
(72, 'RATFORWANT', '20', NULL),
(73, 'ISWANTED', '1', 1),
(74, 'WANTEDDAY', '180', NULL),
(75, 'ICQSTATUS', '1', 1),
(76, 'ISAVAINDOWN', '1', 1),
(77, 'ISTAGS', '1', 1),
(78, 'ISLINKNEWW', '1', 1),
(79, 'ISVKONTAKTE', '0', 1),
(80, 'LINKVKONTAKTE', 'http://vk.com/club18643582', NULL),
(81, 'ISREKLAMA', '1', 0),
(82, 'ISEASYPAY', '0', 0),
(83, 'ISEASYPAYN', '08712772', NULL),
(84, 'ISSNOW', '0', 0),
(85, 'ISRNDPERSRIGHT', '1', 1),
(86, 'NETFORUM', 'http://mmforum.optimizer-portal.com/', NULL),
(87, 'ISFORUM', '0', 0),
(88, 'ISHUB', '1', 0),
(89, 'ISHUBLINK', 'dchub://10.23.4.195/', NULL),
(90, 'KONTAKT_MAIL', 'megamagnet@yandex.by', NULL),
(185, 'ISTOPUSER', NULL, 1),
(221, 'ISNEWSINOBZOR', NULL, 1),
(187, 'COORDHOR', '53.14', NULL),
(188, 'COORDVER', '50.19', NULL),
(189, 'ISHUBPINGER', NULL, 1),
(190, 'ISPOPULARDAY', NULL, 1),
(191, 'AVATARINCOMMENT', NULL, 1),
(192, 'OTHERRELISES', NULL, 0),
(193, 'ISREFLEX_PERSONA', NULL, 0),
(194, 'ISREFLEX_PERSONA_CLASS', 'instant', NULL),
(195, 'ISREFLEX_POSTER', NULL, 0),
(196, 'ISREFLEX_POSTER_CLASS', 'instant', NULL),
(197, 'ISVIEWWHOADD1', NULL, 1),
(198, 'ISCHATSMILES', NULL, 1),
(199, 'ISPERSAUTOCROSSENABLED', NULL, 1),
(200, 'IS_VIEW_KATIMAGE', NULL, 1),
(201, 'ISBOTVIEW', NULL, 0),
(202, 'IS_BOTSEARCH', NULL, 0),
(218, 'ISRNDPERSONA', NULL, 1),
(204, 'ISTOPMODERCOUNT', '5', NULL),
(205, 'ISTOPUSERCOUNT', '20', NULL),
(206, 'ISCHATCOUNT', '25', NULL),
(207, 'ISPOPULARDAYCOUNT', '15', NULL),
(208, 'NEWRELISETXTLIMIT', '256', NULL),
(209, 'NEWSTXTLIMIT', '300', NULL),
(210, 'KEYWORDS', 'Скачать бесплатно, фильмы, музыка, мультфильмы, игры, игра, литература, изображения, боевик, комедия, кино, DC++, Hub', NULL),
(211, 'DESCRIPTION', 'MegaMagnet - Гомельский портал DC++ (скачать бесплатно фильмы, мультфильмы, сериалы, мультсериалы, музыку, игры, софт..)', NULL),
(212, 'GOOGLEVERIFY', 'no code', NULL),
(213, 'ISBOTVIEWTIME', '1', NULL),
(214, 'ISBOTVIEWLOGLENGHT', '200', NULL),
(219, 'CROSSPERSINOBZOR', NULL, 1),
(220, 'ISPERSINOBZOR', NULL, 1),
(222, 'ISVIEWWHOADD2', NULL, 1),
(223, 'OBZORTXTLIMIT', '400', NULL),
(224, 'SHOWPERPAGE', '10', NULL),
(225, 'IS_VOTING', NULL, 1),
(226, 'IS_VOTINGSTOP', NULL, 0),
(227, 'IS_PERSPHOTO', NULL, 1),
(228, 'IS_PERSPHOTO_ADD', NULL, 1),
(229, 'ISPERSPHOTO', '8', NULL),
(230, 'ISPERSPHOTOWIDTH', '120', NULL),
(231, 'IS_QB_ENABLED', NULL, 1),
(232, 'IS_RATIOPARSE', NULL, 0),
(233, 'RAT_PERSONAPHOTO', '2', NULL),
(234, 'KCAPTCHA', NULL, 1),
(235, 'VOPROS', NULL, 1),
(236, 'VOPROS_1', '2 + 2 : 2 =', NULL),
(237, 'VOPROS_2', '3', NULL),
(238, 'ISWEATHER', NULL, 0),
(239, 'IS_INDEX_DOWN_REKL', NULL, 0),
(240, 'CODE_INDEX_DOWN_REKL', '<center><a href="http://www.mysteryptc.com/index.php?ref=katashi59"><img src="images/mysteryptc.gif"></a></center>', NULL),
(241, 'IS_RAZD_REKL', NULL, 0),
(242, 'CODE_RAZD_REKL', '<center><a href="http://www.mysteryptc.com/index.php?ref=katashi59"><img src="images/mysteryptc.gif"></a></center>', NULL),
(243, 'REG_MSG', 'Добро пожаловать на сайт!&lt;br&gt;Наша группа [url=ВКонтакте]http://vkontakte.ru/club18643582[/url]', NULL),
(244, 'IS_REFREG', NULL, 1),
(245, 'RAT_REFREG', '100', NULL),
(246, 'IS_MAINCHANGE', NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_conf_dorab`
--

CREATE TABLE IF NOT EXISTS `tbl_conf_dorab` (
  `CODE` int(11) NOT NULL,
  `OPIS` varchar(250) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `tbl_conf_dorab`
--

INSERT INTO `tbl_conf_dorab` (`CODE`, `OPIS`) VALUES
(1, 'Добавить оригинальное название фильма в название раздачи'),
(2, 'Добавить трэк-лист'),
(3, 'Добавить системные требования'),
(4, 'Исправить магнет-ссылку(и)'),
(5, 'Добавить описание'),
(6, 'Добавить в описание режиссера и список актеров'),
(7, 'Исправить название ссылок'),
(8, 'Поставить нормальный постер');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_conf_shop`
--

CREATE TABLE IF NOT EXISTS `tbl_conf_shop` (
  `CODE` int(11) NOT NULL,
  `NAZV` varchar(250) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_cross`
--

CREATE TABLE IF NOT EXISTS `tbl_cross` (
  `CODE` int(11) NOT NULL,
  `FIRSTCODE` int(11) DEFAULT NULL,
  `SECONDCODE` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1065 DEFAULT CHARSET=cp1251 COLLATE=cp1251_ukrainian_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_cross2`
--

CREATE TABLE IF NOT EXISTS `tbl_cross2` (
  `CODE` int(11) NOT NULL,
  `FIRSTCODE` int(11) DEFAULT NULL,
  `SECONDCODE` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4630 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_dopf`
--

CREATE TABLE IF NOT EXISTS `tbl_dopf` (
  `CODE` int(11) NOT NULL,
  `KATCODE` int(11) DEFAULT NULL,
  `DOPF1` varchar(50) DEFAULT NULL,
  `DOPF2` varchar(50) DEFAULT NULL,
  `DOPF3` varchar(50) DEFAULT NULL,
  `DOPF4` varchar(50) DEFAULT NULL,
  `DOPF5` varchar(50) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `tbl_dopf`
--

INSERT INTO `tbl_dopf` (`CODE`, `KATCODE`, `DOPF1`, `DOPF2`, `DOPF3`, `DOPF4`, `DOPF5`) VALUES
(1, 1, 'Продолжительность', 'Режиссер', 'Сценарист', 'Продюсер', 'В ролях'),
(2, 4, 'Разработчик', 'Издатель', 'Платформа', 'Язык интерфейса и озвучки', 'Таблетка'),
(3, 26, 'Жанр', 'Режиссер', 'Сценарист', 'Продюсер', 'В ролях'),
(4, 24, 'Жанр', 'Режиссер', 'Сценарист', 'Продюсер', 'В ролях'),
(5, 25, 'Продолжительность', 'Режиссер', 'Сценарист', 'Продюсер', 'В ролях'),
(6, 3, 'Формат', 'Качество', 'Артист(ы)', 'Дата релиза', ''),
(7, 23, 'Производитель', 'Версия', 'Язык интерфейса', 'Системные требования', 'Таблетка'),
(8, 32, 'Жанр', 'Режиссер', 'Сценарист', 'Продюсер', 'В ролях'),
(9, 22, 'Издательство', '', '', '', ''),
(10, 29, 'Продолжительность', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_fav`
--

CREATE TABLE IF NOT EXISTS `tbl_fav` (
  `CODE` int(11) NOT NULL,
  `USERCODE` int(11) DEFAULT NULL,
  `BASECODE` int(11) DEFAULT NULL,
  `DATELAST` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=190 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_friends`
--

CREATE TABLE IF NOT EXISTS `tbl_friends` (
  `CODE` int(11) NOT NULL,
  `USERCODE` int(11) DEFAULT NULL,
  `FRIENDCODE` int(11) DEFAULT NULL,
  `CONFIRM` int(1) DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_hublist`
--

CREATE TABLE IF NOT EXISTS `tbl_hublist` (
  `CODE` int(11) NOT NULL,
  `ADDR` varchar(80) NOT NULL,
  `PORT` varchar(10) NOT NULL DEFAULT '411',
  `NAZV` text NOT NULL,
  `OWNER` varchar(64) NOT NULL,
  `DESCR` varchar(666) NOT NULL,
  `SOFT` varchar(64) NOT NULL,
  `LANG` varchar(64) NOT NULL,
  `SITY` varchar(64) NOT NULL,
  `FAVICO` varchar(64) NOT NULL,
  `STATUS` varchar(64) NOT NULL,
  `WHOADD` varchar(128) NOT NULL,
  `DATEADD` datetime DEFAULT NULL,
  `POSITION` int(11) NOT NULL,
  `ISMODER` int(11) NOT NULL,
  `FAV` int(11) NOT NULL,
  `FAVPOS` int(11) NOT NULL,
  `MAXUSERS` varchar(64) NOT NULL DEFAULT '0',
  `MINSHARE` varchar(64) NOT NULL,
  `MAXSHARE` varchar(64) NOT NULL,
  `MINSLOTS` int(11) NOT NULL,
  `MAXSLOTS` int(11) NOT NULL,
  `MAXHUBS` varchar(64) NOT NULL DEFAULT '0',
  `USERS` int(11) NOT NULL DEFAULT '0',
  `USERSPEAK` int(11) NOT NULL DEFAULT '0',
  `OPS` int(11) NOT NULL DEFAULT '0',
  `SHARE` float NOT NULL DEFAULT '0',
  `SHAREPEAK` float NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `tbl_hublist`
--

INSERT INTO `tbl_hublist` (`CODE`, `ADDR`, `PORT`, `NAZV`, `OWNER`, `DESCR`, `SOFT`, `LANG`, `SITY`, `FAVICO`, `STATUS`, `WHOADD`, `DATEADD`, `POSITION`, `ISMODER`, `FAV`, `FAVPOS`, `MAXUSERS`, `MINSHARE`, `MAXSHARE`, `MINSLOTS`, `MAXSLOTS`, `MAXHUBS`, `USERS`, `USERSPEAK`, `OPS`, `SHARE`, `SHAREPEAK`) VALUES
(5, 'dc.iptv.by', '4111', 'Гарант', '', '', '', '', '', '', '', '2', '2010-07-06 17:24:13', 2, 1, 1, 2, '0', '', '', 0, 0, '0', 0, 0, 0, 0, 0),
(6, '4local.ru', '411', '4local.ru', '', '', '', '', '', '', '', '2', '2010-07-12 12:24:49', 3, 1, 1, 4, '0', '', '', 0, 0, '0', 0, 0, 0, 0, 0),
(4, '10.23.4.195', '411', 'MegaMagnet', '', '', '', '', '', '', '', '2', '2010-07-06 17:18:52', 1, 1, 0, 0, '0', '', '', 0, 0, '0', 0, 0, 0, 0, 0),
(8, '192.168.48.1', '422', '4local.ru VIP', '', '', '', '', '', '', '', '2', '2010-12-20 19:19:05', 4, 1, 0, 0, '0', '', '', 0, 0, '0', 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_kat`
--

CREATE TABLE IF NOT EXISTS `tbl_kat` (
  `CODE` int(5) NOT NULL,
  `SUBKAT` int(5) DEFAULT '0',
  `NAZV` char(100) DEFAULT NULL,
  `POSITION` int(5) DEFAULT '1',
  `IMAGE` varchar(200) DEFAULT NULL,
  `TYPE` int(11) DEFAULT NULL,
  `RAZD` int(11) DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=cp1251 ROW_FORMAT=FIXED;

--
-- Дамп данных таблицы `tbl_kat`
--

INSERT INTO `tbl_kat` (`CODE`, `SUBKAT`, `NAZV`, `POSITION`, `IMAGE`, `TYPE`, `RAZD`) VALUES
(1, 0, 'Фильмы', 1, 'films', 1, 0),
(3, 0, 'Музыка', 9, 'music', 1, 0),
(4, 0, 'Игры', 5, 'games', 1, 0),
(21, 0, 'Прочее', 99, 'other1', 1, 0),
(22, 0, 'Литература', 10, 'books', 1, 0),
(23, 0, 'Софт', 6, 'soft', 1, 0),
(24, 0, 'Сериалы', 2, 'serial', 1, 0),
(25, 0, 'Мультфильмы', 3, 'mults', 1, 0),
(26, 0, 'Мультсериалы', 4, 'multserials', 1, 0),
(29, 0, 'Уроки', 11, 'lessons', 1, 0),
(32, 0, 'Аниме', 7, 'anime', 1, 0),
(33, 0, 'WEB-мастеру', 8, 'WEB', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_link`
--

CREATE TABLE IF NOT EXISTS `tbl_link` (
  `CODE` int(11) NOT NULL,
  `ISMODER` int(1) DEFAULT NULL,
  `BASECODE` int(11) DEFAULT NULL,
  `LINKPOS` int(5) DEFAULT NULL,
  `LINK` text,
  `LINKOPIS` varchar(200) DEFAULT NULL,
  `DATEADD` datetime DEFAULT NULL,
  `WHOADD` int(11) DEFAULT NULL,
  `DOWN` int(11) DEFAULT NULL,
  `BADCOUNT` int(11) DEFAULT NULL,
  `ISHR` int(1) DEFAULT NULL,
  `IMPLINK` int(1) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=21119 DEFAULT CHARSET=cp1251 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_logmoder`
--

CREATE TABLE IF NOT EXISTS `tbl_logmoder` (
  `CODE` int(11) NOT NULL,
  `USERCODE` int(11) DEFAULT NULL,
  `DATEADD` datetime DEFAULT NULL,
  `TYPE` varchar(100) DEFAULT NULL,
  `OPIS` varchar(200) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4018 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_menulinks`
--

CREATE TABLE IF NOT EXISTS `tbl_menulinks` (
  `CODE` int(11) NOT NULL,
  `NAZV` varchar(250) DEFAULT NULL,
  `LINKHREF` varchar(250) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `tbl_menulinks`
--

INSERT INTO `tbl_menulinks` (`CODE`, `NAZV`, `LINKHREF`) VALUES
(12, 'Правила оформления раздач', 'index.php?type=opis'),
(13, 'Обзор', 'index2.php'),
(14, 'Статистика', 'stats.php');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_modernote`
--

CREATE TABLE IF NOT EXISTS `tbl_modernote` (
  `CODE` int(11) NOT NULL,
  `WHOADD` int(11) DEFAULT NULL,
  `DATEADD` datetime DEFAULT NULL,
  `OPIS` text
) ENGINE=MyISAM AUTO_INCREMENT=316 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_news`
--

CREATE TABLE IF NOT EXISTS `tbl_news` (
  `CODE` int(11) NOT NULL,
  `DATEADD` datetime DEFAULT NULL,
  `NAZV` varchar(200) DEFAULT NULL,
  `OPIS` text,
  `WHOADD` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_news_comment`
--

CREATE TABLE IF NOT EXISTS `tbl_news_comment` (
  `CODE` int(11) NOT NULL,
  `NEWCODE` int(11) DEFAULT NULL,
  `WHOADD` int(11) DEFAULT NULL,
  `DATEADD` datetime DEFAULT NULL,
  `DATEEDIT` datetime DEFAULT NULL,
  `OPIS` text
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_page`
--

CREATE TABLE IF NOT EXISTS `tbl_page` (
  `CODE` int(11) NOT NULL,
  `PRIMARYCODE` int(11) DEFAULT NULL,
  `NAZV` varchar(255) DEFAULT NULL,
  `POSITION` int(11) DEFAULT NULL,
  `ISSHOW` int(1) DEFAULT NULL,
  `DATEADD` datetime DEFAULT NULL,
  `OPIS` text
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `tbl_page`
--

INSERT INTO `tbl_page` (`CODE`, `PRIMARYCODE`, `NAZV`, `POSITION`, `ISSHOW`, `DATEADD`, `OPIS`) VALUES
(1, 0, 'СИСТЕМА: Счетчики', 0, 1, '2011-07-02 21:36:11', ''),
(28, 0, 'СИСТЕМА: Шаблоны описаний', 0, 1, '2010-05-28 18:02:55', '<p style="text-align: center;"><span style="font-size: x-small;"><span style="font-size: small;"><strong><span style="color: #ff6600;"><span style="color: #008000;"><span style="font-size: large;">Раздачи</span></span></span></strong></span></span></p>\r\n<hr />\r\n<p style="text-align: left;"><span style="font-size: x-small;"><span style="font-size: small;"><strong><span style="color: #ff6600;">Шаблон для раздела "Видео"</span></strong></span><br /><br /><strong>Полтергейст / Poltergeist</strong><br /><br /><strong>Год выпуска:</strong> 1982<br /><strong>Страна:</strong> США<br /></span><span style="font-size: x-small;"><strong>Жанр:</strong></span><span style="font-size: x-small;"> ужасы<br /><strong>Продолжительность:</strong> 01:53:52<br /><strong>Перевод:</strong> Любительск ий (одноголосый)<br /><strong>Русские субтитры:</strong> нет (если есть)<br /><br /><strong>Режиссер:</strong> Тоуб Хупер / Tobe Hooper<br /><br /><strong>В ролях:</strong> Крейг Т. Нелсон / Craig T. Nelson, Джобет Уильямс / JoBeth Williams, Беатрис Стрейт / Beatrice Straight<br /><br /><strong>Описание:</strong> Они здесь... Сначала они кажутся призрачными, играя в прятки в загородном доме Фрилингов. Но затем становится по-настоящему страшно! Тучи сгущаются, ожившие деревья нападают, а малютку Кэрол Энн Фрилинг засасывает в спектральную пустоту. И пока ее семья борется с вереницей кошмаров и пытается спасти девочку, по соседству, где-то рядом затаилось нечто...<br /><br /><strong>Качество:</strong> DVDRip<br /><strong>Формат:</strong> AVI<br /><strong>Видео кодек:</strong> DivX<br /><strong>Аудио кодек:</strong> MP3<br /><strong>Видео:</strong> DivX 5 560x240 29.97fps 726Kbps<br /><strong>Аудио:</strong> MPEG Audio Layer 3 44100Hz stereo 112Kbps (если несколько дорожек, указываем все, Дорога 1, Дорога 2)</span></p>\r\n<p style="text-align: left;"><span style="font-size: x-small;"><span style="color: #ff0000;">Скриншоты приветствуются!!!</span></span></p>\r\n<hr />\r\n<p><span style="font-size: x-small;"><br /><span style="font-size: small;"><span style="color: #ff6600;"><strong>Шаблон для Раздела "Музыка"</strong></span></span><br /><br /><br /><strong>SlimHouse Осень 2009 - Mixed By DJ Riga</strong><br /><br /><strong>Жанр:</strong> House, Electro House<br /><strong>Год выпуска диска:</strong> 2009<br /><strong>Аудио кодек:</strong> MP3<br /><strong>Битрейт аудио:</strong> 320 kbps<br /><strong>Продолжительность:</strong> 65:16<br /><strong>Трэклист:</strong>(обязательно)<br /><br /><span style="color: #ff0000;">Все поля обязятельны к заполнению</span><br /></span></p>\r\n<hr />\r\n<p><span style="font-size: x-small;"><br /><span style="font-size: small;"><span style="color: #ff6600;"><strong>Шаблон для Раздела "Игры"</strong></span></span><br /><br /><strong>Left 4 Dead 2 Demo (2009)|RUS+ENG</strong><br /><br /><strong>Год выпуска:</strong> 2009<br /><strong>Жанр:</strong> Action/Horror (Tactical / Shooter) / 3D / 1st Person<br /><strong>Разработчик:</strong> Valve Software<br /><strong>Издательство:</strong> Valve Software<br /><br /><strong>Системные требования:</strong><br /><strong>Рекомендуемые:</strong><br /><strong>Операционная система:</strong> Windows&reg; 7 / Vista / Vista64 / XP<br /><strong>Процессор:</strong> Intel core 2 duo 2.4GHz<br /><strong>Оперативка:</strong> 1 GB for XP / 2GB for Vista<br /><strong>Видео Карта:</strong> DirectX 9 compatible video card with Shader model 3.0. NVidia 7600, ATI X1600 or better<br /><strong>Место на жёском диске:</strong> At least 7.5 GB of free space<br /><strong>Звуковая карта:</strong> DirectX 9.0c compatible sound card<br /><br /><strong>Тип издания:</strong> лицензия<br /><strong>Язык интерфейса:</strong> английский + русский<br /><strong>Язык озвучки:</strong> английский<br /><strong>Таблэтка:</strong> вшита<br /><br /><strong>Описание:</strong><br />Долгожданное продолжение орденоносного кооперативного шутера "Left 4 Dead", признанного многими изданиями лучшей игрой 2008-го года в своем жанре. На этот раз вам вместе с друзьями предстоит пробираться сквозь города, болота и кладбища американского Юга - от порта Саванны до Нового Орлеана - в ходе 5 обширных кампаний. Вы играете за одного из четырех выживших героев, имеющих широкий спектр всевозможного оружия, как классического образца, так и усовершенствованных модификаций. В дополнение к огнестрельному оружию у вас будет возможность разобраться с мертвяками и по-свойски, по-мужски, взяв в руки бензопилу, топоры или же смертельную ужасающую сокрушительную... сковородку! Да, теперь мертвецам наконец-то есть, чего бояться! Отныне разлагающаяся плоть тоже познает страх!<br /><br /><strong>Дополнительная информация:</strong> если есть<br /><br /><strong>Установка:</strong><br />1. Монтируем<br />2. Устанавливаем<br />3. Наслаждаемся игрой<br /><br /><span style="font-size: x-small;"><span style="color: #ff0000;">Скриншоты приветствуются!!!</span></span><br /></span></p>\r\n<hr />\r\n<p><span style="font-size: x-small;"><br /><strong><span style="font-size: small;"><span style="color: #ff6600;">Шаблон для раздела "Софт"</span></span></strong><br /><br /><strong>Полный набор обновлений для Microsoft Windows и Office (Rus) x86 и x64 от 03.11.09</strong><br /><br /><strong>Год выпуска:</strong> 2009<br /><strong>Версия:</strong> 6.2 build 031109<br /><strong>Разработчик:</strong> Microsoft<br /><strong>Язык интерфейса:</strong> только английский<br /><strong>Таблэтка:</strong> Не требуется<br /><br /><strong>Описание:</strong><br />Все обновления, вышедшие до 03.11.09 для русских версий<br />Windows 2000, XP (x86 и x64), Vista (x86 и x64), 7 (x86 и x64), 2003 Server (x86 и x64), 2008 Server (x86 и x64).<br />Office 2000, XP, 2003, 2007 (x86 и x64).<br />Программа сама определяет каких обновлений не хватает в ОС и устанавливает их автоматически. По желанию можно также создать откат системных файлов.<br /><br /><span style="font-size: x-small;"><span style="color: #ff0000;">Скриншоты приветствуются!!!</span></span></span></p>\r\n<hr />\r\n<p style="text-align: center;"><span style="font-size: x-small;"><span style="font-size: x-small;"><span style="color: #ff0000;"><span style="font-size: medium;"><span style="color: #008000;"><strong><span style="font-size: large;">Персоналии</span></strong></span></span></span></span></span></p>\r\n<hr />\r\n<p style="text-align: left;"><br /><span style="font-size: small;"><strong><span style="color: #ff6600;">Персоналия на актёра</span></strong></span><br /><span style="font-size: x-small;"></span><br /><strong><span style="font-size: x-small;">Рост:&nbsp;<br />Карьера:&nbsp;<br />Дата рождения:&nbsp;<br />Место рождения:&nbsp;<br />Жанры:&nbsp;<br />Первый фильм:</span></strong><span style="font-size: x-small;"> <br /><br /><br /><strong>Биография:&nbsp;</strong><br />.....   <br /><br /><br /><strong>Дополнительная информация:</strong><br />.....<br /><br /><br /><strong>Фильмография:</strong><br />1. ...<br />2. ...</span></p>'),
(9, 0, 'СИСТЕМА: Объявление на главной странице', 0, 0, '2012-10-14 14:47:39', '<p style="text-align: center;"><span style="font-size: large;"><span style="color: #3366ff;"><span style="font-size: xx-large;"><strong>Кто сюда зашел - <span style="text-decoration: underline;">БЕГИТЕ</span>, ибо сайт мертв уже долгий год с лишним!!!</strong></span></span></span></p>\r\n<p><strong> </strong></p>\r\n<p style="text-align: center;"><span style="font-size: large;"><span style="color: #3366ff;"><span style="font-size: x-large;"><strong>(а хотя хороший был проект, но ему не дали развиться)</strong></span></span><strong><span style="color: #3366ff;"><br /></span></strong></span></p>'),
(11, 0, 'СИСТЕМА: Правила ресурса', 0, 0, '2012-02-20 21:26:51', '<p><span style="font-size: small;"><strong>Правила пользования ресурсом Magneto:</strong></span><span style="font-size: small;"><strong>&nbsp;</strong></span></p>\r\n<p><span style="font-size: small;"><strong>Г</strong><strong>лавные 2 правила:<br />&nbsp; 1. </strong><strong>Системный Администратор всегда прав.<br />&nbsp; 2. Если СисАдмин не прав смотри пункт 1.</strong></span><strong><span style="font-size: x-small;"><br /></span></strong></p>\r\n<p>- Правила являются обязательными для исполнения всеми присутствующими на портале Magneto.<br />- Администрация оставляет за собой право редактировать и удалять раздачи и сообщения пользователей.<br />- Администрация оставляет за собой право отключать лиц, нарушающих Порядок и Правила.<br />- Кроме того, Администрация оставляет за собой право изменять данные Правила.<br /><br /><br /><strong>1. Регистрация</strong><br /><br />- Регистрация является неотъемлемой частью присутствия на Magneto. Прохождение регистрации автоматически обозначает Ваше согласие с данными Правилами.<br /><strong>Обратите внимание, если имя (ник), которое Вы выбрали для участия в форуме:</strong><br />- повторяет уже существующее или нарушает нормы приличия и морали (такие Ники как: Гитлер, Аллах-акбар, Шоха, Пахан и т.д.)<br />- несет в себе рекламу сайтов<br />- содержит ненормативную лексику то Ваша учётная запись будет деактивирована без предупреждения.<br />- Кроме того, не выбирайте себе аватары, содержащие нецензурные изображения, иначе Ваш аккаунт будет удален без предупреждения.<br /><br /><br /><strong>2. Ограничения при написании комментариев.</strong><br /><br />Всем участникам Magneto запрещается:<br />2.1. Оскорблять участников в любой форме (Запрещено проявление любой грубости, угроз, личных оскорблений и нецензурных высказываний, в том числе и в скрытой форме, как в отношении юридических, так и конкретных физических лиц). Участники должны соблюдать уважительную форму общения.<br />2.2. Пользоваться в комментариях матерными выражениями и словами, в том числе и в завуалированной форме.<br />2.3. Заниматься оверпостингом и размножением офф-топиков.<br />2.4. Публиковать кряки, серийные номера и т.п. в открытом виде.<br />2.5. Писать рекламные тексты и сообщения коммерческого характера.<br />2.6. Заниматься коммерцией при помощи ресурса.<br />2.7. Использовать Magneto как почтовую доску объявлений для сообщений приватного характера, адресованных конкретному участнику.<br />2.8. Проявлять расовую, национальную и религиозную неприязнь, пропагандировать терроризм, экстремизм, наркотики и прочие темы, несовместимые с общепринятыми законами морали и приличия.<br />2.9. Публично предъявлять претензии и обсуждать действия Модератора или Администратора. Участник Magneto, не согласный с действиями Модератора, может высказать своё несогласие Модератору по почте или в личном сообщении. Если от Модератора нет ответа или ответ, по мнению участника, необоснованный, последний вправе переписку с Модератором отправить Администратору. Конечное решение принимает Администратор. Это решение является окончательным и необсуждаемым.<br /><br /><br /><strong>3. Правила создания раздач на Magneto.</strong></p>\r\n<p>Все создающие раздачу (выкладывающие файл для общедоступного скачивания) обязаны контролировать содержимое этой раздачи по качеству (к примеру, отсутствие звука в фильмах, неснятая защита в программах, неработоспособность программы и т.п.).<br />3.1.1. Запрещается<strong> </strong>Создавать и размещать для скачивания раздачу, дублирующую ту, что уже существует на Magneto. При создании одинаковой раздачи Модератор или Администратор добавят Вашу ссылку в уже существующую раздачу (если имеются отличия), а Ваша раздача будет удалена.<br />3.1.2. Запрещается Создавать новую раздачу в тематической серии (например, если это сериал, то запрещается создавать отдельно серии, разрешается только добавлять).<br />3.1.3. Запрещается Создавать и размещать для скачивания раздачу, содержащую документальные съёмки сцен и предметов, нарушающих общественные нормы морали, различные Базы Данных, отсутствующие в свободном законном доступе и т.п.<br />3.1.4. Запрещается Закрывать канал на отдачу или создавать несуществующие магнет-ссылки. Старайтесь оставаться на раздаче как можно дольше. Также запрещается техническими, программными или иными средствами и методами препятствовать файлообмену и искажать статистику обмена данными между пользователями ресурса (читерство). В противном случае Ваша учётная запись может быть деактивирована без предупреждения.<br />3.2. Все поля обязательны для заполнения<br />3.3. При добавлении раздачи фильма в названии ссылки указывать качество. (DVDRip, HDRip, HDTvRip, CamRip, TvRip, BDRip и тд)<br />3.4. Если вы добавляете сериал с несколькими сезонами, то не пишите в названии "Сезон -", а пишите полностью с названием "Название сериала - Сезон -"<br />3.5. Не добавляйте анимированные постеры в формате gif<br />3.6. Если вы добавляете раздачу в которой ссылка на 1 файл, то в поле "Название ссылки" вставляйте название раздачи<br />3.7. Ссылки вставлять только magnet, никаких ссылок на FTP или когда берёте magnet-ссылку не вставьте случайно TTH файла<br />3.8.&nbsp;Названия ссылок типа: скачать, качни, кликни ссылку, сама игра, игра, фильм, вот файл и т.д - не подходять. Нужно писать канкретно, к примеру для фильма название и в скобках Rip: Мачете / Machete [HDRip]<br /><br /><em>Решение по соответствию раздачи данным требованиям принимает Модератор и/или Администратор. В их полномочиях редактировать, перемещать, закрывать или удалять раздачу.</em><br /><br /><br /><strong>4. Дополнительные правила.</strong><br /><br />4.1. При копировании раздач с другого сайта обязательно указывать, что информация взята с этого сайта.<br />4.2. Если кто-то будет копировать раздачи с нашего сайта, то ставить эту раздачу на свой ресурс не раньше чем через 2-3 дня после появления её на нашем сайта, и обязательно указать что раздача взята отсюда, в противном случае вы получите бан на 2 дня, с каждим разом бан будет увеличиваться на 1 день.<br />4.3. Модератор обязан вылаживать минимум 10 раздач за неделю. Если втечение месяца он будет добавлять меньше 10 раздач в неделю, он будет лишён прав модератора.<br />4.4. Нельзя просить права модератора, администратора и т.д. на форуме, даже если вы модератор или администратор на сайте. Системного администратора на сайте тоже нельзя просить. Либо получите бан.<br />4.5. Все раздачи должны создаваться по всем правилам портала.</p>'),
(26, 0, 'СИСТЕМА: Реклама', 0, 1, '2010-11-01 18:02:51', '<p style="text-align: center;"><br /><a href="http://forex4you.org/?affid=5617932" target="_blank"><img src="images/fx200x200.gif" alt="" width="150" height="150" /><br /><br /></a><a href="http://advego.ru/0AuGZjVmQ7" target="_blank">Advego &mdash; общайся и зарабатывай деньги!<br /></a><br /><strong><a href="http://bux.to/?r=katashi59" target="_blank">Bux.to - отличный зароботок за клики!!!</a></strong><br /><strong><br /></strong></p>'),
(24, 0, 'СИСТЕМА: Полезная информация', 0, 1, '2012-02-20 17:13:55', '<p style="text-align: center;"><span style="font-size: medium;"><span style="color: #ff6600;"><span style="font-size: small;"><strong>Как тут качать</strong></span></span></span></p>\r\n<p style="text-align: left;"><span style="font-size: small;"><span style="color: #888888;"><strong>1. Чтобы тут качать у вас должен быть установлен DC клиент, в нём вы должны подключиться к хабу.<br />2. Выберите нужную вам раздачу или воспользуйтесь поиском чтобы найти то что вам нужно.<br />3. Зайдите на раздачу и перейдите по ссылке, находящейся внизу раздачи в ссылках.<br />4. DC клиент обработает эту ссылку и запросит разрешение на поиск по этой ссылке, нажмите искать.<br />5. Если файл найден по ссылке, нажмите правой кнопкой по файлу, скачать, и выберите куда скачать файл.</strong></span></span></p>\r\n<hr />\r\n<p><span style="font-size: small;"><strong><span style="color: #00ffff;">Локальные хабы:</span><br /><a href="dchub://10.23.4.195"><span style="color: #ff0000;">MegaMagnet</span></a> ; <a href="dchub://dc.iptv.by:4111">Гарант</a> ;&nbsp;<a href="dchub://4local.ru">4local.ru</a> ;&nbsp;<a href="dchub://4local.ru:422">4local.ru VIP</a><br /><span style="color: #00ffff;">Хабы для пользователей интернета:</span><span style="color: #00ffff;"><br /></span><span style="color: #00ffff;"><span style="color: #000000;"><a href="dchub://dc.ozerki.net">Cifra.Ozerki DC.Hub</a> ;&nbsp;<a href="dchub://dc.dcmagnets.ru">DCmagnets.ru</a> ;&nbsp;<a href="dchub://dc.filimania.ru">DC&bull;Filimania&bull;RU</a> ;&nbsp;<a href="dchub://dc.tiera.ru">TiERA</a> ;&nbsp;<a href="dchub://dc.mside.ru">&deg;&bull;-MagicSide-&bull;&deg;</a> ;&nbsp;<a href="dchub://verlihub.org:4111">VerliHub.ORG</a> ;&nbsp;<a href="dchub://dc.a-galaxy.ru">Andromeda Galaxy</a> ;&nbsp;<a href="dchub://m-radio.info">M-RADIO</a> ;&nbsp;<a href="dchub://starshub.ru">-=x STAR HUB Spb x=-</a> ;&nbsp;<a href="dchub://prapor.OXAHA.com">Prapor.OXAHA.Com</a></span></span></strong><strong> ; <a href="dchub://dc.magneto.by">Magneto.by</a></strong></span></p>\r\n<hr />\r\n<p style="text-align: center;"><span style="font-size: medium;"><span style="color: #ff6600;"><strong><span style="font-size: small;">Как добавить раздачу</span></strong></span></span></p>\r\n<p style="text-align: left;"><span style="font-size: x-small;"><span style="color: #ff6600;"><strong><span style="color: #888888;"><span style="font-size: small;">Чтобы научится делать раздачи на нашем сайте вы можете посмотреть мануал по созданию раздачи по</span></span><span style="font-size: small;"> </span><a href="files/add_topic_magneto.doc"><span style="color: #ff0000;"><span style="font-size: small;">этой ссылке.</span></span></a></strong></span></span></p>\r\n<p><span style="font-size: small;"> </span></p>\r\n<p style="text-align: left;"><span style="font-size: x-small;"><span style="color: #ff6600;"><strong><a href="index.php?type=opis"><span style="font-size: small;"><span style="color: #00ff00;"><span style="font-size: small;">Шаблоны оформления раздач и персоналий</span></span></span></a><span style="font-size: small;"><br /></span><span style="color: #888888;"><span style="font-size: small;"><br /></span></span></strong></span><span style="color: #ff6600;"><strong><span style="color: #99cc00;"><span style="font-size: small;">Правила добавления раздач:</span></span></strong></span><span style="color: #3366ff;"><strong><span style="font-size: x-small;"><span style="color: #3366ff;"><strong><span style="font-size: x-small;"><span style="color: #ff6600;"><strong><span style="color: #99cc00;"><span style="color: #008000;"><span style="font-size: small;"><br /></span><span style="font-size: small;">Перед созданием раздачи убедитесь что такой ещё нету.(воспользуйтесь поиском)</span><span style="font-size: small;"><br /></span></span></span></strong></span></span></strong></span></span><span style="font-size: small;">- Все поля обязательны для заполнения</span><span style="font-size: small;"><br /></span><span style="font-size: small;">- При добавлении раздачи фильма в названии ссылки указывать качество. (DVDRip, HDRip, HDTvRip, CamRip, TvRip, BDRip и тд)</span><span style="font-size: small;"><br /></span><span style="font-size: small;">- Если вы добавляете сериал с несколькими сезонами, то не пишите в названии "Сезон -", а пишите полностью с названием "Название сериала - Сезон -"</span><span style="font-size: small;"><br /></span><span style="font-size: small;">- Постеры раздач желательно чтобы были формата jpg, если у вас другой форматто сконвертируйте картинку в jpg</span><span style="font-size: small;"><br /></span><span style="font-size: small;">- Не добавляйте постеры в формате gif</span><span style="font-size: small;"><br /></span><span style="font-size: small;">- Если вы добавляете раздачу в которой ссылка на 1 файл, то в поле "Название ссылки" вставляйте название раздачи.</span><span style="font-size: small;"><br /></span><span style="font-size: small;">- Ссылки вставлять только magnet, никаких ссылок на FTP или когда берёте </span></strong></span><span style="color: #3366ff;"><strong><span style="font-size: small;">magnet-ссылку не вставьте случайно </span></strong></span><span style="color: #3366ff;"><strong><span style="font-size: small;">TTH файла</span><span style="font-size: small;"><br /></span><span style="font-size: small;">- Названия ссылок типа: скачать, качни, кликни ссылку, сама игра, игра, фильм, вот файл и т.д - не подходять. Нужно писать канкретно, к примеру для фильма название и в скобках Rip:&nbsp;Мачете / Machete [HDRip]</span><span style="font-size: small;"><br /></span><span style="font-size: small;">- Не добавляйте постер с логотипом другого сайта</span><span style="font-size: small;"><br /></span><span style="font-size: small;"><br /></span><span style="font-size: x-small;"><span style="color: #ff6600;"><strong><span style="color: #99cc00;"><span style="font-size: small;">Правила добавления персоналий:</span><span style="font-size: small;"><br /></span><span style="color: #008000;"><span style="font-size: small;">Перед созданием персоналии убедитесь что такой ещё нету.(воспользуйтесь поиском)</span></span><span style="font-size: small;"><br /></span><span style="font-size: x-small;"><span style="color: #3366ff;"><strong><span style="font-size: small;">- Название пункта должно начинаться с заглавной буквы</span></strong></span></span><span style="font-size: small;"><br /></span><span style="color: #3366ff;"><span style="font-size: small;">- В персоналиях актёров обязательно должна присутствовать Фильмография</span><span style="font-size: x-small;"><span style="color: #3366ff;"><strong><span style="font-size: x-small;"><span style="color: #ff6600;"><strong><span style="color: #99cc00;"><span style="font-size: small;"><br /></span><span style="font-size: small;"><br /></span><span style="font-size: small;">Дополнительная информация:</span><span style="font-size: small;"><br /></span><span style="color: #3366ff;"><span style="font-size: small;">- Если ваш браузер Opera, то в редакторе раздач при переходе на новую строку жмите не Enter, а Shift+Enter</span><span style="font-size: small;"><br /></span><span style="font-size: small;">- Если вдруг при добавлении раздачи, когда вы нажали кнопку "Добавить" вас перенаправляет на главную страницу и выходит из аккаунта, то не надо расстраиваться и заполнять раздачу заново , а входите в аккаунт и жмёте вроде 2 раза на кнопку вернутся назад в браузере оно переходит на добавление раздачи которую вы делали, дождитесь полной загрузки страницы, должно появится все что вы заполнили&nbsp; и снова жмите "Добавить"</span></span></span></strong></span></span></strong></span></span><br /></span></span></strong></span></span></strong></span></span></p>\r\n<hr />\r\n<p style="text-align: center;"><span style="font-size: small;"><span style="color: #ff6600;"><strong>Помощь Проекту<br /></strong></span></span></p>\r\n<p><span style="font-size: small;"><strong>Желающие помочь в развитии портала могут отправить любую сумму денег на WebMoney:<br />B363950588037<br />R586247492108<br />Z159700266495</strong></span></p>\r\n<hr />\r\n<p><strong> </strong></p>\r\n<p style="text-align: center;"><span style="font-size: small;"><span style="color: #ff6600;"><strong>Насч</strong></span><strong><span style="color: #ff6600;">ёт модераторства</span></strong></span></p>\r\n<p style="text-align: left;"><span style="font-size: small;"><span style="color: #b601fd;"><strong>Чтобы стать модератором на сайте нужно:<br />1. Ознакомиться со всеми правилами и информацией (особенной с этой странице).<br />2. Быть пользователем сайта не меньше 1 месяца.<br /></strong></span><span style="color: #b601fd;"><strong>3. Быть активным на сайте.<br />4. Вылаживать много хорошо оформленных раздач.<br />5. Быть адекватным в общении.<br />6. Написать Sharingan с вопросом о модераторстве.<br /><br />Обязанности модератора:<br />1. Должен создавать не менее 10 раздач и 5 персоналий в неделю.<br />2. Создавать раздачи по всем правилам.<br />3. Оповещать админов заранее о долгом отсутствии, чтобы не лишится прав.<br />4. Следить за пользователями, соблюдением всех правил.<br />5. Проверять раздачи пользователей на ошибки, недочеты.<br />6. Проверять комментарии пользователей. (чтобы небыло ничего "лишнего")</strong></span></span><span style="color: #b601fd;"><span style="font-size: small;"><strong><span style="font-size: x-small;"><br /></span></strong></span></span></p>\r\n<hr />\r\n<p style="text-align: center;"><span style="font-size: small;"><strong><span style="color: #ff6600;">Faq (Часто задавемые Вопросы)</span><span style="color: #000000;"><span style="color: #c0c0c0;">&nbsp;</span></span></strong></span></p>\r\n<p style="text-align: left;"><span style="color: #000000;"><span style="font-size: medium;"><span style="font-size: small;"><strong><span style="color: #008000;">Q: Где я?</span></strong></span><span style="font-size: small;"><strong><span style="color: #008000;"><span style="font-size: small;"><br /></span></span></strong></span><span style="font-size: small;"><strong><span style="color: #008000;"><span style="font-size: small;">A: Ты на DC портале</span></span></strong></span></span></span></p>\r\n<p><span style="font-size: small;"> </span></p>\r\n<p><span style="font-size: small;"> </span></p>\r\n<p><span style="font-size: small;"> </span></p>\r\n<p><span style="color: #008000;"><span style="font-size: small;"> </span></span></p>\r\n<p><span style="font-size: small;"> </span></p>\r\n<p><span style="font-size: small;"> </span></p>\r\n<p><span style="font-size: small;"> </span></p>\r\n<p><span style="color: #000000;"><span style="font-size: medium;"><span style="font-size: small;"><strong><span style="color: #008000;"><span style="font-size: small;">Q: Что это такое?</span></span></strong></span><span style="font-size: small;"><strong><span style="color: #008000;"><span style="font-size: small;"><br /></span></span></strong></span><span style="font-size: small;"><strong><span style="color: #008000;"><span style="font-size: small;">A: Это портал для создания релизов к файлам в файлобменных сетях DC</span></span></strong></span></span></span></p>\r\n<p><span style="font-size: small;"> </span></p>\r\n<p><span style="color: #000000;"><span style="font-size: medium;"><span style="font-size: small;"><strong><span style="color: #008000;"><span style="font-size: x-small;"><span style="color: #000000;"><span style="font-size: medium;"><span style="font-size: small;"><strong><span style="color: #008000;"><span style="font-size: small;">Q: Как повысить себе репутацию?</span></span></strong></span><span style="font-size: small;"><strong><span style="color: #008000;"><span style="font-size: small;"><br /></span></span></strong></span><span style="font-size: small;"><strong><span style="color: #008000;"><span style="font-size: small;">A: Чтобы повысить себе репутацию нужно создавать раздачи (по 15 репутации) и персоналии (по 10 репутации)</span></span></strong></span></span></span></span></span></strong></span></span></span></p>\r\n<p><span style="font-size: small;"> </span></p>\r\n<p><span style="font-size: small;"> </span></p>\r\n<p><span style="font-size: small;"> </span></p>\r\n<p><span style="color: #008000;"><span style="font-size: small;"> </span></span></p>\r\n<p><span style="font-size: small;"> </span></p>\r\n<p><span style="font-size: small;"> </span></p>\r\n<p><span style="font-size: small;"> </span></p>\r\n<p><span style="color: #000000;"><span style="font-size: medium;"><span style="font-size: small;"><strong><span style="color: #008000;"><span style="font-size: small;">Q: Как сделать релиз?</span></span></strong><strong><span style="color: #008000;"><span style="font-size: small;"><br /></span></span></strong><strong><span style="color: #008000;"><span style="font-size: x-small;"><span style="font-size: small;">A: Для начала надо зарегестрироватся, а потом нажать </span><a href="topic.php?type=new"><span style="font-size: small;">"Добавить раздачу"</span></a><span style="font-size: small;"> в меню слева или прочитайте инструкцию по добавлению раздач на портал (ссылка чуть выше)</span></span></span></strong></span></span></span></p>\r\n<p><span style="font-size: small;"> </span></p>\r\n<p><span style="font-size: small;"> </span></p>\r\n<p><span style="font-size: small;"> </span></p>\r\n<p><span style="color: #008000;"><span style="font-size: small;"> </span></span></p>\r\n<p><span style="font-size: small;"> </span></p>\r\n<p><span style="font-size: small;"> </span></p>\r\n<p><span style="font-size: small;"> </span></p>\r\n<p><span style="color: #000000;"><span style="font-size: medium;"><span style="font-size: small;"><strong><span style="color: #008000;"><span style="font-size: small;">Q: Как получить магнет ссылку?</span></span></strong><strong><span style="color: #008000;"><span style="font-size: small;"><br /></span></span></strong></span></span></span><span style="color: #000000;"><span style="font-size: medium;"><span style="font-size: small;"><strong><span style="color: #008000;"><span style="font-size: small;">A:&nbsp;</span></span></strong><strong><span style="color: #008000;"><span style="font-size: small;">Надо зайти в DC агент, открыть свой список файлов, найти нужный файл и скопировать его магнет-ссылку</span></span></strong></span></span></span></p>\r\n<hr />\r\n<p style="text-align: center"><span style="font-size: medium;"><span style="color: #ff6600;"><strong><span style="font-size: small;">Информация<br /></span></strong></span></span></p>\r\n<p style="text-align: left;"><span style="font-size: small;"><span style="color: #808080;"><strong>Когда используете поиск, ставьте галочку в "Расширенный поиск" иначе будет искать только по названиям.<br />Если у вас появляются большые картинки вместо маленьких значков либо просто изображение стоит не на своем месте, не пугайтесь сайт не сломался, эта проблема бывает только в Opera, вам следует очистить кэш сайта.</strong></span></span></p>\r\n<hr />');
INSERT INTO `tbl_page` (`CODE`, `PRIMARYCODE`, `NAZV`, `POSITION`, `ISSHOW`, `DATEADD`, `OPIS`) VALUES
(29, 0, 'Цвета', 1, 1, '2010-07-22 19:12:43', '<table border="0">\r\n<tbody>\r\n<tr>\r\n<td bgcolor="#ff00fe">ff00fe</td>\r\n<td bgcolor="#ff00fd">ff00fd</td>\r\n<td bgcolor="#ff00fc">ff00fc</td>\r\n<td bgcolor="#ff00fb">ff00fb</td>\r\n<td bgcolor="#ff00fa">ff00fa</td>\r\n<td bgcolor="#ff00f9">ff00f9</td>\r\n<td bgcolor="#ff00f8">ff00f8</td>\r\n<td bgcolor="#ff00f7">ff00f7</td>\r\n<td bgcolor="#ff00f6">ff00f6</td>\r\n<td bgcolor="#ff00f5">ff00f5</td>\r\n<td bgcolor="#ff00f4">ff00f4</td>\r\n<td bgcolor="#ff00f3">ff00f3</td>\r\n<td bgcolor="#ff00f2">ff00f2</td>\r\n<td bgcolor="#ff00f1">ff00f1</td>\r\n<td bgcolor="#ff00f0">ff00f0</td>\r\n<td bgcolor="#ff00ef">ff00ef</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ff00ee">ff00ee</td>\r\n<td bgcolor="#ff00ed">ff00ed</td>\r\n<td bgcolor="#ff00ec">ff00ec</td>\r\n<td bgcolor="#ff00eb">ff00eb</td>\r\n<td bgcolor="#ff00ea">ff00ea</td>\r\n<td bgcolor="#ff00e9">ff00e9</td>\r\n<td bgcolor="#ff00e8">ff00e8</td>\r\n<td bgcolor="#ff00e7">ff00e7</td>\r\n<td bgcolor="#ff00e6">ff00e6</td>\r\n<td bgcolor="#ff00e5">ff00e5</td>\r\n<td bgcolor="#ff00e4">ff00e4</td>\r\n<td bgcolor="#ff00e3">ff00e3</td>\r\n<td bgcolor="#ff00e2">ff00e2</td>\r\n<td bgcolor="#ff00e1">ff00e1</td>\r\n<td bgcolor="#ff00e0">ff00e0</td>\r\n<td bgcolor="#ff00df">ff00df</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ff00de">ff00de</td>\r\n<td bgcolor="#ff00dd">ff00dd</td>\r\n<td bgcolor="#ff00dc">ff00dc</td>\r\n<td bgcolor="#ff00db">ff00db</td>\r\n<td bgcolor="#ff00da">ff00da</td>\r\n<td bgcolor="#ff00d9">ff00d9</td>\r\n<td bgcolor="#ff00d8">ff00d8</td>\r\n<td bgcolor="#ff00d7">ff00d7</td>\r\n<td bgcolor="#ff00d6">ff00d6</td>\r\n<td bgcolor="#ff00d5">ff00d5</td>\r\n<td bgcolor="#ff00d4">ff00d4</td>\r\n<td bgcolor="#ff00d3">ff00d3</td>\r\n<td bgcolor="#ff00d2">ff00d2</td>\r\n<td bgcolor="#ff00d1">ff00d1</td>\r\n<td bgcolor="#ff00d0">ff00d0</td>\r\n<td bgcolor="#ff00cf">ff00cf</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ff00ce">ff00ce</td>\r\n<td bgcolor="#ff00cd">ff00cd</td>\r\n<td bgcolor="#ff00cc">ff00cc</td>\r\n<td bgcolor="#ff00cb">ff00cb</td>\r\n<td bgcolor="#ff00ca">ff00ca</td>\r\n<td bgcolor="#ff00c9">ff00c9</td>\r\n<td bgcolor="#ff00c8">ff00c8</td>\r\n<td bgcolor="#ff00c7">ff00c7</td>\r\n<td bgcolor="#ff00c6">ff00c6</td>\r\n<td bgcolor="#ff00c5">ff00c5</td>\r\n<td bgcolor="#ff00c4">ff00c4</td>\r\n<td bgcolor="#ff00c3">ff00c3</td>\r\n<td bgcolor="#ff00c2">ff00c2</td>\r\n<td bgcolor="#ff00c1">ff00c1</td>\r\n<td bgcolor="#ff00c0">ff00c0</td>\r\n<td bgcolor="#ff00bf">ff00bf</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ff00be">ff00be</td>\r\n<td bgcolor="#ff00bd">ff00bd</td>\r\n<td bgcolor="#ff00bc">ff00bc</td>\r\n<td bgcolor="#ff00bb">ff00bb</td>\r\n<td bgcolor="#ff00ba">ff00ba</td>\r\n<td bgcolor="#ff00b9">ff00b9</td>\r\n<td bgcolor="#ff00b8">ff00b8</td>\r\n<td bgcolor="#ff00b7">ff00b7</td>\r\n<td bgcolor="#ff00b6">ff00b6</td>\r\n<td bgcolor="#ff00b5">ff00b5</td>\r\n<td bgcolor="#ff00b4">ff00b4</td>\r\n<td bgcolor="#ff00b3">ff00b3</td>\r\n<td bgcolor="#ff00b2">ff00b2</td>\r\n<td bgcolor="#ff00b1">ff00b1</td>\r\n<td bgcolor="#ff00b0">ff00b0</td>\r\n<td bgcolor="#ff00af">ff00af</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ff00ae">ff00ae</td>\r\n<td bgcolor="#ff00ad">ff00ad</td>\r\n<td bgcolor="#ff00ac">ff00ac</td>\r\n<td bgcolor="#ff00ab">ff00ab</td>\r\n<td bgcolor="#ff00aa">ff00aa</td>\r\n<td bgcolor="#ff00a9">ff00a9</td>\r\n<td bgcolor="#ff00a8">ff00a8</td>\r\n<td bgcolor="#ff00a7">ff00a7</td>\r\n<td bgcolor="#ff00a6">ff00a6</td>\r\n<td bgcolor="#ff00a5">ff00a5</td>\r\n<td bgcolor="#ff00a4">ff00a4</td>\r\n<td bgcolor="#ff00a3">ff00a3</td>\r\n<td bgcolor="#ff00a2">ff00a2</td>\r\n<td bgcolor="#ff00a1">ff00a1</td>\r\n<td bgcolor="#ff00a0">ff00a0</td>\r\n<td bgcolor="#ff009f">ff009f</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ff009e">ff009e</td>\r\n<td bgcolor="#ff009d">ff009d</td>\r\n<td bgcolor="#ff009c">ff009c</td>\r\n<td bgcolor="#ff009b">ff009b</td>\r\n<td bgcolor="#ff009a">ff009a</td>\r\n<td bgcolor="#ff0099">ff0099</td>\r\n<td bgcolor="#ff0098">ff0098</td>\r\n<td bgcolor="#ff0097">ff0097</td>\r\n<td bgcolor="#ff0096">ff0096</td>\r\n<td bgcolor="#ff0095">ff0095</td>\r\n<td bgcolor="#ff0094">ff0094</td>\r\n<td bgcolor="#ff0093">ff0093</td>\r\n<td bgcolor="#ff0092">ff0092</td>\r\n<td bgcolor="#ff0091">ff0091</td>\r\n<td bgcolor="#ff0090">ff0090</td>\r\n<td bgcolor="#ff008f">ff008f</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ff008e">ff008e</td>\r\n<td bgcolor="#ff008d">ff008d</td>\r\n<td bgcolor="#ff008c">ff008c</td>\r\n<td bgcolor="#ff008b">ff008b</td>\r\n<td bgcolor="#ff008a">ff008a</td>\r\n<td bgcolor="#ff0089">ff0089</td>\r\n<td bgcolor="#ff0088">ff0088</td>\r\n<td bgcolor="#ff0087">ff0087</td>\r\n<td bgcolor="#ff0086">ff0086</td>\r\n<td bgcolor="#ff0085">ff0085</td>\r\n<td bgcolor="#ff0084">ff0084</td>\r\n<td bgcolor="#ff0083">ff0083</td>\r\n<td bgcolor="#ff0082">ff0082</td>\r\n<td bgcolor="#ff0081">ff0081</td>\r\n<td bgcolor="#ff0080">ff0080</td>\r\n<td bgcolor="#ff007f">ff007f</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ff007e">ff007e</td>\r\n<td bgcolor="#ff007d">ff007d</td>\r\n<td bgcolor="#ff007c">ff007c</td>\r\n<td bgcolor="#ff007b">ff007b</td>\r\n<td bgcolor="#ff007a">ff007a</td>\r\n<td bgcolor="#ff0079">ff0079</td>\r\n<td bgcolor="#ff0078">ff0078</td>\r\n<td bgcolor="#ff0077">ff0077</td>\r\n<td bgcolor="#ff0076">ff0076</td>\r\n<td bgcolor="#ff0075">ff0075</td>\r\n<td bgcolor="#ff0074">ff0074</td>\r\n<td bgcolor="#ff0073">ff0073</td>\r\n<td bgcolor="#ff0072">ff0072</td>\r\n<td bgcolor="#ff0071">ff0071</td>\r\n<td bgcolor="#ff0070">ff0070</td>\r\n<td bgcolor="#ff006f">ff006f</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ff006e">ff006e</td>\r\n<td bgcolor="#ff006d">ff006d</td>\r\n<td bgcolor="#ff006c">ff006c</td>\r\n<td bgcolor="#ff006b">ff006b</td>\r\n<td bgcolor="#ff006a">ff006a</td>\r\n<td bgcolor="#ff0069">ff0069</td>\r\n<td bgcolor="#ff0068">ff0068</td>\r\n<td bgcolor="#ff0067">ff0067</td>\r\n<td bgcolor="#ff0066">ff0066</td>\r\n<td bgcolor="#ff0065">ff0065</td>\r\n<td bgcolor="#ff0064">ff0064</td>\r\n<td bgcolor="#ff0063">ff0063</td>\r\n<td bgcolor="#ff0062">ff0062</td>\r\n<td bgcolor="#ff0061">ff0061</td>\r\n<td bgcolor="#ff0060">ff0060</td>\r\n<td bgcolor="#ff005f">ff005f</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ff005e">ff005e</td>\r\n<td bgcolor="#ff005d">ff005d</td>\r\n<td bgcolor="#ff005c">ff005c</td>\r\n<td bgcolor="#ff005b">ff005b</td>\r\n<td bgcolor="#ff005a">ff005a</td>\r\n<td bgcolor="#ff0059">ff0059</td>\r\n<td bgcolor="#ff0058">ff0058</td>\r\n<td bgcolor="#ff0057">ff0057</td>\r\n<td bgcolor="#ff0056">ff0056</td>\r\n<td bgcolor="#ff0055">ff0055</td>\r\n<td bgcolor="#ff0054">ff0054</td>\r\n<td bgcolor="#ff0053">ff0053</td>\r\n<td bgcolor="#ff0052">ff0052</td>\r\n<td bgcolor="#ff0051">ff0051</td>\r\n<td bgcolor="#ff0050">ff0050</td>\r\n<td bgcolor="#ff004f">ff004f</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ff004e">ff004e</td>\r\n<td bgcolor="#ff004d">ff004d</td>\r\n<td bgcolor="#ff004c">ff004c</td>\r\n<td bgcolor="#ff004b">ff004b</td>\r\n<td bgcolor="#ff004a">ff004a</td>\r\n<td bgcolor="#ff0049">ff0049</td>\r\n<td bgcolor="#ff0048">ff0048</td>\r\n<td bgcolor="#ff0047">ff0047</td>\r\n<td bgcolor="#ff0046">ff0046</td>\r\n<td bgcolor="#ff0045">ff0045</td>\r\n<td bgcolor="#ff0044">ff0044</td>\r\n<td bgcolor="#ff0043">ff0043</td>\r\n<td bgcolor="#ff0042">ff0042</td>\r\n<td bgcolor="#ff0041">ff0041</td>\r\n<td bgcolor="#ff0040">ff0040</td>\r\n<td bgcolor="#ff003f">ff003f</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ff003e">ff003e</td>\r\n<td bgcolor="#ff003d">ff003d</td>\r\n<td bgcolor="#ff003c">ff003c</td>\r\n<td bgcolor="#ff003b">ff003b</td>\r\n<td bgcolor="#ff003a">ff003a</td>\r\n<td bgcolor="#ff0039">ff0039</td>\r\n<td bgcolor="#ff0038">ff0038</td>\r\n<td bgcolor="#ff0037">ff0037</td>\r\n<td bgcolor="#ff0036">ff0036</td>\r\n<td bgcolor="#ff0035">ff0035</td>\r\n<td bgcolor="#ff0034">ff0034</td>\r\n<td bgcolor="#ff0033">ff0033</td>\r\n<td bgcolor="#ff0032">ff0032</td>\r\n<td bgcolor="#ff0031">ff0031</td>\r\n<td bgcolor="#ff0030">ff0030</td>\r\n<td bgcolor="#ff002f">ff002f</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ff002e">ff002e</td>\r\n<td bgcolor="#ff002d">ff002d</td>\r\n<td bgcolor="#ff002c">ff002c</td>\r\n<td bgcolor="#ff002b">ff002b</td>\r\n<td bgcolor="#ff002a">ff002a</td>\r\n<td bgcolor="#ff0029">ff0029</td>\r\n<td bgcolor="#ff0028">ff0028</td>\r\n<td bgcolor="#ff0027">ff0027</td>\r\n<td bgcolor="#ff0026">ff0026</td>\r\n<td bgcolor="#ff0025">ff0025</td>\r\n<td bgcolor="#ff0024">ff0024</td>\r\n<td bgcolor="#ff0023">ff0023</td>\r\n<td bgcolor="#ff0022">ff0022</td>\r\n<td bgcolor="#ff0021">ff0021</td>\r\n<td bgcolor="#ff0020">ff0020</td>\r\n<td bgcolor="#ff001f">ff001f</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ff001e">ff001e</td>\r\n<td bgcolor="#ff001d">ff001d</td>\r\n<td bgcolor="#ff001c">ff001c</td>\r\n<td bgcolor="#ff001b">ff001b</td>\r\n<td bgcolor="#ff001a">ff001a</td>\r\n<td bgcolor="#ff0019">ff0019</td>\r\n<td bgcolor="#ff0018">ff0018</td>\r\n<td bgcolor="#ff0017">ff0017</td>\r\n<td bgcolor="#ff0016">ff0016</td>\r\n<td bgcolor="#ff0015">ff0015</td>\r\n<td bgcolor="#ff0014">ff0014</td>\r\n<td bgcolor="#ff0013">ff0013</td>\r\n<td bgcolor="#ff0012">ff0012</td>\r\n<td bgcolor="#ff0011">ff0011</td>\r\n<td bgcolor="#ff0010">ff0010</td>\r\n<td bgcolor="#ff000f">ff000f</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ff000e">ff000e</td>\r\n<td bgcolor="#ff000d">ff000d</td>\r\n<td bgcolor="#ff000c">ff000c</td>\r\n<td bgcolor="#ff000b">ff000b</td>\r\n<td bgcolor="#ff000a">ff000a</td>\r\n<td bgcolor="#ff0009">ff0009</td>\r\n<td bgcolor="#ff0008">ff0008</td>\r\n<td bgcolor="#ff0007">ff0007</td>\r\n<td bgcolor="#ff0006">ff0006</td>\r\n<td bgcolor="#ff0005">ff0005</td>\r\n<td bgcolor="#ff0004">ff0004</td>\r\n<td bgcolor="#ff0003">ff0003</td>\r\n<td bgcolor="#ff0002">ff0002</td>\r\n<td bgcolor="#ff0001">ff0001</td>\r\n<td bgcolor="#ff0000">ff0000</td>\r\n<td bgcolor="#ff0000">ff0000</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ff0100">ff0100</td>\r\n<td bgcolor="#ff0200">ff0200</td>\r\n<td bgcolor="#ff0300">ff0300</td>\r\n<td bgcolor="#ff0400">ff0400</td>\r\n<td bgcolor="#ff0500">ff0500</td>\r\n<td bgcolor="#ff0600">ff0600</td>\r\n<td bgcolor="#ff0700">ff0700</td>\r\n<td bgcolor="#ff0800">ff0800</td>\r\n<td bgcolor="#ff0900">ff0900</td>\r\n<td bgcolor="#ff0a00">ff0a00</td>\r\n<td bgcolor="#ff0b00">ff0b00</td>\r\n<td bgcolor="#ff0c00">ff0c00</td>\r\n<td bgcolor="#ff0d00">ff0d00</td>\r\n<td bgcolor="#ff0e00">ff0e00</td>\r\n<td bgcolor="#ff0f00">ff0f00</td>\r\n<td bgcolor="#ff1000">ff1000</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ff1100">ff1100</td>\r\n<td bgcolor="#ff1200">ff1200</td>\r\n<td bgcolor="#ff1300">ff1300</td>\r\n<td bgcolor="#ff1400">ff1400</td>\r\n<td bgcolor="#ff1500">ff1500</td>\r\n<td bgcolor="#ff1600">ff1600</td>\r\n<td bgcolor="#ff1700">ff1700</td>\r\n<td bgcolor="#ff1800">ff1800</td>\r\n<td bgcolor="#ff1900">ff1900</td>\r\n<td bgcolor="#ff1a00">ff1a00</td>\r\n<td bgcolor="#ff1b00">ff1b00</td>\r\n<td bgcolor="#ff1c00">ff1c00</td>\r\n<td bgcolor="#ff1d00">ff1d00</td>\r\n<td bgcolor="#ff1e00">ff1e00</td>\r\n<td bgcolor="#ff1f00">ff1f00</td>\r\n<td bgcolor="#ff2000">ff2000</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ff2100">ff2100</td>\r\n<td bgcolor="#ff2200">ff2200</td>\r\n<td bgcolor="#ff2300">ff2300</td>\r\n<td bgcolor="#ff2400">ff2400</td>\r\n<td bgcolor="#ff2500">ff2500</td>\r\n<td bgcolor="#ff2600">ff2600</td>\r\n<td bgcolor="#ff2700">ff2700</td>\r\n<td bgcolor="#ff2800">ff2800</td>\r\n<td bgcolor="#ff2900">ff2900</td>\r\n<td bgcolor="#ff2a00">ff2a00</td>\r\n<td bgcolor="#ff2b00">ff2b00</td>\r\n<td bgcolor="#ff2c00">ff2c00</td>\r\n<td bgcolor="#ff2d00">ff2d00</td>\r\n<td bgcolor="#ff2e00">ff2e00</td>\r\n<td bgcolor="#ff2f00">ff2f00</td>\r\n<td bgcolor="#ff3000">ff3000</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ff3100">ff3100</td>\r\n<td bgcolor="#ff3200">ff3200</td>\r\n<td bgcolor="#ff3300">ff3300</td>\r\n<td bgcolor="#ff3400">ff3400</td>\r\n<td bgcolor="#ff3500">ff3500</td>\r\n<td bgcolor="#ff3600">ff3600</td>\r\n<td bgcolor="#ff3700">ff3700</td>\r\n<td bgcolor="#ff3800">ff3800</td>\r\n<td bgcolor="#ff3900">ff3900</td>\r\n<td bgcolor="#ff3a00">ff3a00</td>\r\n<td bgcolor="#ff3b00">ff3b00</td>\r\n<td bgcolor="#ff3c00">ff3c00</td>\r\n<td bgcolor="#ff3d00">ff3d00</td>\r\n<td bgcolor="#ff3e00">ff3e00</td>\r\n<td bgcolor="#ff3f00">ff3f00</td>\r\n<td bgcolor="#ff4000">ff4000</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ff4100">ff4100</td>\r\n<td bgcolor="#ff4200">ff4200</td>\r\n<td bgcolor="#ff4300">ff4300</td>\r\n<td bgcolor="#ff4400">ff4400</td>\r\n<td bgcolor="#ff4500">ff4500</td>\r\n<td bgcolor="#ff4600">ff4600</td>\r\n<td bgcolor="#ff4700">ff4700</td>\r\n<td bgcolor="#ff4800">ff4800</td>\r\n<td bgcolor="#ff4900">ff4900</td>\r\n<td bgcolor="#ff4a00">ff4a00</td>\r\n<td bgcolor="#ff4b00">ff4b00</td>\r\n<td bgcolor="#ff4c00">ff4c00</td>\r\n<td bgcolor="#ff4d00">ff4d00</td>\r\n<td bgcolor="#ff4e00">ff4e00</td>\r\n<td bgcolor="#ff4f00">ff4f00</td>\r\n<td bgcolor="#ff5000">ff5000</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ff5100">ff5100</td>\r\n<td bgcolor="#ff5200">ff5200</td>\r\n<td bgcolor="#ff5300">ff5300</td>\r\n<td bgcolor="#ff5400">ff5400</td>\r\n<td bgcolor="#ff5500">ff5500</td>\r\n<td bgcolor="#ff5600">ff5600</td>\r\n<td bgcolor="#ff5700">ff5700</td>\r\n<td bgcolor="#ff5800">ff5800</td>\r\n<td bgcolor="#ff5900">ff5900</td>\r\n<td bgcolor="#ff5a00">ff5a00</td>\r\n<td bgcolor="#ff5b00">ff5b00</td>\r\n<td bgcolor="#ff5c00">ff5c00</td>\r\n<td bgcolor="#ff5d00">ff5d00</td>\r\n<td bgcolor="#ff5e00">ff5e00</td>\r\n<td bgcolor="#ff5f00">ff5f00</td>\r\n<td bgcolor="#ff6000">ff6000</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ff6100">ff6100</td>\r\n<td bgcolor="#ff6200">ff6200</td>\r\n<td bgcolor="#ff6300">ff6300</td>\r\n<td bgcolor="#ff6400">ff6400</td>\r\n<td bgcolor="#ff6500">ff6500</td>\r\n<td bgcolor="#ff6600">ff6600</td>\r\n<td bgcolor="#ff6700">ff6700</td>\r\n<td bgcolor="#ff6800">ff6800</td>\r\n<td bgcolor="#ff6900">ff6900</td>\r\n<td bgcolor="#ff6a00">ff6a00</td>\r\n<td bgcolor="#ff6b00">ff6b00</td>\r\n<td bgcolor="#ff6c00">ff6c00</td>\r\n<td bgcolor="#ff6d00">ff6d00</td>\r\n<td bgcolor="#ff6e00">ff6e00</td>\r\n<td bgcolor="#ff6f00">ff6f00</td>\r\n<td bgcolor="#ff7000">ff7000</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ff7100">ff7100</td>\r\n<td bgcolor="#ff7200">ff7200</td>\r\n<td bgcolor="#ff7300">ff7300</td>\r\n<td bgcolor="#ff7400">ff7400</td>\r\n<td bgcolor="#ff7500">ff7500</td>\r\n<td bgcolor="#ff7600">ff7600</td>\r\n<td bgcolor="#ff7700">ff7700</td>\r\n<td bgcolor="#ff7800">ff7800</td>\r\n<td bgcolor="#ff7900">ff7900</td>\r\n<td bgcolor="#ff7a00">ff7a00</td>\r\n<td bgcolor="#ff7b00">ff7b00</td>\r\n<td bgcolor="#ff7c00">ff7c00</td>\r\n<td bgcolor="#ff7d00">ff7d00</td>\r\n<td bgcolor="#ff7e00">ff7e00</td>\r\n<td bgcolor="#ff7f00">ff7f00</td>\r\n<td bgcolor="#ff8000">ff8000</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ff8100">ff8100</td>\r\n<td bgcolor="#ff8200">ff8200</td>\r\n<td bgcolor="#ff8300">ff8300</td>\r\n<td bgcolor="#ff8400">ff8400</td>\r\n<td bgcolor="#ff8500">ff8500</td>\r\n<td bgcolor="#ff8600">ff8600</td>\r\n<td bgcolor="#ff8700">ff8700</td>\r\n<td bgcolor="#ff8800">ff8800</td>\r\n<td bgcolor="#ff8900">ff8900</td>\r\n<td bgcolor="#ff8a00">ff8a00</td>\r\n<td bgcolor="#ff8b00">ff8b00</td>\r\n<td bgcolor="#ff8c00">ff8c00</td>\r\n<td bgcolor="#ff8d00">ff8d00</td>\r\n<td bgcolor="#ff8e00">ff8e00</td>\r\n<td bgcolor="#ff8f00">ff8f00</td>\r\n<td bgcolor="#ff9000">ff9000</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ff9100">ff9100</td>\r\n<td bgcolor="#ff9200">ff9200</td>\r\n<td bgcolor="#ff9300">ff9300</td>\r\n<td bgcolor="#ff9400">ff9400</td>\r\n<td bgcolor="#ff9500">ff9500</td>\r\n<td bgcolor="#ff9600">ff9600</td>\r\n<td bgcolor="#ff9700">ff9700</td>\r\n<td bgcolor="#ff9800">ff9800</td>\r\n<td bgcolor="#ff9900">ff9900</td>\r\n<td bgcolor="#ff9a00">ff9a00</td>\r\n<td bgcolor="#ff9b00">ff9b00</td>\r\n<td bgcolor="#ff9c00">ff9c00</td>\r\n<td bgcolor="#ff9d00">ff9d00</td>\r\n<td bgcolor="#ff9e00">ff9e00</td>\r\n<td bgcolor="#ff9f00">ff9f00</td>\r\n<td bgcolor="#ffa000">ffa000</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ffa100">ffa100</td>\r\n<td bgcolor="#ffa200">ffa200</td>\r\n<td bgcolor="#ffa300">ffa300</td>\r\n<td bgcolor="#ffa400">ffa400</td>\r\n<td bgcolor="#ffa500">ffa500</td>\r\n<td bgcolor="#ffa600">ffa600</td>\r\n<td bgcolor="#ffa700">ffa700</td>\r\n<td bgcolor="#ffa800">ffa800</td>\r\n<td bgcolor="#ffa900">ffa900</td>\r\n<td bgcolor="#ffaa00">ffaa00</td>\r\n<td bgcolor="#ffab00">ffab00</td>\r\n<td bgcolor="#ffac00">ffac00</td>\r\n<td bgcolor="#ffad00">ffad00</td>\r\n<td bgcolor="#ffae00">ffae00</td>\r\n<td bgcolor="#ffaf00">ffaf00</td>\r\n<td bgcolor="#ffb000">ffb000</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ffb100">ffb100</td>\r\n<td bgcolor="#ffb200">ffb200</td>\r\n<td bgcolor="#ffb300">ffb300</td>\r\n<td bgcolor="#ffb400">ffb400</td>\r\n<td bgcolor="#ffb500">ffb500</td>\r\n<td bgcolor="#ffb600">ffb600</td>\r\n<td bgcolor="#ffb700">ffb700</td>\r\n<td bgcolor="#ffb800">ffb800</td>\r\n<td bgcolor="#ffb900">ffb900</td>\r\n<td bgcolor="#ffba00">ffba00</td>\r\n<td bgcolor="#ffbb00">ffbb00</td>\r\n<td bgcolor="#ffbc00">ffbc00</td>\r\n<td bgcolor="#ffbd00">ffbd00</td>\r\n<td bgcolor="#ffbe00">ffbe00</td>\r\n<td bgcolor="#ffbf00">ffbf00</td>\r\n<td bgcolor="#ffc000">ffc000</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ffc100">ffc100</td>\r\n<td bgcolor="#ffc200">ffc200</td>\r\n<td bgcolor="#ffc300">ffc300</td>\r\n<td bgcolor="#ffc400">ffc400</td>\r\n<td bgcolor="#ffc500">ffc500</td>\r\n<td bgcolor="#ffc600">ffc600</td>\r\n<td bgcolor="#ffc700">ffc700</td>\r\n<td bgcolor="#ffc800">ffc800</td>\r\n<td bgcolor="#ffc900">ffc900</td>\r\n<td bgcolor="#ffca00">ffca00</td>\r\n<td bgcolor="#ffcb00">ffcb00</td>\r\n<td bgcolor="#ffcc00">ffcc00</td>\r\n<td bgcolor="#ffcd00">ffcd00</td>\r\n<td bgcolor="#ffce00">ffce00</td>\r\n<td bgcolor="#ffcf00">ffcf00</td>\r\n<td bgcolor="#ffd000">ffd000</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ffd100">ffd100</td>\r\n<td bgcolor="#ffd200">ffd200</td>\r\n<td bgcolor="#ffd300">ffd300</td>\r\n<td bgcolor="#ffd400">ffd400</td>\r\n<td bgcolor="#ffd500">ffd500</td>\r\n<td bgcolor="#ffd600">ffd600</td>\r\n<td bgcolor="#ffd700">ffd700</td>\r\n<td bgcolor="#ffd800">ffd800</td>\r\n<td bgcolor="#ffd900">ffd900</td>\r\n<td bgcolor="#ffda00">ffda00</td>\r\n<td bgcolor="#ffdb00">ffdb00</td>\r\n<td bgcolor="#ffdc00">ffdc00</td>\r\n<td bgcolor="#ffdd00">ffdd00</td>\r\n<td bgcolor="#ffde00">ffde00</td>\r\n<td bgcolor="#ffdf00">ffdf00</td>\r\n<td bgcolor="#ffe000">ffe000</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ffe100">ffe100</td>\r\n<td bgcolor="#ffe200">ffe200</td>\r\n<td bgcolor="#ffe300">ffe300</td>\r\n<td bgcolor="#ffe400">ffe400</td>\r\n<td bgcolor="#ffe500">ffe500</td>\r\n<td bgcolor="#ffe600">ffe600</td>\r\n<td bgcolor="#ffe700">ffe700</td>\r\n<td bgcolor="#ffe800">ffe800</td>\r\n<td bgcolor="#ffe900">ffe900</td>\r\n<td bgcolor="#ffea00">ffea00</td>\r\n<td bgcolor="#ffeb00">ffeb00</td>\r\n<td bgcolor="#ffec00">ffec00</td>\r\n<td bgcolor="#ffed00">ffed00</td>\r\n<td bgcolor="#ffee00">ffee00</td>\r\n<td bgcolor="#ffef00">ffef00</td>\r\n<td bgcolor="#fff000">fff000</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#fff100">fff100</td>\r\n<td bgcolor="#fff200">fff200</td>\r\n<td bgcolor="#fff300">fff300</td>\r\n<td bgcolor="#fff400">fff400</td>\r\n<td bgcolor="#fff500">fff500</td>\r\n<td bgcolor="#fff600">fff600</td>\r\n<td bgcolor="#fff700">fff700</td>\r\n<td bgcolor="#fff800">fff800</td>\r\n<td bgcolor="#fff900">fff900</td>\r\n<td bgcolor="#fffa00">fffa00</td>\r\n<td bgcolor="#fffb00">fffb00</td>\r\n<td bgcolor="#fffc00">fffc00</td>\r\n<td bgcolor="#fffd00">fffd00</td>\r\n<td bgcolor="#fffe00">fffe00</td>\r\n<td bgcolor="#ffff00">ffff00</td>\r\n<td bgcolor="#feff00">feff00</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#fdff00">fdff00</td>\r\n<td bgcolor="#fcff00">fcff00</td>\r\n<td bgcolor="#fbff00">fbff00</td>\r\n<td bgcolor="#faff00">faff00</td>\r\n<td bgcolor="#f9ff00">f9ff00</td>\r\n<td bgcolor="#f8ff00">f8ff00</td>\r\n<td bgcolor="#f7ff00">f7ff00</td>\r\n<td bgcolor="#f6ff00">f6ff00</td>\r\n<td bgcolor="#f5ff00">f5ff00</td>\r\n<td bgcolor="#f4ff00">f4ff00</td>\r\n<td bgcolor="#f3ff00">f3ff00</td>\r\n<td bgcolor="#f2ff00">f2ff00</td>\r\n<td bgcolor="#f1ff00">f1ff00</td>\r\n<td bgcolor="#f0ff00">f0ff00</td>\r\n<td bgcolor="#efff00">efff00</td>\r\n<td bgcolor="#eeff00">eeff00</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#edff00">edff00</td>\r\n<td bgcolor="#ecff00">ecff00</td>\r\n<td bgcolor="#ebff00">ebff00</td>\r\n<td bgcolor="#eaff00">eaff00</td>\r\n<td bgcolor="#e9ff00">e9ff00</td>\r\n<td bgcolor="#e8ff00">e8ff00</td>\r\n<td bgcolor="#e7ff00">e7ff00</td>\r\n<td bgcolor="#e6ff00">e6ff00</td>\r\n<td bgcolor="#e5ff00">e5ff00</td>\r\n<td bgcolor="#e4ff00">e4ff00</td>\r\n<td bgcolor="#e3ff00">e3ff00</td>\r\n<td bgcolor="#e2ff00">e2ff00</td>\r\n<td bgcolor="#e1ff00">e1ff00</td>\r\n<td bgcolor="#e0ff00">e0ff00</td>\r\n<td bgcolor="#dfff00">dfff00</td>\r\n<td bgcolor="#deff00">deff00</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#ddff00">ddff00</td>\r\n<td bgcolor="#dcff00">dcff00</td>\r\n<td bgcolor="#dbff00">dbff00</td>\r\n<td bgcolor="#daff00">daff00</td>\r\n<td bgcolor="#d9ff00">d9ff00</td>\r\n<td bgcolor="#d8ff00">d8ff00</td>\r\n<td bgcolor="#d7ff00">d7ff00</td>\r\n<td bgcolor="#d6ff00">d6ff00</td>\r\n<td bgcolor="#d5ff00">d5ff00</td>\r\n<td bgcolor="#d4ff00">d4ff00</td>\r\n<td bgcolor="#d3ff00">d3ff00</td>\r\n<td bgcolor="#d2ff00">d2ff00</td>\r\n<td bgcolor="#d1ff00">d1ff00</td>\r\n<td bgcolor="#d0ff00">d0ff00</td>\r\n<td bgcolor="#cfff00">cfff00</td>\r\n<td bgcolor="#ceff00">ceff00</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#cdff00">cdff00</td>\r\n<td bgcolor="#ccff00">ccff00</td>\r\n<td bgcolor="#cbff00">cbff00</td>\r\n<td bgcolor="#caff00">caff00</td>\r\n<td bgcolor="#c9ff00">c9ff00</td>\r\n<td bgcolor="#c8ff00">c8ff00</td>\r\n<td bgcolor="#c7ff00">c7ff00</td>\r\n<td bgcolor="#c6ff00">c6ff00</td>\r\n<td bgcolor="#c5ff00">c5ff00</td>\r\n<td bgcolor="#c4ff00">c4ff00</td>\r\n<td bgcolor="#c3ff00">c3ff00</td>\r\n<td bgcolor="#c2ff00">c2ff00</td>\r\n<td bgcolor="#c1ff00">c1ff00</td>\r\n<td bgcolor="#c0ff00">c0ff00</td>\r\n<td bgcolor="#bfff00">bfff00</td>\r\n<td bgcolor="#beff00">beff00</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#bdff00">bdff00</td>\r\n<td bgcolor="#bcff00">bcff00</td>\r\n<td bgcolor="#bbff00">bbff00</td>\r\n<td bgcolor="#baff00">baff00</td>\r\n<td bgcolor="#b9ff00">b9ff00</td>\r\n<td bgcolor="#b8ff00">b8ff00</td>\r\n<td bgcolor="#b7ff00">b7ff00</td>\r\n<td bgcolor="#b6ff00">b6ff00</td>\r\n<td bgcolor="#b5ff00">b5ff00</td>\r\n<td bgcolor="#b4ff00">b4ff00</td>\r\n<td bgcolor="#b3ff00">b3ff00</td>\r\n<td bgcolor="#b2ff00">b2ff00</td>\r\n<td bgcolor="#b1ff00">b1ff00</td>\r\n<td bgcolor="#b0ff00">b0ff00</td>\r\n<td bgcolor="#afff00">afff00</td>\r\n<td bgcolor="#aeff00">aeff00</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#adff00">adff00</td>\r\n<td bgcolor="#acff00">acff00</td>\r\n<td bgcolor="#abff00">abff00</td>\r\n<td bgcolor="#aaff00">aaff00</td>\r\n<td bgcolor="#a9ff00">a9ff00</td>\r\n<td bgcolor="#a8ff00">a8ff00</td>\r\n<td bgcolor="#a7ff00">a7ff00</td>\r\n<td bgcolor="#a6ff00">a6ff00</td>\r\n<td bgcolor="#a5ff00">a5ff00</td>\r\n<td bgcolor="#a4ff00">a4ff00</td>\r\n<td bgcolor="#a3ff00">a3ff00</td>\r\n<td bgcolor="#a2ff00">a2ff00</td>\r\n<td bgcolor="#a1ff00">a1ff00</td>\r\n<td bgcolor="#a0ff00">a0ff00</td>\r\n<td bgcolor="#9fff00">9fff00</td>\r\n<td bgcolor="#9eff00">9eff00</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#9dff00">9dff00</td>\r\n<td bgcolor="#9cff00">9cff00</td>\r\n<td bgcolor="#9bff00">9bff00</td>\r\n<td bgcolor="#9aff00">9aff00</td>\r\n<td bgcolor="#99ff00">99ff00</td>\r\n<td bgcolor="#98ff00">98ff00</td>\r\n<td bgcolor="#97ff00">97ff00</td>\r\n<td bgcolor="#96ff00">96ff00</td>\r\n<td bgcolor="#95ff00">95ff00</td>\r\n<td bgcolor="#94ff00">94ff00</td>\r\n<td bgcolor="#93ff00">93ff00</td>\r\n<td bgcolor="#92ff00">92ff00</td>\r\n<td bgcolor="#91ff00">91ff00</td>\r\n<td bgcolor="#90ff00">90ff00</td>\r\n<td bgcolor="#8fff00">8fff00</td>\r\n<td bgcolor="#8eff00">8eff00</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#8dff00">8dff00</td>\r\n<td bgcolor="#8cff00">8cff00</td>\r\n<td bgcolor="#8bff00">8bff00</td>\r\n<td bgcolor="#8aff00">8aff00</td>\r\n<td bgcolor="#89ff00">89ff00</td>\r\n<td bgcolor="#88ff00">88ff00</td>\r\n<td bgcolor="#87ff00">87ff00</td>\r\n<td bgcolor="#86ff00">86ff00</td>\r\n<td bgcolor="#85ff00">85ff00</td>\r\n<td bgcolor="#84ff00">84ff00</td>\r\n<td bgcolor="#83ff00">83ff00</td>\r\n<td bgcolor="#82ff00">82ff00</td>\r\n<td bgcolor="#81ff00">81ff00</td>\r\n<td bgcolor="#80ff00">80ff00</td>\r\n<td bgcolor="#7fff00">7fff00</td>\r\n<td bgcolor="#7eff00">7eff00</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#7dff00">7dff00</td>\r\n<td bgcolor="#7cff00">7cff00</td>\r\n<td bgcolor="#7bff00">7bff00</td>\r\n<td bgcolor="#7aff00">7aff00</td>\r\n<td bgcolor="#79ff00">79ff00</td>\r\n<td bgcolor="#78ff00">78ff00</td>\r\n<td bgcolor="#77ff00">77ff00</td>\r\n<td bgcolor="#76ff00">76ff00</td>\r\n<td bgcolor="#75ff00">75ff00</td>\r\n<td bgcolor="#74ff00">74ff00</td>\r\n<td bgcolor="#73ff00">73ff00</td>\r\n<td bgcolor="#72ff00">72ff00</td>\r\n<td bgcolor="#71ff00">71ff00</td>\r\n<td bgcolor="#70ff00">70ff00</td>\r\n<td bgcolor="#6fff00">6fff00</td>\r\n<td bgcolor="#6eff00">6eff00</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#6dff00">6dff00</td>\r\n<td bgcolor="#6cff00">6cff00</td>\r\n<td bgcolor="#6bff00">6bff00</td>\r\n<td bgcolor="#6aff00">6aff00</td>\r\n<td bgcolor="#69ff00">69ff00</td>\r\n<td bgcolor="#68ff00">68ff00</td>\r\n<td bgcolor="#67ff00">67ff00</td>\r\n<td bgcolor="#66ff00">66ff00</td>\r\n<td bgcolor="#65ff00">65ff00</td>\r\n<td bgcolor="#64ff00">64ff00</td>\r\n<td bgcolor="#63ff00">63ff00</td>\r\n<td bgcolor="#62ff00">62ff00</td>\r\n<td bgcolor="#61ff00">61ff00</td>\r\n<td bgcolor="#60ff00">60ff00</td>\r\n<td bgcolor="#5fff00">5fff00</td>\r\n<td bgcolor="#5eff00">5eff00</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#5dff00">5dff00</td>\r\n<td bgcolor="#5cff00">5cff00</td>\r\n<td bgcolor="#5bff00">5bff00</td>\r\n<td bgcolor="#5aff00">5aff00</td>\r\n<td bgcolor="#59ff00">59ff00</td>\r\n<td bgcolor="#58ff00">58ff00</td>\r\n<td bgcolor="#57ff00">57ff00</td>\r\n<td bgcolor="#56ff00">56ff00</td>\r\n<td bgcolor="#55ff00">55ff00</td>\r\n<td bgcolor="#54ff00">54ff00</td>\r\n<td bgcolor="#53ff00">53ff00</td>\r\n<td bgcolor="#52ff00">52ff00</td>\r\n<td bgcolor="#51ff00">51ff00</td>\r\n<td bgcolor="#50ff00">50ff00</td>\r\n<td bgcolor="#4fff00">4fff00</td>\r\n<td bgcolor="#4eff00">4eff00</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#4dff00">4dff00</td>\r\n<td bgcolor="#4cff00">4cff00</td>\r\n<td bgcolor="#4bff00">4bff00</td>\r\n<td bgcolor="#4aff00">4aff00</td>\r\n<td bgcolor="#49ff00">49ff00</td>\r\n<td bgcolor="#48ff00">48ff00</td>\r\n<td bgcolor="#47ff00">47ff00</td>\r\n<td bgcolor="#46ff00">46ff00</td>\r\n<td bgcolor="#45ff00">45ff00</td>\r\n<td bgcolor="#44ff00">44ff00</td>\r\n<td bgcolor="#43ff00">43ff00</td>\r\n<td bgcolor="#42ff00">42ff00</td>\r\n<td bgcolor="#41ff00">41ff00</td>\r\n<td bgcolor="#40ff00">40ff00</td>\r\n<td bgcolor="#3fff00">3fff00</td>\r\n<td bgcolor="#3eff00">3eff00</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#3dff00">3dff00</td>\r\n<td bgcolor="#3cff00">3cff00</td>\r\n<td bgcolor="#3bff00">3bff00</td>\r\n<td bgcolor="#3aff00">3aff00</td>\r\n<td bgcolor="#39ff00">39ff00</td>\r\n<td bgcolor="#38ff00">38ff00</td>\r\n<td bgcolor="#37ff00">37ff00</td>\r\n<td bgcolor="#36ff00">36ff00</td>\r\n<td bgcolor="#35ff00">35ff00</td>\r\n<td bgcolor="#34ff00">34ff00</td>\r\n<td bgcolor="#33ff00">33ff00</td>\r\n<td bgcolor="#32ff00">32ff00</td>\r\n<td bgcolor="#31ff00">31ff00</td>\r\n<td bgcolor="#30ff00">30ff00</td>\r\n<td bgcolor="#2fff00">2fff00</td>\r\n<td bgcolor="#2eff00">2eff00</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#2dff00">2dff00</td>\r\n<td bgcolor="#2cff00">2cff00</td>\r\n<td bgcolor="#2bff00">2bff00</td>\r\n<td bgcolor="#2aff00">2aff00</td>\r\n<td bgcolor="#29ff00">29ff00</td>\r\n<td bgcolor="#28ff00">28ff00</td>\r\n<td bgcolor="#27ff00">27ff00</td>\r\n<td bgcolor="#26ff00">26ff00</td>\r\n<td bgcolor="#25ff00">25ff00</td>\r\n<td bgcolor="#24ff00">24ff00</td>\r\n<td bgcolor="#23ff00">23ff00</td>\r\n<td bgcolor="#22ff00">22ff00</td>\r\n<td bgcolor="#21ff00">21ff00</td>\r\n<td bgcolor="#20ff00">20ff00</td>\r\n<td bgcolor="#1fff00">1fff00</td>\r\n<td bgcolor="#1eff00">1eff00</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#1dff00">1dff00</td>\r\n<td bgcolor="#1cff00">1cff00</td>\r\n<td bgcolor="#1bff00">1bff00</td>\r\n<td bgcolor="#1aff00">1aff00</td>\r\n<td bgcolor="#19ff00">19ff00</td>\r\n<td bgcolor="#18ff00">18ff00</td>\r\n<td bgcolor="#17ff00">17ff00</td>\r\n<td bgcolor="#16ff00">16ff00</td>\r\n<td bgcolor="#15ff00">15ff00</td>\r\n<td bgcolor="#14ff00">14ff00</td>\r\n<td bgcolor="#13ff00">13ff00</td>\r\n<td bgcolor="#12ff00">12ff00</td>\r\n<td bgcolor="#11ff00">11ff00</td>\r\n<td bgcolor="#10ff00">10ff00</td>\r\n<td bgcolor="#0fff00">0fff00</td>\r\n<td bgcolor="#0eff00">0eff00</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#0dff00">0dff00</td>\r\n<td bgcolor="#0cff00">0cff00</td>\r\n<td bgcolor="#0bff00">0bff00</td>\r\n<td bgcolor="#0aff00">0aff00</td>\r\n<td bgcolor="#09ff00">09ff00</td>\r\n<td bgcolor="#08ff00">08ff00</td>\r\n<td bgcolor="#07ff00">07ff00</td>\r\n<td bgcolor="#06ff00">06ff00</td>\r\n<td bgcolor="#05ff00">05ff00</td>\r\n<td bgcolor="#04ff00">04ff00</td>\r\n<td bgcolor="#03ff00">03ff00</td>\r\n<td bgcolor="#02ff00">02ff00</td>\r\n<td bgcolor="#01ff00">01ff00</td>\r\n<td bgcolor="#00ff00">00ff00</td>\r\n<td bgcolor="#00ff01">00ff01</td>\r\n<td bgcolor="#00ff02">00ff02</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#00ff03">00ff03</td>\r\n<td bgcolor="#00ff04">00ff04</td>\r\n<td bgcolor="#00ff05">00ff05</td>\r\n<td bgcolor="#00ff06">00ff06</td>\r\n<td bgcolor="#00ff07">00ff07</td>\r\n<td bgcolor="#00ff08">00ff08</td>\r\n<td bgcolor="#00ff09">00ff09</td>\r\n<td bgcolor="#00ff0a">00ff0a</td>\r\n<td bgcolor="#00ff0b">00ff0b</td>\r\n<td bgcolor="#00ff0c">00ff0c</td>\r\n<td bgcolor="#00ff0d">00ff0d</td>\r\n<td bgcolor="#00ff0e">00ff0e</td>\r\n<td bgcolor="#00ff0f">00ff0f</td>\r\n<td bgcolor="#00ff10">00ff10</td>\r\n<td bgcolor="#00ff11">00ff11</td>\r\n<td bgcolor="#00ff12">00ff12</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#00ff13">00ff13</td>\r\n<td bgcolor="#00ff14">00ff14</td>\r\n<td bgcolor="#00ff15">00ff15</td>\r\n<td bgcolor="#00ff16">00ff16</td>\r\n<td bgcolor="#00ff17">00ff17</td>\r\n<td bgcolor="#00ff18">00ff18</td>\r\n<td bgcolor="#00ff19">00ff19</td>\r\n<td bgcolor="#00ff1a">00ff1a</td>\r\n<td bgcolor="#00ff1b">00ff1b</td>\r\n<td bgcolor="#00ff1c">00ff1c</td>\r\n<td bgcolor="#00ff1d">00ff1d</td>\r\n<td bgcolor="#00ff1e">00ff1e</td>\r\n<td bgcolor="#00ff1f">00ff1f</td>\r\n<td bgcolor="#00ff20">00ff20</td>\r\n<td bgcolor="#00ff21">00ff21</td>\r\n<td bgcolor="#00ff22">00ff22</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#00ff23">00ff23</td>\r\n<td bgcolor="#00ff24">00ff24</td>\r\n<td bgcolor="#00ff25">00ff25</td>\r\n<td bgcolor="#00ff26">00ff26</td>\r\n<td bgcolor="#00ff27">00ff27</td>\r\n<td bgcolor="#00ff28">00ff28</td>\r\n<td bgcolor="#00ff29">00ff29</td>\r\n<td bgcolor="#00ff2a">00ff2a</td>\r\n<td bgcolor="#00ff2b">00ff2b</td>\r\n<td bgcolor="#00ff2c">00ff2c</td>\r\n<td bgcolor="#00ff2d">00ff2d</td>\r\n<td bgcolor="#00ff2e">00ff2e</td>\r\n<td bgcolor="#00ff2f">00ff2f</td>\r\n<td bgcolor="#00ff30">00ff30</td>\r\n<td bgcolor="#00ff31">00ff31</td>\r\n<td bgcolor="#00ff32">00ff32</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#00ff33">00ff33</td>\r\n<td bgcolor="#00ff34">00ff34</td>\r\n<td bgcolor="#00ff35">00ff35</td>\r\n<td bgcolor="#00ff36">00ff36</td>\r\n<td bgcolor="#00ff37">00ff37</td>\r\n<td bgcolor="#00ff38">00ff38</td>\r\n<td bgcolor="#00ff39">00ff39</td>\r\n<td bgcolor="#00ff3a">00ff3a</td>\r\n<td bgcolor="#00ff3b">00ff3b</td>\r\n<td bgcolor="#00ff3c">00ff3c</td>\r\n<td bgcolor="#00ff3d">00ff3d</td>\r\n<td bgcolor="#00ff3e">00ff3e</td>\r\n<td bgcolor="#00ff3f">00ff3f</td>\r\n<td bgcolor="#00ff40">00ff40</td>\r\n<td bgcolor="#00ff41">00ff41</td>\r\n<td bgcolor="#00ff42">00ff42</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#00ff43">00ff43</td>\r\n<td bgcolor="#00ff44">00ff44</td>\r\n<td bgcolor="#00ff45">00ff45</td>\r\n<td bgcolor="#00ff46">00ff46</td>\r\n<td bgcolor="#00ff47">00ff47</td>\r\n<td bgcolor="#00ff48">00ff48</td>\r\n<td bgcolor="#00ff49">00ff49</td>\r\n<td bgcolor="#00ff4a">00ff4a</td>\r\n<td bgcolor="#00ff4b">00ff4b</td>\r\n<td bgcolor="#00ff4c">00ff4c</td>\r\n<td bgcolor="#00ff4d">00ff4d</td>\r\n<td bgcolor="#00ff4e">00ff4e</td>\r\n<td bgcolor="#00ff4f">00ff4f</td>\r\n<td bgcolor="#00ff50">00ff50</td>\r\n<td bgcolor="#00ff51">00ff51</td>\r\n<td bgcolor="#00ff52">00ff52</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#00ff53">00ff53</td>\r\n<td bgcolor="#00ff54">00ff54</td>\r\n<td bgcolor="#00ff55">00ff55</td>\r\n<td bgcolor="#00ff56">00ff56</td>\r\n<td bgcolor="#00ff57">00ff57</td>\r\n<td bgcolor="#00ff58">00ff58</td>\r\n<td bgcolor="#00ff59">00ff59</td>\r\n<td bgcolor="#00ff5a">00ff5a</td>\r\n<td bgcolor="#00ff5b">00ff5b</td>\r\n<td bgcolor="#00ff5c">00ff5c</td>\r\n<td bgcolor="#00ff5d">00ff5d</td>\r\n<td bgcolor="#00ff5e">00ff5e</td>\r\n<td bgcolor="#00ff5f">00ff5f</td>\r\n<td bgcolor="#00ff60">00ff60</td>\r\n<td bgcolor="#00ff61">00ff61</td>\r\n<td bgcolor="#00ff62">00ff62</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#00ff63">00ff63</td>\r\n<td bgcolor="#00ff64">00ff64</td>\r\n<td bgcolor="#00ff65">00ff65</td>\r\n<td bgcolor="#00ff66">00ff66</td>\r\n<td bgcolor="#00ff67">00ff67</td>\r\n<td bgcolor="#00ff68">00ff68</td>\r\n<td bgcolor="#00ff69">00ff69</td>\r\n<td bgcolor="#00ff6a">00ff6a</td>\r\n<td bgcolor="#00ff6b">00ff6b</td>\r\n<td bgcolor="#00ff6c">00ff6c</td>\r\n<td bgcolor="#00ff6d">00ff6d</td>\r\n<td bgcolor="#00ff6e">00ff6e</td>\r\n<td bgcolor="#00ff6f">00ff6f</td>\r\n<td bgcolor="#00ff70">00ff70</td>\r\n<td bgcolor="#00ff71">00ff71</td>\r\n<td bgcolor="#00ff72">00ff72</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#00ff73">00ff73</td>\r\n<td bgcolor="#00ff74">00ff74</td>\r\n<td bgcolor="#00ff75">00ff75</td>\r\n<td bgcolor="#00ff76">00ff76</td>\r\n<td bgcolor="#00ff77">00ff77</td>\r\n<td bgcolor="#00ff78">00ff78</td>\r\n<td bgcolor="#00ff79">00ff79</td>\r\n<td bgcolor="#00ff7a">00ff7a</td>\r\n<td bgcolor="#00ff7b">00ff7b</td>\r\n<td bgcolor="#00ff7c">00ff7c</td>\r\n<td bgcolor="#00ff7d">00ff7d</td>\r\n<td bgcolor="#00ff7e">00ff7e</td>\r\n<td bgcolor="#00ff7f">00ff7f</td>\r\n<td bgcolor="#00ff80">00ff80</td>\r\n<td bgcolor="#00ff81">00ff81</td>\r\n<td bgcolor="#00ff82">00ff82</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#00ff83">00ff83</td>\r\n<td bgcolor="#00ff84">00ff84</td>\r\n<td bgcolor="#00ff85">00ff85</td>\r\n<td bgcolor="#00ff86">00ff86</td>\r\n<td bgcolor="#00ff87">00ff87</td>\r\n<td bgcolor="#00ff88">00ff88</td>\r\n<td bgcolor="#00ff89">00ff89</td>\r\n<td bgcolor="#00ff8a">00ff8a</td>\r\n<td bgcolor="#00ff8b">00ff8b</td>\r\n<td bgcolor="#00ff8c">00ff8c</td>\r\n<td bgcolor="#00ff8d">00ff8d</td>\r\n<td bgcolor="#00ff8e">00ff8e</td>\r\n<td bgcolor="#00ff8f">00ff8f</td>\r\n<td bgcolor="#00ff90">00ff90</td>\r\n<td bgcolor="#00ff91">00ff91</td>\r\n<td bgcolor="#00ff92">00ff92</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#00ff93">00ff93</td>\r\n<td bgcolor="#00ff94">00ff94</td>\r\n<td bgcolor="#00ff95">00ff95</td>\r\n<td bgcolor="#00ff96">00ff96</td>\r\n<td bgcolor="#00ff97">00ff97</td>\r\n<td bgcolor="#00ff98">00ff98</td>\r\n<td bgcolor="#00ff99">00ff99</td>\r\n<td bgcolor="#00ff9a">00ff9a</td>\r\n<td bgcolor="#00ff9b">00ff9b</td>\r\n<td bgcolor="#00ff9c">00ff9c</td>\r\n<td bgcolor="#00ff9d">00ff9d</td>\r\n<td bgcolor="#00ff9e">00ff9e</td>\r\n<td bgcolor="#00ff9f">00ff9f</td>\r\n<td bgcolor="#00ffa0">00ffa0</td>\r\n<td bgcolor="#00ffa1">00ffa1</td>\r\n<td bgcolor="#00ffa2">00ffa2</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#00ffa3">00ffa3</td>\r\n<td bgcolor="#00ffa4">00ffa4</td>\r\n<td bgcolor="#00ffa5">00ffa5</td>\r\n<td bgcolor="#00ffa6">00ffa6</td>\r\n<td bgcolor="#00ffa7">00ffa7</td>\r\n<td bgcolor="#00ffa8">00ffa8</td>\r\n<td bgcolor="#00ffa9">00ffa9</td>\r\n<td bgcolor="#00ffaa">00ffaa</td>\r\n<td bgcolor="#00ffab">00ffab</td>\r\n<td bgcolor="#00ffac">00ffac</td>\r\n<td bgcolor="#00ffad">00ffad</td>\r\n<td bgcolor="#00ffae">00ffae</td>\r\n<td bgcolor="#00ffaf">00ffaf</td>\r\n<td bgcolor="#00ffb0">00ffb0</td>\r\n<td bgcolor="#00ffb1">00ffb1</td>\r\n<td bgcolor="#00ffb2">00ffb2</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#00ffb3">00ffb3</td>\r\n<td bgcolor="#00ffb4">00ffb4</td>\r\n<td bgcolor="#00ffb5">00ffb5</td>\r\n<td bgcolor="#00ffb6">00ffb6</td>\r\n<td bgcolor="#00ffb7">00ffb7</td>\r\n<td bgcolor="#00ffb8">00ffb8</td>\r\n<td bgcolor="#00ffb9">00ffb9</td>\r\n<td bgcolor="#00ffba">00ffba</td>\r\n<td bgcolor="#00ffbb">00ffbb</td>\r\n<td bgcolor="#00ffbc">00ffbc</td>\r\n<td bgcolor="#00ffbd">00ffbd</td>\r\n<td bgcolor="#00ffbe">00ffbe</td>\r\n<td bgcolor="#00ffbf">00ffbf</td>\r\n<td bgcolor="#00ffc0">00ffc0</td>\r\n<td bgcolor="#00ffc1">00ffc1</td>\r\n<td bgcolor="#00ffc2">00ffc2</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#00ffc3">00ffc3</td>\r\n<td bgcolor="#00ffc4">00ffc4</td>\r\n<td bgcolor="#00ffc5">00ffc5</td>\r\n<td bgcolor="#00ffc6">00ffc6</td>\r\n<td bgcolor="#00ffc7">00ffc7</td>\r\n<td bgcolor="#00ffc8">00ffc8</td>\r\n<td bgcolor="#00ffc9">00ffc9</td>\r\n<td bgcolor="#00ffca">00ffca</td>\r\n<td bgcolor="#00ffcb">00ffcb</td>\r\n<td bgcolor="#00ffcc">00ffcc</td>\r\n<td bgcolor="#00ffcd">00ffcd</td>\r\n<td bgcolor="#00ffce">00ffce</td>\r\n<td bgcolor="#00ffcf">00ffcf</td>\r\n<td bgcolor="#00ffd0">00ffd0</td>\r\n<td bgcolor="#00ffd1">00ffd1</td>\r\n<td bgcolor="#00ffd2">00ffd2</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#00ffd3">00ffd3</td>\r\n<td bgcolor="#00ffd4">00ffd4</td>\r\n<td bgcolor="#00ffd5">00ffd5</td>\r\n<td bgcolor="#00ffd6">00ffd6</td>\r\n<td bgcolor="#00ffd7">00ffd7</td>\r\n<td bgcolor="#00ffd8">00ffd8</td>\r\n<td bgcolor="#00ffd9">00ffd9</td>\r\n<td bgcolor="#00ffda">00ffda</td>\r\n<td bgcolor="#00ffdb">00ffdb</td>\r\n<td bgcolor="#00ffdc">00ffdc</td>\r\n<td bgcolor="#00ffdd">00ffdd</td>\r\n<td bgcolor="#00ffde">00ffde</td>\r\n<td bgcolor="#00ffdf">00ffdf</td>\r\n<td bgcolor="#00ffe0">00ffe0</td>\r\n<td bgcolor="#00ffe1">00ffe1</td>\r\n<td bgcolor="#00ffe2">00ffe2</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#00ffe3">00ffe3</td>\r\n<td bgcolor="#00ffe4">00ffe4</td>\r\n<td bgcolor="#00ffe5">00ffe5</td>\r\n<td bgcolor="#00ffe6">00ffe6</td>\r\n<td bgcolor="#00ffe7">00ffe7</td>\r\n<td bgcolor="#00ffe8">00ffe8</td>\r\n<td bgcolor="#00ffe9">00ffe9</td>\r\n<td bgcolor="#00ffea">00ffea</td>\r\n<td bgcolor="#00ffeb">00ffeb</td>\r\n<td bgcolor="#00ffec">00ffec</td>\r\n<td bgcolor="#00ffed">00ffed</td>\r\n<td bgcolor="#00ffee">00ffee</td>\r\n<td bgcolor="#00ffef">00ffef</td>\r\n<td bgcolor="#00fff0">00fff0</td>\r\n<td bgcolor="#00fff1">00fff1</td>\r\n<td bgcolor="#00fff2">00fff2</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#00fff3">00fff3</td>\r\n<td bgcolor="#00fff4">00fff4</td>\r\n<td bgcolor="#00fff5">00fff5</td>\r\n<td bgcolor="#00fff6">00fff6</td>\r\n<td bgcolor="#00fff7">00fff7</td>\r\n<td bgcolor="#00fff8">00fff8</td>\r\n<td bgcolor="#00fff9">00fff9</td>\r\n<td bgcolor="#00fffa">00fffa</td>\r\n<td bgcolor="#00fffb">00fffb</td>\r\n<td bgcolor="#00fffc">00fffc</td>\r\n<td bgcolor="#00fffd">00fffd</td>\r\n<td bgcolor="#00fffe">00fffe</td>\r\n<td bgcolor="#00ffff">00ffff</td>\r\n<td bgcolor="#00feff">00feff</td>\r\n<td bgcolor="#00fdff">00fdff</td>\r\n<td bgcolor="#00fcff">00fcff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#00fbff">00fbff</td>\r\n<td bgcolor="#00faff">00faff</td>\r\n<td bgcolor="#00f9ff">00f9ff</td>\r\n<td bgcolor="#00f8ff">00f8ff</td>\r\n<td bgcolor="#00f7ff">00f7ff</td>\r\n<td bgcolor="#00f6ff">00f6ff</td>\r\n<td bgcolor="#00f5ff">00f5ff</td>\r\n<td bgcolor="#00f4ff">00f4ff</td>\r\n<td bgcolor="#00f3ff">00f3ff</td>\r\n<td bgcolor="#00f2ff">00f2ff</td>\r\n<td bgcolor="#00f1ff">00f1ff</td>\r\n<td bgcolor="#00f0ff">00f0ff</td>\r\n<td bgcolor="#00efff">00efff</td>\r\n<td bgcolor="#00eeff">00eeff</td>\r\n<td bgcolor="#00edff">00edff</td>\r\n<td bgcolor="#00ecff">00ecff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#00ebff">00ebff</td>\r\n<td bgcolor="#00eaff">00eaff</td>\r\n<td bgcolor="#00e9ff">00e9ff</td>\r\n<td bgcolor="#00e8ff">00e8ff</td>\r\n<td bgcolor="#00e7ff">00e7ff</td>\r\n<td bgcolor="#00e6ff">00e6ff</td>\r\n<td bgcolor="#00e5ff">00e5ff</td>\r\n<td bgcolor="#00e4ff">00e4ff</td>\r\n<td bgcolor="#00e3ff">00e3ff</td>\r\n<td bgcolor="#00e2ff">00e2ff</td>\r\n<td bgcolor="#00e1ff">00e1ff</td>\r\n<td bgcolor="#00e0ff">00e0ff</td>\r\n<td bgcolor="#00dfff">00dfff</td>\r\n<td bgcolor="#00deff">00deff</td>\r\n<td bgcolor="#00ddff">00ddff</td>\r\n<td bgcolor="#00dcff">00dcff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#00dbff">00dbff</td>\r\n<td bgcolor="#00daff">00daff</td>\r\n<td bgcolor="#00d9ff">00d9ff</td>\r\n<td bgcolor="#00d8ff">00d8ff</td>\r\n<td bgcolor="#00d7ff">00d7ff</td>\r\n<td bgcolor="#00d6ff">00d6ff</td>\r\n<td bgcolor="#00d5ff">00d5ff</td>\r\n<td bgcolor="#00d4ff">00d4ff</td>\r\n<td bgcolor="#00d3ff">00d3ff</td>\r\n<td bgcolor="#00d2ff">00d2ff</td>\r\n<td bgcolor="#00d1ff">00d1ff</td>\r\n<td bgcolor="#00d0ff">00d0ff</td>\r\n<td bgcolor="#00cfff">00cfff</td>\r\n<td bgcolor="#00ceff">00ceff</td>\r\n<td bgcolor="#00cdff">00cdff</td>\r\n<td bgcolor="#00ccff">00ccff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#00cbff">00cbff</td>\r\n<td bgcolor="#00caff">00caff</td>\r\n<td bgcolor="#00c9ff">00c9ff</td>\r\n<td bgcolor="#00c8ff">00c8ff</td>\r\n<td bgcolor="#00c7ff">00c7ff</td>\r\n<td bgcolor="#00c6ff">00c6ff</td>\r\n<td bgcolor="#00c5ff">00c5ff</td>\r\n<td bgcolor="#00c4ff">00c4ff</td>\r\n<td bgcolor="#00c3ff">00c3ff</td>\r\n<td bgcolor="#00c2ff">00c2ff</td>\r\n<td bgcolor="#00c1ff">00c1ff</td>\r\n<td bgcolor="#00c0ff">00c0ff</td>\r\n<td bgcolor="#00bfff">00bfff</td>\r\n<td bgcolor="#00beff">00beff</td>\r\n<td bgcolor="#00bdff">00bdff</td>\r\n<td bgcolor="#00bcff">00bcff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#00bbff">00bbff</td>\r\n<td bgcolor="#00baff">00baff</td>\r\n<td bgcolor="#00b9ff">00b9ff</td>\r\n<td bgcolor="#00b8ff">00b8ff</td>\r\n<td bgcolor="#00b7ff">00b7ff</td>\r\n<td bgcolor="#00b6ff">00b6ff</td>\r\n<td bgcolor="#00b5ff">00b5ff</td>\r\n<td bgcolor="#00b4ff">00b4ff</td>\r\n<td bgcolor="#00b3ff">00b3ff</td>\r\n<td bgcolor="#00b2ff">00b2ff</td>\r\n<td bgcolor="#00b1ff">00b1ff</td>\r\n<td bgcolor="#00b0ff">00b0ff</td>\r\n<td bgcolor="#00afff">00afff</td>\r\n<td bgcolor="#00aeff">00aeff</td>\r\n<td bgcolor="#00adff">00adff</td>\r\n<td bgcolor="#00acff">00acff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#00abff">00abff</td>\r\n<td bgcolor="#00aaff">00aaff</td>\r\n<td bgcolor="#00a9ff">00a9ff</td>\r\n<td bgcolor="#00a8ff">00a8ff</td>\r\n<td bgcolor="#00a7ff">00a7ff</td>\r\n<td bgcolor="#00a6ff">00a6ff</td>\r\n<td bgcolor="#00a5ff">00a5ff</td>\r\n<td bgcolor="#00a4ff">00a4ff</td>\r\n<td bgcolor="#00a3ff">00a3ff</td>\r\n<td bgcolor="#00a2ff">00a2ff</td>\r\n<td bgcolor="#00a1ff">00a1ff</td>\r\n<td bgcolor="#00a0ff">00a0ff</td>\r\n<td bgcolor="#009fff">009fff</td>\r\n<td bgcolor="#009eff">009eff</td>\r\n<td bgcolor="#009dff">009dff</td>\r\n<td bgcolor="#009cff">009cff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#009bff">009bff</td>\r\n<td bgcolor="#009aff">009aff</td>\r\n<td bgcolor="#0099ff">0099ff</td>\r\n<td bgcolor="#0098ff">0098ff</td>\r\n<td bgcolor="#0097ff">0097ff</td>\r\n<td bgcolor="#0096ff">0096ff</td>\r\n<td bgcolor="#0095ff">0095ff</td>\r\n<td bgcolor="#0094ff">0094ff</td>\r\n<td bgcolor="#0093ff">0093ff</td>\r\n<td bgcolor="#0092ff">0092ff</td>\r\n<td bgcolor="#0091ff">0091ff</td>\r\n<td bgcolor="#0090ff">0090ff</td>\r\n<td bgcolor="#008fff">008fff</td>\r\n<td bgcolor="#008eff">008eff</td>\r\n<td bgcolor="#008dff">008dff</td>\r\n<td bgcolor="#008cff">008cff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#008bff">008bff</td>\r\n<td bgcolor="#008aff">008aff</td>\r\n<td bgcolor="#0089ff">0089ff</td>\r\n<td bgcolor="#0088ff">0088ff</td>\r\n<td bgcolor="#0087ff">0087ff</td>\r\n<td bgcolor="#0086ff">0086ff</td>\r\n<td bgcolor="#0085ff">0085ff</td>\r\n<td bgcolor="#0084ff">0084ff</td>\r\n<td bgcolor="#0083ff">0083ff</td>\r\n<td bgcolor="#0082ff">0082ff</td>\r\n<td bgcolor="#0081ff">0081ff</td>\r\n<td bgcolor="#0080ff">0080ff</td>\r\n<td bgcolor="#007fff">007fff</td>\r\n<td bgcolor="#007eff">007eff</td>\r\n<td bgcolor="#007dff">007dff</td>\r\n<td bgcolor="#007cff">007cff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#007bff">007bff</td>\r\n<td bgcolor="#007aff">007aff</td>\r\n<td bgcolor="#0079ff">0079ff</td>\r\n<td bgcolor="#0078ff">0078ff</td>\r\n<td bgcolor="#0077ff">0077ff</td>\r\n<td bgcolor="#0076ff">0076ff</td>\r\n<td bgcolor="#0075ff">0075ff</td>\r\n<td bgcolor="#0074ff">0074ff</td>\r\n<td bgcolor="#0073ff">0073ff</td>\r\n<td bgcolor="#0072ff">0072ff</td>\r\n<td bgcolor="#0071ff">0071ff</td>\r\n<td bgcolor="#0070ff">0070ff</td>\r\n<td bgcolor="#006fff">006fff</td>\r\n<td bgcolor="#006eff">006eff</td>\r\n<td bgcolor="#006dff">006dff</td>\r\n<td bgcolor="#006cff">006cff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#006bff">006bff</td>\r\n<td bgcolor="#006aff">006aff</td>\r\n<td bgcolor="#0069ff">0069ff</td>\r\n<td bgcolor="#0068ff">0068ff</td>\r\n<td bgcolor="#0067ff">0067ff</td>\r\n<td bgcolor="#0066ff">0066ff</td>\r\n<td bgcolor="#0065ff">0065ff</td>\r\n<td bgcolor="#0064ff">0064ff</td>\r\n<td bgcolor="#0063ff">0063ff</td>\r\n<td bgcolor="#0062ff">0062ff</td>\r\n<td bgcolor="#0061ff">0061ff</td>\r\n<td bgcolor="#0060ff">0060ff</td>\r\n<td bgcolor="#005fff">005fff</td>\r\n<td bgcolor="#005eff">005eff</td>\r\n<td bgcolor="#005dff">005dff</td>\r\n<td bgcolor="#005cff">005cff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#005bff">005bff</td>\r\n<td bgcolor="#005aff">005aff</td>\r\n<td bgcolor="#0059ff">0059ff</td>\r\n<td bgcolor="#0058ff">0058ff</td>\r\n<td bgcolor="#0057ff">0057ff</td>\r\n<td bgcolor="#0056ff">0056ff</td>\r\n<td bgcolor="#0055ff">0055ff</td>\r\n<td bgcolor="#0054ff">0054ff</td>\r\n<td bgcolor="#0053ff">0053ff</td>\r\n<td bgcolor="#0052ff">0052ff</td>\r\n<td bgcolor="#0051ff">0051ff</td>\r\n<td bgcolor="#0050ff">0050ff</td>\r\n<td bgcolor="#004fff">004fff</td>\r\n<td bgcolor="#004eff">004eff</td>\r\n<td bgcolor="#004dff">004dff</td>\r\n<td bgcolor="#004cff">004cff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#004bff">004bff</td>\r\n<td bgcolor="#004aff">004aff</td>\r\n<td bgcolor="#0049ff">0049ff</td>\r\n<td bgcolor="#0048ff">0048ff</td>\r\n<td bgcolor="#0047ff">0047ff</td>\r\n<td bgcolor="#0046ff">0046ff</td>\r\n<td bgcolor="#0045ff">0045ff</td>\r\n<td bgcolor="#0044ff">0044ff</td>\r\n<td bgcolor="#0043ff">0043ff</td>\r\n<td bgcolor="#0042ff">0042ff</td>\r\n<td bgcolor="#0041ff">0041ff</td>\r\n<td bgcolor="#0040ff">0040ff</td>\r\n<td bgcolor="#003fff">003fff</td>\r\n<td bgcolor="#003eff">003eff</td>\r\n<td bgcolor="#003dff">003dff</td>\r\n<td bgcolor="#003cff">003cff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#003bff">003bff</td>\r\n<td bgcolor="#003aff">003aff</td>\r\n<td bgcolor="#0039ff">0039ff</td>\r\n<td bgcolor="#0038ff">0038ff</td>\r\n<td bgcolor="#0037ff">0037ff</td>\r\n<td bgcolor="#0036ff">0036ff</td>\r\n<td bgcolor="#0035ff">0035ff</td>\r\n<td bgcolor="#0034ff">0034ff</td>\r\n<td bgcolor="#0033ff">0033ff</td>\r\n<td bgcolor="#0032ff">0032ff</td>\r\n<td bgcolor="#0031ff">0031ff</td>\r\n<td bgcolor="#0030ff">0030ff</td>\r\n<td bgcolor="#002fff">002fff</td>\r\n<td bgcolor="#002eff">002eff</td>\r\n<td bgcolor="#002dff">002dff</td>\r\n<td bgcolor="#002cff">002cff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#002bff">002bff</td>\r\n<td bgcolor="#002aff">002aff</td>\r\n<td bgcolor="#0029ff">0029ff</td>\r\n<td bgcolor="#0028ff">0028ff</td>\r\n<td bgcolor="#0027ff">0027ff</td>\r\n<td bgcolor="#0026ff">0026ff</td>\r\n<td bgcolor="#0025ff">0025ff</td>\r\n<td bgcolor="#0024ff">0024ff</td>\r\n<td bgcolor="#0023ff">0023ff</td>\r\n<td bgcolor="#0022ff">0022ff</td>\r\n<td bgcolor="#0021ff">0021ff</td>\r\n<td bgcolor="#0020ff">0020ff</td>\r\n<td bgcolor="#001fff">001fff</td>\r\n<td bgcolor="#001eff">001eff</td>\r\n<td bgcolor="#001dff">001dff</td>\r\n<td bgcolor="#001cff">001cff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#001bff">001bff</td>\r\n<td bgcolor="#001aff">001aff</td>\r\n<td bgcolor="#0019ff">0019ff</td>\r\n<td bgcolor="#0018ff">0018ff</td>\r\n<td bgcolor="#0017ff">0017ff</td>\r\n<td bgcolor="#0016ff">0016ff</td>\r\n<td bgcolor="#0015ff">0015ff</td>\r\n<td bgcolor="#0014ff">0014ff</td>\r\n<td bgcolor="#0013ff">0013ff</td>\r\n<td bgcolor="#0012ff">0012ff</td>\r\n<td bgcolor="#0011ff">0011ff</td>\r\n<td bgcolor="#0010ff">0010ff</td>\r\n<td bgcolor="#000fff">000fff</td>\r\n<td bgcolor="#000eff">000eff</td>\r\n<td bgcolor="#000dff">000dff</td>\r\n<td bgcolor="#000cff">000cff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#000bff">000bff</td>\r\n<td bgcolor="#000aff">000aff</td>\r\n<td bgcolor="#0009ff">0009ff</td>\r\n<td bgcolor="#0008ff">0008ff</td>\r\n<td bgcolor="#0007ff">0007ff</td>\r\n<td bgcolor="#0006ff">0006ff</td>\r\n<td bgcolor="#0005ff">0005ff</td>\r\n<td bgcolor="#0004ff">0004ff</td>\r\n<td bgcolor="#0003ff">0003ff</td>\r\n<td bgcolor="#0002ff">0002ff</td>\r\n<td bgcolor="#0001ff">0001ff</td>\r\n<td bgcolor="#0000ff">0000ff</td>\r\n<td bgcolor="#0000ff">0000ff</td>\r\n<td bgcolor="#0100ff">0100ff</td>\r\n<td bgcolor="#0200ff">0200ff</td>\r\n<td bgcolor="#0300ff">0300ff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#0400ff">0400ff</td>\r\n<td bgcolor="#0500ff">0500ff</td>\r\n<td bgcolor="#0600ff">0600ff</td>\r\n<td bgcolor="#0700ff">0700ff</td>\r\n<td bgcolor="#0800ff">0800ff</td>\r\n<td bgcolor="#0900ff">0900ff</td>\r\n<td bgcolor="#0a00ff">0a00ff</td>\r\n<td bgcolor="#0b00ff">0b00ff</td>\r\n<td bgcolor="#0c00ff">0c00ff</td>\r\n<td bgcolor="#0d00ff">0d00ff</td>\r\n<td bgcolor="#0e00ff">0e00ff</td>\r\n<td bgcolor="#0f00ff">0f00ff</td>\r\n<td bgcolor="#1000ff">1000ff</td>\r\n<td bgcolor="#1100ff">1100ff</td>\r\n<td bgcolor="#1200ff">1200ff</td>\r\n<td bgcolor="#1300ff">1300ff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#1400ff">1400ff</td>\r\n<td bgcolor="#1500ff">1500ff</td>\r\n<td bgcolor="#1600ff">1600ff</td>\r\n<td bgcolor="#1700ff">1700ff</td>\r\n<td bgcolor="#1800ff">1800ff</td>\r\n<td bgcolor="#1900ff">1900ff</td>\r\n<td bgcolor="#1a00ff">1a00ff</td>\r\n<td bgcolor="#1b00ff">1b00ff</td>\r\n<td bgcolor="#1c00ff">1c00ff</td>\r\n<td bgcolor="#1d00ff">1d00ff</td>\r\n<td bgcolor="#1e00ff">1e00ff</td>\r\n<td bgcolor="#1f00ff">1f00ff</td>\r\n<td bgcolor="#2000ff">2000ff</td>\r\n<td bgcolor="#2100ff">2100ff</td>\r\n<td bgcolor="#2200ff">2200ff</td>\r\n<td bgcolor="#2300ff">2300ff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#2400ff">2400ff</td>\r\n<td bgcolor="#2500ff">2500ff</td>\r\n<td bgcolor="#2600ff">2600ff</td>\r\n<td bgcolor="#2700ff">2700ff</td>\r\n<td bgcolor="#2800ff">2800ff</td>\r\n<td bgcolor="#2900ff">2900ff</td>\r\n<td bgcolor="#2a00ff">2a00ff</td>\r\n<td bgcolor="#2b00ff">2b00ff</td>\r\n<td bgcolor="#2c00ff">2c00ff</td>\r\n<td bgcolor="#2d00ff">2d00ff</td>\r\n<td bgcolor="#2e00ff">2e00ff</td>\r\n<td bgcolor="#2f00ff">2f00ff</td>\r\n<td bgcolor="#3000ff">3000ff</td>\r\n<td bgcolor="#3100ff">3100ff</td>\r\n<td bgcolor="#3200ff">3200ff</td>\r\n<td bgcolor="#3300ff">3300ff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#3400ff">3400ff</td>\r\n<td bgcolor="#3500ff">3500ff</td>\r\n<td bgcolor="#3600ff">3600ff</td>\r\n<td bgcolor="#3700ff">3700ff</td>\r\n<td bgcolor="#3800ff">3800ff</td>\r\n<td bgcolor="#3900ff">3900ff</td>\r\n<td bgcolor="#3a00ff">3a00ff</td>\r\n<td bgcolor="#3b00ff">3b00ff</td>\r\n<td bgcolor="#3c00ff">3c00ff</td>\r\n<td bgcolor="#3d00ff">3d00ff</td>\r\n<td bgcolor="#3e00ff">3e00ff</td>\r\n<td bgcolor="#3f00ff">3f00ff</td>\r\n<td bgcolor="#4000ff">4000ff</td>\r\n<td bgcolor="#4100ff">4100ff</td>\r\n<td bgcolor="#4200ff">4200ff</td>\r\n<td bgcolor="#4300ff">4300ff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#4400ff">4400ff</td>\r\n<td bgcolor="#4500ff">4500ff</td>\r\n<td bgcolor="#4600ff">4600ff</td>\r\n<td bgcolor="#4700ff">4700ff</td>\r\n<td bgcolor="#4800ff">4800ff</td>\r\n<td bgcolor="#4900ff">4900ff</td>\r\n<td bgcolor="#4a00ff">4a00ff</td>\r\n<td bgcolor="#4b00ff">4b00ff</td>\r\n<td bgcolor="#4c00ff">4c00ff</td>\r\n<td bgcolor="#4d00ff">4d00ff</td>\r\n<td bgcolor="#4e00ff">4e00ff</td>\r\n<td bgcolor="#4f00ff">4f00ff</td>\r\n<td bgcolor="#5000ff">5000ff</td>\r\n<td bgcolor="#5100ff">5100ff</td>\r\n<td bgcolor="#5200ff">5200ff</td>\r\n<td bgcolor="#5300ff">5300ff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#5400ff">5400ff</td>\r\n<td bgcolor="#5500ff">5500ff</td>\r\n<td bgcolor="#5600ff">5600ff</td>\r\n<td bgcolor="#5700ff">5700ff</td>\r\n<td bgcolor="#5800ff">5800ff</td>\r\n<td bgcolor="#5900ff">5900ff</td>\r\n<td bgcolor="#5a00ff">5a00ff</td>\r\n<td bgcolor="#5b00ff">5b00ff</td>\r\n<td bgcolor="#5c00ff">5c00ff</td>\r\n<td bgcolor="#5d00ff">5d00ff</td>\r\n<td bgcolor="#5e00ff">5e00ff</td>\r\n<td bgcolor="#5f00ff">5f00ff</td>\r\n<td bgcolor="#6000ff">6000ff</td>\r\n<td bgcolor="#6100ff">6100ff</td>\r\n<td bgcolor="#6200ff">6200ff</td>\r\n<td bgcolor="#6300ff">6300ff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#6400ff">6400ff</td>\r\n<td bgcolor="#6500ff">6500ff</td>\r\n<td bgcolor="#6600ff">6600ff</td>\r\n<td bgcolor="#6700ff">6700ff</td>\r\n<td bgcolor="#6800ff">6800ff</td>\r\n<td bgcolor="#6900ff">6900ff</td>\r\n<td bgcolor="#6a00ff">6a00ff</td>\r\n<td bgcolor="#6b00ff">6b00ff</td>\r\n<td bgcolor="#6c00ff">6c00ff</td>\r\n<td bgcolor="#6d00ff">6d00ff</td>\r\n<td bgcolor="#6e00ff">6e00ff</td>\r\n<td bgcolor="#6f00ff">6f00ff</td>\r\n<td bgcolor="#7000ff">7000ff</td>\r\n<td bgcolor="#7100ff">7100ff</td>\r\n<td bgcolor="#7200ff">7200ff</td>\r\n<td bgcolor="#7300ff">7300ff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#7400ff">7400ff</td>\r\n<td bgcolor="#7500ff">7500ff</td>\r\n<td bgcolor="#7600ff">7600ff</td>\r\n<td bgcolor="#7700ff">7700ff</td>\r\n<td bgcolor="#7800ff">7800ff</td>\r\n<td bgcolor="#7900ff">7900ff</td>\r\n<td bgcolor="#7a00ff">7a00ff</td>\r\n<td bgcolor="#7b00ff">7b00ff</td>\r\n<td bgcolor="#7c00ff">7c00ff</td>\r\n<td bgcolor="#7d00ff">7d00ff</td>\r\n<td bgcolor="#7e00ff">7e00ff</td>\r\n<td bgcolor="#7f00ff">7f00ff</td>\r\n<td bgcolor="#8000ff">8000ff</td>\r\n<td bgcolor="#8100ff">8100ff</td>\r\n<td bgcolor="#8200ff">8200ff</td>\r\n<td bgcolor="#8300ff">8300ff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#8400ff">8400ff</td>\r\n<td bgcolor="#8500ff">8500ff</td>\r\n<td bgcolor="#8600ff">8600ff</td>\r\n<td bgcolor="#8700ff">8700ff</td>\r\n<td bgcolor="#8800ff">8800ff</td>\r\n<td bgcolor="#8900ff">8900ff</td>\r\n<td bgcolor="#8a00ff">8a00ff</td>\r\n<td bgcolor="#8b00ff">8b00ff</td>\r\n<td bgcolor="#8c00ff">8c00ff</td>\r\n<td bgcolor="#8d00ff">8d00ff</td>\r\n<td bgcolor="#8e00ff">8e00ff</td>\r\n<td bgcolor="#8f00ff">8f00ff</td>\r\n<td bgcolor="#9000ff">9000ff</td>\r\n<td bgcolor="#9100ff">9100ff</td>\r\n<td bgcolor="#9200ff">9200ff</td>\r\n<td bgcolor="#9300ff">9300ff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#9400ff">9400ff</td>\r\n<td bgcolor="#9500ff">9500ff</td>\r\n<td bgcolor="#9600ff">9600ff</td>\r\n<td bgcolor="#9700ff">9700ff</td>\r\n<td bgcolor="#9800ff">9800ff</td>\r\n<td bgcolor="#9900ff">9900ff</td>\r\n<td bgcolor="#9a00ff">9a00ff</td>\r\n<td bgcolor="#9b00ff">9b00ff</td>\r\n<td bgcolor="#9c00ff">9c00ff</td>\r\n<td bgcolor="#9d00ff">9d00ff</td>\r\n<td bgcolor="#9e00ff">9e00ff</td>\r\n<td bgcolor="#9f00ff">9f00ff</td>\r\n<td bgcolor="#a000ff">a000ff</td>\r\n<td bgcolor="#a100ff">a100ff</td>\r\n<td bgcolor="#a200ff">a200ff</td>\r\n<td bgcolor="#a300ff">a300ff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#a400ff">a400ff</td>\r\n<td bgcolor="#a500ff">a500ff</td>\r\n<td bgcolor="#a600ff">a600ff</td>\r\n<td bgcolor="#a700ff">a700ff</td>\r\n<td bgcolor="#a800ff">a800ff</td>\r\n<td bgcolor="#a900ff">a900ff</td>\r\n<td bgcolor="#aa00ff">aa00ff</td>\r\n<td bgcolor="#ab00ff">ab00ff</td>\r\n<td bgcolor="#ac00ff">ac00ff</td>\r\n<td bgcolor="#ad00ff">ad00ff</td>\r\n<td bgcolor="#ae00ff">ae00ff</td>\r\n<td bgcolor="#af00ff">af00ff</td>\r\n<td bgcolor="#b000ff">b000ff</td>\r\n<td bgcolor="#b100ff">b100ff</td>\r\n<td bgcolor="#b200ff">b200ff</td>\r\n<td bgcolor="#b300ff">b300ff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#b400ff">b400ff</td>\r\n<td bgcolor="#b500ff">b500ff</td>\r\n<td bgcolor="#b600ff">b600ff</td>\r\n<td bgcolor="#b700ff">b700ff</td>\r\n<td bgcolor="#b800ff">b800ff</td>\r\n<td bgcolor="#b900ff">b900ff</td>\r\n<td bgcolor="#ba00ff">ba00ff</td>\r\n<td bgcolor="#bb00ff">bb00ff</td>\r\n<td bgcolor="#bc00ff">bc00ff</td>\r\n<td bgcolor="#bd00ff">bd00ff</td>\r\n<td bgcolor="#be00ff">be00ff</td>\r\n<td bgcolor="#bf00ff">bf00ff</td>\r\n<td bgcolor="#c000ff">c000ff</td>\r\n<td bgcolor="#c100ff">c100ff</td>\r\n<td bgcolor="#c200ff">c200ff</td>\r\n<td bgcolor="#c300ff">c300ff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#c400ff">c400ff</td>\r\n<td bgcolor="#c500ff">c500ff</td>\r\n<td bgcolor="#c600ff">c600ff</td>\r\n<td bgcolor="#c700ff">c700ff</td>\r\n<td bgcolor="#c800ff">c800ff</td>\r\n<td bgcolor="#c900ff">c900ff</td>\r\n<td bgcolor="#ca00ff">ca00ff</td>\r\n<td bgcolor="#cb00ff">cb00ff</td>\r\n<td bgcolor="#cc00ff">cc00ff</td>\r\n<td bgcolor="#cd00ff">cd00ff</td>\r\n<td bgcolor="#ce00ff">ce00ff</td>\r\n<td bgcolor="#cf00ff">cf00ff</td>\r\n<td bgcolor="#d000ff">d000ff</td>\r\n<td bgcolor="#d100ff">d100ff</td>\r\n<td bgcolor="#d200ff">d200ff</td>\r\n<td bgcolor="#d300ff">d300ff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#d400ff">d400ff</td>\r\n<td bgcolor="#d500ff">d500ff</td>\r\n<td bgcolor="#d600ff">d600ff</td>\r\n<td bgcolor="#d700ff">d700ff</td>\r\n<td bgcolor="#d800ff">d800ff</td>\r\n<td bgcolor="#d900ff">d900ff</td>\r\n<td bgcolor="#da00ff">da00ff</td>\r\n<td bgcolor="#db00ff">db00ff</td>\r\n<td bgcolor="#dc00ff">dc00ff</td>\r\n<td bgcolor="#dd00ff">dd00ff</td>\r\n<td bgcolor="#de00ff">de00ff</td>\r\n<td bgcolor="#df00ff">df00ff</td>\r\n<td bgcolor="#e000ff">e000ff</td>\r\n<td bgcolor="#e100ff">e100ff</td>\r\n<td bgcolor="#e200ff">e200ff</td>\r\n<td bgcolor="#e300ff">e300ff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#e400ff">e400ff</td>\r\n<td bgcolor="#e500ff">e500ff</td>\r\n<td bgcolor="#e600ff">e600ff</td>\r\n<td bgcolor="#e700ff">e700ff</td>\r\n<td bgcolor="#e800ff">e800ff</td>\r\n<td bgcolor="#e900ff">e900ff</td>\r\n<td bgcolor="#ea00ff">ea00ff</td>\r\n<td bgcolor="#eb00ff">eb00ff</td>\r\n<td bgcolor="#ec00ff">ec00ff</td>\r\n<td bgcolor="#ed00ff">ed00ff</td>\r\n<td bgcolor="#ee00ff">ee00ff</td>\r\n<td bgcolor="#ef00ff">ef00ff</td>\r\n<td bgcolor="#f000ff">f000ff</td>\r\n<td bgcolor="#f100ff">f100ff</td>\r\n<td bgcolor="#f200ff">f200ff</td>\r\n<td bgcolor="#f300ff">f300ff</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#f400ff">f400ff</td>\r\n<td bgcolor="#f500ff">f500ff</td>\r\n<td bgcolor="#f600ff">f600ff</td>\r\n<td bgcolor="#f700ff">f700ff</td>\r\n<td bgcolor="#f800ff">f800ff</td>\r\n<td bgcolor="#f900ff">f900ff</td>\r\n<td bgcolor="#fa00ff">fa00ff</td>\r\n<td bgcolor="#fb00ff">fb00ff</td>\r\n<td bgcolor="#fc00ff">fc00ff</td>\r\n<td bgcolor="#fd00ff">fd00ff</td>\r\n<td bgcolor="#fe00ff">fe00ff</td>\r\n<td bgcolor="#ff00ff">ff00ff<br /></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<table border="0">\r\n<tbody>\r\n<tr>\r\n<td bgcolor="#ffffff">ffffff</td>\r\n<td bgcolor="#fefefe">fefefe</td>\r\n<td bgcolor="#fdfdfd">fdfdfd</td>\r\n<td bgcolor="#fcfcfc">fcfcfc</td>\r\n<td bgcolor="#fbfbfb">fbfbfb</td>\r\n<td bgcolor="#fafafa">fafafa</td>\r\n<td bgcolor="#f9f9f9">f9f9f9</td>\r\n<td bgcolor="#f8f8f8">f8f8f8</td>\r\n<td bgcolor="#f7f7f7">f7f7f7</td>\r\n<td bgcolor="#f6f6f6">f6f6f6</td>\r\n<td bgcolor="#f5f5f5">f5f5f5</td>\r\n<td bgcolor="#f4f4f4">f4f4f4</td>\r\n<td bgcolor="#f3f3f3">f3f3f3</td>\r\n<td bgcolor="#f2f2f2">f2f2f2</td>\r\n<td bgcolor="#f1f1f1">f1f1f1</td>\r\n<td bgcolor="#f0f0f0">f0f0f0</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#efefef">efefef</td>\r\n<td bgcolor="#eeeeee">eeeeee</td>\r\n<td bgcolor="#ededed">ededed</td>\r\n<td bgcolor="#ececec">ececec</td>\r\n<td bgcolor="#ebebeb">ebebeb</td>\r\n<td bgcolor="#eaeaea">eaeaea</td>\r\n<td bgcolor="#e9e9e9">e9e9e9</td>\r\n<td bgcolor="#e8e8e8">e8e8e8</td>\r\n<td bgcolor="#e7e7e7">e7e7e7</td>\r\n<td bgcolor="#e6e6e6">e6e6e6</td>\r\n<td bgcolor="#e5e5e5">e5e5e5</td>\r\n<td bgcolor="#e4e4e4">e4e4e4</td>\r\n<td bgcolor="#e3e3e3">e3e3e3</td>\r\n<td bgcolor="#e2e2e2">e2e2e2</td>\r\n<td bgcolor="#e1e1e1">e1e1e1</td>\r\n<td bgcolor="#e0e0e0">e0e0e0</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#dfdfdf">dfdfdf</td>\r\n<td bgcolor="#dedede">dedede</td>\r\n<td bgcolor="#dddddd">dddddd</td>\r\n<td bgcolor="#dcdcdc">dcdcdc</td>\r\n<td bgcolor="#dbdbdb">dbdbdb</td>\r\n<td bgcolor="#dadada">dadada</td>\r\n<td bgcolor="#d9d9d9">d9d9d9</td>\r\n<td bgcolor="#d8d8d8">d8d8d8</td>\r\n<td bgcolor="#d7d7d7">d7d7d7</td>\r\n<td bgcolor="#d6d6d6">d6d6d6</td>\r\n<td bgcolor="#d5d5d5">d5d5d5</td>\r\n<td bgcolor="#d4d4d4">d4d4d4</td>\r\n<td bgcolor="#d3d3d3">d3d3d3</td>\r\n<td bgcolor="#d2d2d2">d2d2d2</td>\r\n<td bgcolor="#d1d1d1">d1d1d1</td>\r\n<td bgcolor="#d0d0d0">d0d0d0</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#cfcfcf">cfcfcf</td>\r\n<td bgcolor="#cecece">cecece</td>\r\n<td bgcolor="#cdcdcd">cdcdcd</td>\r\n<td bgcolor="#cccccc">cccccc</td>\r\n<td bgcolor="#cbcbcb">cbcbcb</td>\r\n<td bgcolor="#cacaca">cacaca</td>\r\n<td bgcolor="#c9c9c9">c9c9c9</td>\r\n<td bgcolor="#c8c8c8">c8c8c8</td>\r\n<td bgcolor="#c7c7c7">c7c7c7</td>\r\n<td bgcolor="#c6c6c6">c6c6c6</td>\r\n<td bgcolor="#c5c5c5">c5c5c5</td>\r\n<td bgcolor="#c4c4c4">c4c4c4</td>\r\n<td bgcolor="#c3c3c3">c3c3c3</td>\r\n<td bgcolor="#c2c2c2">c2c2c2</td>\r\n<td bgcolor="#c1c1c1">c1c1c1</td>\r\n<td bgcolor="#c0c0c0">c0c0c0</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#bfbfbf">bfbfbf</td>\r\n<td bgcolor="#bebebe">bebebe</td>\r\n<td bgcolor="#bdbdbd">bdbdbd</td>\r\n<td bgcolor="#bcbcbc">bcbcbc</td>\r\n<td bgcolor="#bbbbbb">bbbbbb</td>\r\n<td bgcolor="#bababa">bababa</td>\r\n<td bgcolor="#b9b9b9">b9b9b9</td>\r\n<td bgcolor="#b8b8b8">b8b8b8</td>\r\n<td bgcolor="#b7b7b7">b7b7b7</td>\r\n<td bgcolor="#b6b6b6">b6b6b6</td>\r\n<td bgcolor="#b5b5b5">b5b5b5</td>\r\n<td bgcolor="#b4b4b4">b4b4b4</td>\r\n<td bgcolor="#b3b3b3">b3b3b3</td>\r\n<td bgcolor="#b2b2b2">b2b2b2</td>\r\n<td bgcolor="#b1b1b1">b1b1b1</td>\r\n<td bgcolor="#b0b0b0">b0b0b0</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#afafaf">afafaf</td>\r\n<td bgcolor="#aeaeae">aeaeae</td>\r\n<td bgcolor="#adadad">adadad</td>\r\n<td bgcolor="#acacac">acacac</td>\r\n<td bgcolor="#ababab">ababab</td>\r\n<td bgcolor="#aaaaaa">aaaaaa</td>\r\n<td bgcolor="#a9a9a9">a9a9a9</td>\r\n<td bgcolor="#a8a8a8">a8a8a8</td>\r\n<td bgcolor="#a7a7a7">a7a7a7</td>\r\n<td bgcolor="#a6a6a6">a6a6a6</td>\r\n<td bgcolor="#a5a5a5">a5a5a5</td>\r\n<td bgcolor="#a4a4a4">a4a4a4</td>\r\n<td bgcolor="#a3a3a3">a3a3a3</td>\r\n<td bgcolor="#a2a2a2">a2a2a2</td>\r\n<td bgcolor="#a1a1a1">a1a1a1</td>\r\n<td bgcolor="#a0a0a0">a0a0a0</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#9f9f9f">9f9f9f</td>\r\n<td bgcolor="#9e9e9e">9e9e9e</td>\r\n<td bgcolor="#9d9d9d">9d9d9d</td>\r\n<td bgcolor="#9c9c9c">9c9c9c</td>\r\n<td bgcolor="#9b9b9b">9b9b9b</td>\r\n<td bgcolor="#9a9a9a">9a9a9a</td>\r\n<td bgcolor="#999999">999999</td>\r\n<td bgcolor="#989898">989898</td>\r\n<td bgcolor="#979797">979797</td>\r\n<td bgcolor="#969696">969696</td>\r\n<td bgcolor="#959595">959595</td>\r\n<td bgcolor="#949494">949494</td>\r\n<td bgcolor="#939393">939393</td>\r\n<td bgcolor="#929292">929292</td>\r\n<td bgcolor="#919191">919191</td>\r\n<td bgcolor="#909090">909090</td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#8f8f8f">8f8f8f</td>\r\n<td bgcolor="#8e8e8e">8e8e8e</td>\r\n<td bgcolor="#8d8d8d">8d8d8d</td>\r\n<td bgcolor="#8c8c8c">8c8c8c</td>\r\n<td bgcolor="#8b8b8b">8b8b8b</td>\r\n<td bgcolor="#8a8a8a">8a8a8a</td>\r\n<td bgcolor="#898989">898989</td>\r\n<td bgcolor="#888888">888888</td>\r\n<td bgcolor="#878787">878787</td>\r\n<td bgcolor="#868686">868686</td>\r\n<td bgcolor="#858585">858585</td>\r\n<td bgcolor="#848484">848484</td>\r\n<td bgcolor="#838383">838383</td>\r\n<td bgcolor="#828282">828282</td>\r\n<td bgcolor="#818181">818181</td>\r\n<td bgcolor="#808080"><span style="color: #ffffff;">808080</span></td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#7f7f7f"><span style="color: #ffffff;">7f7f7f</span></td>\r\n<td bgcolor="#7e7e7e"><span style="color: #ffffff;">7e7e7e</span></td>\r\n<td bgcolor="#7d7d7d"><span style="color: #ffffff;">7d7d7d</span></td>\r\n<td bgcolor="#7c7c7c"><span style="color: #ffffff;">7c7c7c</span></td>\r\n<td bgcolor="#7b7b7b"><span style="color: #ffffff;">7b7b7b</span></td>\r\n<td bgcolor="#7a7a7a"><span style="color: #ffffff;">7a7a7a</span></td>\r\n<td bgcolor="#797979"><span style="color: #ffffff;">797979</span></td>\r\n<td bgcolor="#787878"><span style="color: #ffffff;">787878</span></td>\r\n<td bgcolor="#777777"><span style="color: #ffffff;">777777</span></td>\r\n<td bgcolor="#767676"><span style="color: #ffffff;">767676</span></td>\r\n<td bgcolor="#757575"><span style="color: #ffffff;">757575</span></td>\r\n<td bgcolor="#747474"><span style="color: #ffffff;">747474</span></td>\r\n<td bgcolor="#737373"><span style="color: #ffffff;">737373</span></td>\r\n<td bgcolor="#727272"><span style="color: #ffffff;">727272</span></td>\r\n<td bgcolor="#717171"><span style="color: #ffffff;">717171</span></td>\r\n<td bgcolor="#707070"><span style="color: #ffffff;">707070</span></td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#6f6f6f"><span style="color: #ffffff;">6f6f6f</span></td>\r\n<td bgcolor="#6e6e6e"><span style="color: #ffffff;">6e6e6e</span></td>\r\n<td bgcolor="#6d6d6d"><span style="color: #ffffff;">6d6d6d</span></td>\r\n<td bgcolor="#6c6c6c"><span style="color: #ffffff;">6c6c6c</span></td>\r\n<td bgcolor="#6b6b6b"><span style="color: #ffffff;">6b6b6b</span></td>\r\n<td bgcolor="#6a6a6a"><span style="color: #ffffff;">6a6a6a</span></td>\r\n<td bgcolor="#696969"><span style="color: #ffffff;">696969</span></td>\r\n<td bgcolor="#686868"><span style="color: #ffffff;">686868</span></td>\r\n<td bgcolor="#676767"><span style="color: #ffffff;">676767</span></td>\r\n<td bgcolor="#666666"><span style="color: #ffffff;">666666</span></td>\r\n<td bgcolor="#656565"><span style="color: #ffffff;">656565</span></td>\r\n<td bgcolor="#646464"><span style="color: #ffffff;">646464</span></td>\r\n<td bgcolor="#636363"><span style="color: #ffffff;">636363</span></td>\r\n<td bgcolor="#626262"><span style="color: #ffffff;">626262</span></td>\r\n<td bgcolor="#616161"><span style="color: #ffffff;">616161</span></td>\r\n<td bgcolor="#606060"><span style="color: #ffffff;">606060</span></td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#5f5f5f"><span style="color: #ffffff;">5f5f5f</span></td>\r\n<td bgcolor="#5e5e5e"><span style="color: #ffffff;">5e5e5e</span></td>\r\n<td bgcolor="#5d5d5d"><span style="color: #ffffff;">5d5d5d</span></td>\r\n<td bgcolor="#5c5c5c"><span style="color: #ffffff;">5c5c5c</span></td>\r\n<td bgcolor="#5b5b5b"><span style="color: #ffffff;">5b5b5b</span></td>\r\n<td bgcolor="#5a5a5a"><span style="color: #ffffff;">5a5a5a</span></td>\r\n<td bgcolor="#595959"><span style="color: #ffffff;">595959</span></td>\r\n<td bgcolor="#585858"><span style="color: #ffffff;">585858</span></td>\r\n<td bgcolor="#575757"><span style="color: #ffffff;">575757</span></td>\r\n<td bgcolor="#565656"><span style="color: #ffffff;">565656</span></td>\r\n<td bgcolor="#555555"><span style="color: #ffffff;">555555</span></td>\r\n<td bgcolor="#545454"><span style="color: #ffffff;">545454</span></td>\r\n<td bgcolor="#535353"><span style="color: #ffffff;">535353</span></td>\r\n<td bgcolor="#525252"><span style="color: #ffffff;">525252</span></td>\r\n<td bgcolor="#515151"><span style="color: #ffffff;">515151</span></td>\r\n<td bgcolor="#505050"><span style="color: #ffffff;">505050</span></td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#4f4f4f"><span style="color: #ffffff;">4f4f4f</span></td>\r\n<td bgcolor="#4e4e4e"><span style="color: #ffffff;">4e4e4e</span></td>\r\n<td bgcolor="#4d4d4d"><span style="color: #ffffff;">4d4d4d</span></td>\r\n<td bgcolor="#4c4c4c"><span style="color: #ffffff;">4c4c4c</span></td>\r\n<td bgcolor="#4b4b4b"><span style="color: #ffffff;">4b4b4b</span></td>\r\n<td bgcolor="#4a4a4a"><span style="color: #ffffff;">4a4a4a</span></td>\r\n<td bgcolor="#494949"><span style="color: #ffffff;">494949</span></td>\r\n<td bgcolor="#484848"><span style="color: #ffffff;">484848</span></td>\r\n<td bgcolor="#474747"><span style="color: #ffffff;">474747</span></td>\r\n<td bgcolor="#464646"><span style="color: #ffffff;">464646</span></td>\r\n<td bgcolor="#454545"><span style="color: #ffffff;">454545</span></td>\r\n<td bgcolor="#444444"><span style="color: #ffffff;">444444</span></td>\r\n<td bgcolor="#434343"><span style="color: #ffffff;">434343</span></td>\r\n<td bgcolor="#424242"><span style="color: #ffffff;">424242</span></td>\r\n<td bgcolor="#414141"><span style="color: #ffffff;">414141</span></td>\r\n<td bgcolor="#404040"><span style="color: #ffffff;">404040</span></td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#3f3f3f"><span style="color: #ffffff;">3f3f3f</span></td>\r\n<td bgcolor="#3e3e3e"><span style="color: #ffffff;">3e3e3e</span></td>\r\n<td bgcolor="#3d3d3d"><span style="color: #ffffff;">3d3d3d</span></td>\r\n<td bgcolor="#3c3c3c"><span style="color: #ffffff;">3c3c3c</span></td>\r\n<td bgcolor="#3b3b3b"><span style="color: #ffffff;">3b3b3b</span></td>\r\n<td bgcolor="#3a3a3a"><span style="color: #ffffff;">3a3a3a</span></td>\r\n<td bgcolor="#393939"><span style="color: #ffffff;">393939</span></td>\r\n<td bgcolor="#383838"><span style="color: #ffffff;">383838</span></td>\r\n<td bgcolor="#373737"><span style="color: #ffffff;">373737</span></td>\r\n<td bgcolor="#363636"><span style="color: #ffffff;">363636</span></td>\r\n<td bgcolor="#353535"><span style="color: #ffffff;">353535</span></td>\r\n<td bgcolor="#343434"><span style="color: #ffffff;">343434</span></td>\r\n<td bgcolor="#333333"><span style="color: #ffffff;">333333</span></td>\r\n<td bgcolor="#323232"><span style="color: #ffffff;">323232</span></td>\r\n<td bgcolor="#313131"><span style="color: #ffffff;">313131</span></td>\r\n<td bgcolor="#303030"><span style="color: #ffffff;">303030</span></td>\r\n</tr>\r\n<tr>\r\n<td bgcolor="#2f2f2f"><span style="color: #ffffff;">2f2f2f</span></td>\r\n<td bgcolor="#2e2e2e"><span style="color: #ffffff;">#2e2e2e');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_persona`
--

CREATE TABLE IF NOT EXISTS `tbl_persona` (
  `CODE` int(11) NOT NULL,
  `ISMODER` int(1) DEFAULT NULL,
  `NAZV` varchar(250) DEFAULT NULL,
  `NAZVROD` varchar(250) DEFAULT NULL,
  `IMDB` varchar(200) DEFAULT NULL,
  `OPIS` text,
  `POSTER` varchar(100) DEFAULT NULL,
  `LINKS` int(11) DEFAULT NULL,
  `DATEADD` datetime DEFAULT NULL,
  `WHOADD` int(5) DEFAULT NULL,
  `DATEMODER` datetime DEFAULT NULL,
  `WHOMODER` int(5) DEFAULT NULL,
  `DATEEDIT` datetime DEFAULT NULL,
  `WHOEDIT` int(11) DEFAULT NULL,
  `ISDOPEDIT` int(1) DEFAULT NULL,
  `WHODOPEDIT` int(11) DEFAULT NULL,
  `WHYDOPEDIT` varchar(200) DEFAULT NULL,
  `KINOPOISK` varchar(200) DEFAULT NULL,
  `RAT` int(1) DEFAULT '0',
  `VIEW` int(255) DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=1160 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_persona_photo`
--

CREATE TABLE IF NOT EXISTS `tbl_persona_photo` (
  `CODE` int(11) NOT NULL,
  `POSITION` int(5) NOT NULL DEFAULT '1',
  `ISMODER` int(1) DEFAULT NULL,
  `BASECODE` int(11) DEFAULT NULL,
  `POSTER` char(200) DEFAULT NULL,
  `WHOADD` int(11) DEFAULT NULL,
  `DATEADD` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=452 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_poll`
--

CREATE TABLE IF NOT EXISTS `tbl_poll` (
  `CODE` int(11) NOT NULL,
  `QUESTION` varchar(255) DEFAULT NULL,
  `TOTALVOTES` int(11) DEFAULT '0',
  `STATUS` int(11) DEFAULT '0',
  `TIMESTART` int(25) DEFAULT '0',
  `TIMEEND` int(25) DEFAULT '0',
  `EXPIRE` int(1) DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `tbl_poll`
--

INSERT INTO `tbl_poll` (`CODE`, `QUESTION`, `TOTALVOTES`, `STATUS`, `TIMESTART`, `TIMEEND`, `EXPIRE`) VALUES
(12, 'Как вам модуль голосований?', 30, 0, 0, 0, 0),
(3, 'Нравиться ли вам тут?', 37, 1, 0, 0, 0),
(2, 'Ваш пол?', 39, 1, 0, 0, 0),
(4, 'Нужен ли сайту форум?', 31, 1, 0, 0, 0),
(5, 'Сколько вам лет?', 60, 1, 0, 0, 0),
(7, 'Вы курите ?', 39, 1, 0, 0, 0),
(8, 'Какой у вас сотовый оператор?', 35, 1, 0, 0, 0),
(9, 'Что вы делаете на сайте?', 27, 1, 0, 0, 0),
(10, 'Как вы оцениваете скорость работы портала?', 32, 1, 0, 0, 0),
(11, 'Каким браузером вы пользуетесь?', 38, 1, 0, 0, 0),
(13, 'Какой домен лучше нашему сайту?', 30, 1, 0, 0, 0),
(14, 'Как вам ФотоГалереи для персоналий?', 33, 1, 0, 0, 0),
(15, 'Пользуетесь ли вы парсером с кинопоиска?', 27, 1, 0, 0, 0),
(16, 'Хотели бы вы чтобы на сайте было онлайн-видео?', 32, 1, 0, 0, 0),
(17, 'Добавить комментарии в персоналии?', 29, 1, 0, 1291986000, 0),
(20, 'Какой у вас телефон?', 43, 1, 0, 0, 0),
(19, 'Как вы смотрите на то, чтобы связать ваши аккаунты с соц. сетями?', 15, 0, 0, 0, 0),
(21, 'Какой фирмы ваш телефон?', 34, 1, 0, 0, 0),
(23, 'Каким Вы считаете себя?', 24, 1, 0, 0, 0),
(24, 'Хотели бы вы за приглашение пользователей получать рейтинг?', 23, 0, 0, 0, 0),
(25, 'Нужна ли на сайте система &#34FAQ&#34? (Вопрос-ответ; вопросы пользователей)', 22, 0, 0, 0, 0),
(29, 'Одиноки ли мы во вселенной?', 15, 1, 0, 0, 0),
(30, 'Какого цвета ваши глаза?', 23, 1, 0, 0, 0),
(31, 'Вы смотрите +100500?', 18, 1, 0, 0, 0),
(32, 'Нужны ли подписи пользователям как на форумах?', 15, 1, 0, 0, 0),
(33, 'Как вам новый модуль голосований?', 13, 1, 0, 0, 0),
(34, 'Где бы вы хотели отдохнуть летом?', 17, 1, 0, 0, 0),
(35, 'Едите ли вы мясо?', 17, 1, 0, 0, 0),
(36, 'Какая музыка вам нравится?', 19, 1, 0, 0, 0),
(37, 'Каким антивирусом Вы пользуетесь?', 18, 1, 0, 0, 0),
(38, 'Сколько времени в день вы проводите на нашем сайте?', 18, 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_polldata`
--

CREATE TABLE IF NOT EXISTS `tbl_polldata` (
  `CODE` int(11) NOT NULL,
  `BASECODE` int(11) DEFAULT '0',
  `TEXT` varchar(80) DEFAULT NULL,
  `VOTES` int(11) DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=162 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `tbl_polldata`
--

INSERT INTO `tbl_polldata` (`CODE`, `BASECODE`, `TEXT`, `VOTES`) VALUES
(56, 12, 'Классный', 12),
(3, 2, 'Мужской', 34),
(4, 2, 'Женский', 5),
(6, 3, 'Да', 26),
(7, 3, 'Нет', 3),
(8, 3, 'Не знаю', 8),
(9, 4, 'Да', 17),
(10, 4, 'Нет', 6),
(11, 4, 'Мне без разницы', 8),
(12, 5, 'меньше 13', 3),
(13, 5, '13-15', 8),
(14, 5, '15-18', 26),
(15, 5, '19-25', 10),
(16, 5, '25-30', 5),
(17, 5, '31-35', 2),
(18, 5, '36-40', 1),
(19, 5, '41-45', 1),
(20, 5, '46-50', 0),
(21, 5, 'больше 50', 4),
(39, 8, 'Life ;)', 7),
(38, 8, 'MTS', 5),
(37, 8, 'Velcom', 23),
(36, 7, 'Да, но пытаюсь бросить...', 6),
(35, 7, 'Да', 8),
(34, 7, 'Нет, но думаю начать...', 2),
(33, 7, 'Нет!', 23),
(40, 8, 'Другой', 0),
(41, 9, 'Только качаю', 11),
(42, 9, 'Выкладываю раздачи', 3),
(43, 9, 'Качаю и выкладываю', 9),
(44, 9, 'Просто посещяю', 4),
(45, 10, 'На Отлично!', 13),
(46, 10, 'Нормально всё. Как любой другой сайт.', 8),
(47, 10, 'Слегка подтормаживает.', 8),
(48, 10, 'Чёта медленно ваще открывается.', 2),
(50, 10, 'У меня медленое соединение сети. Оценивать не буду.', 1),
(63, 14, 'Класно', 9),
(64, 14, 'Нормально', 15),
(51, 11, 'Internet Explorer', 0),
(52, 11, 'Opera', 35),
(53, 11, 'FireFox', 0),
(54, 11, 'Google Chrome', 2),
(55, 11, 'другим...', 1),
(57, 12, 'Нормально', 11),
(58, 12, 'Так себе', 3),
(59, 12, 'Полное говно', 4),
(61, 13, 'megamagnet', 22),
(62, 13, 'mmportal', 4),
(65, 14, 'Я считаю это не нужно', 4),
(66, 14, 'Фигня', 5),
(67, 15, 'Конечно, отличная вещь', 4),
(68, 15, 'Иногда пользуюсь', 6),
(69, 15, 'Редко', 5),
(70, 15, 'Нафиг надо', 12),
(71, 16, 'Да', 29),
(72, 16, 'Нет', 3),
(73, 17, 'Да', 21),
(74, 17, 'Нет', 8),
(81, 20, 'Кнопочный', 22),
(80, 13, 'magnet', 2),
(79, 13, 'magneto', 2),
(77, 19, 'Хорошая идея', 10),
(78, 19, 'Это не нужно', 5),
(82, 20, 'Сенсорный', 13),
(83, 21, 'Nokia', 16),
(84, 21, 'SonyEricsson', 8),
(85, 21, 'HTC', 1),
(86, 21, 'LG', 0),
(87, 21, 'Motorola', 0),
(88, 21, 'Iphone', 2),
(89, 21, 'Philips', 0),
(90, 21, 'Siemens', 1),
(91, 21, 'ZTE', 1),
(104, 21, 'Samsung', 4),
(96, 23, 'симпатичным', 8),
(95, 23, 'красивым', 6),
(97, 23, 'средней внешности', 9),
(98, 23, 'страшным', 1),
(99, 24, 'Да', 20),
(100, 24, 'Нет', 3),
(101, 25, 'Да', 13),
(102, 25, 'Нет', 5),
(103, 25, 'Я не знаю что это', 4),
(105, 21, 'Другой', 1),
(114, 29, 'Я думаю что мы не одни', 9),
(115, 29, 'Во вселенной много обитаемых планет', 2),
(116, 29, '50 на 50', 2),
(117, 29, 'Наша планета единственная обитаемая', 0),
(118, 29, 'Я с другой планеты, отвечать отказываюсь', 2),
(119, 30, 'Карие', 9),
(120, 30, 'Черные', 1),
(121, 30, 'Голубые', 3),
(122, 30, 'Зелёные', 7),
(123, 30, 'Серые', 1),
(124, 30, 'Секрет...', 2),
(125, 31, 'Да, конечно', 5),
(126, 31, 'Иногда смотрю', 7),
(127, 31, 'Мне не нравится', 4),
(128, 31, 'Не знаю такого', 2),
(129, 32, 'Да-да', 6),
(130, 32, 'Зачем, не форум же', 9),
(132, 33, 'Чудесно, давно бы так :)', 8),
(133, 33, 'Таки неплохо', 4),
(134, 33, 'Ничего особого', 1),
(135, 34, 'На море', 11),
(136, 34, 'В деревне', 0),
(137, 34, 'На даче', 2),
(138, 34, 'Дома', 4),
(139, 34, 'В санатории', 0),
(140, 34, 'Не люблю отдых', 0),
(141, 35, 'Да', 6),
(142, 35, 'Да, обожаю!!', 10),
(143, 35, 'Нет. Просто не ем.', 1),
(144, 35, 'Нет конечно! Я вегетарианец(ка)!', 0),
(145, 35, 'Зубы уже не те...', 0),
(146, 36, 'Русская / Белорусская', 6),
(147, 36, 'Зарубежная', 13),
(148, 37, 'Kaspersky', 7),
(149, 37, 'Avast', 2),
(150, 37, 'Dr.Web', 3),
(151, 37, 'ESET NOD32', 5),
(152, 37, 'Norton', 0),
(153, 37, 'Panda', 0),
(154, 37, 'Другим', 0),
(155, 37, 'Нет антивируса', 1),
(156, 38, 'Все время', 4),
(157, 38, '3-4 часа', 2),
(158, 38, '2-3 часа', 0),
(159, 38, '1-2 часа', 3),
(160, 38, 'Меньше часа', 3),
(161, 38, 'Меньше 10 минут', 6);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_polluser`
--

CREATE TABLE IF NOT EXISTS `tbl_polluser` (
  `CODE` int(11) NOT NULL,
  `QUESTION_ID` int(11) DEFAULT NULL,
  `VOTE_ID` int(11) DEFAULT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `DATEVOTE` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=879 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_poster`
--

CREATE TABLE IF NOT EXISTS `tbl_poster` (
  `CODE` int(11) NOT NULL,
  `ISMODER` int(1) DEFAULT NULL,
  `BASECODE` int(11) DEFAULT NULL,
  `POSTER` char(200) DEFAULT NULL,
  `WHOADD` int(11) DEFAULT NULL,
  `DATEADD` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4631 DEFAULT CHARSET=cp1251 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_qb`
--

CREATE TABLE IF NOT EXISTS `tbl_qb` (
  `CODE` int(11) NOT NULL,
  `WHOADD` int(11) DEFAULT NULL,
  `DATEADD` datetime DEFAULT NULL,
  `OPIS` text
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_ratbonus`
--

CREATE TABLE IF NOT EXISTS `tbl_ratbonus` (
  `CODE` int(11) NOT NULL,
  `WHOADD` int(5) DEFAULT NULL,
  `USER` int(5) DEFAULT NULL,
  `DATEADD` datetime DEFAULT NULL,
  `RAT` int(11) DEFAULT NULL,
  `OPIS` text
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_search`
--

CREATE TABLE IF NOT EXISTS `tbl_search` (
  `CODE` int(11) NOT NULL,
  `USERCODE` int(11) DEFAULT NULL,
  `DATEADD` datetime DEFAULT NULL,
  `OPIS` varchar(200) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4936 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_shop`
--

CREATE TABLE IF NOT EXISTS `tbl_shop` (
  `CODE` int(11) NOT NULL,
  `SHOPCODE` int(11) DEFAULT NULL,
  `BASECODE` int(11) DEFAULT NULL,
  `SHOPLINK` varchar(250) DEFAULT NULL,
  `PRICECASH` varchar(40) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_stat`
--

CREATE TABLE IF NOT EXISTS `tbl_stat` (
  `CODE` int(11) NOT NULL,
  `USERCODE` int(5) DEFAULT NULL,
  `LINKCODE` int(11) DEFAULT NULL,
  `DATESTAT` datetime DEFAULT NULL,
  `USERIP` varchar(15) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=19107 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_stats`
--

CREATE TABLE IF NOT EXISTS `tbl_stats` (
  `PARAM` varchar(20) NOT NULL,
  `VALUE` int(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `tbl_stats`
--

INSERT INTO `tbl_stats` (`PARAM`, `VALUE`) VALUES
('all_razd', 0),
('all_links', 0),
('all_posters', 0),
('all_comments', 0),
('all_users', 0),
('all_persona', 0),
('all_news', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_subkat`
--

CREATE TABLE IF NOT EXISTS `tbl_subkat` (
  `CODE` int(5) NOT NULL,
  `KATCODE` int(11) DEFAULT '0',
  `NAZV` char(100) DEFAULT NULL,
  `POSITION` int(5) DEFAULT '1',
  `RAZD` int(11) DEFAULT '0',
  `MOTSK` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=445 DEFAULT CHARSET=cp1251 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_tags`
--

CREATE TABLE IF NOT EXISTS `tbl_tags` (
  `CODE` int(11) NOT NULL,
  `BASECODE` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=190 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_trouble`
--

CREATE TABLE IF NOT EXISTS `tbl_trouble` (
  `CODE` int(11) NOT NULL,
  `FROMCODE` int(11) DEFAULT NULL,
  `TOCODE` int(11) DEFAULT NULL,
  `OPIS` text COLLATE cp1251_ukrainian_ci,
  `BASECODE` int(11) DEFAULT NULL,
  `DATEADD` datetime DEFAULT NULL,
  `ISREAD` int(1) DEFAULT NULL,
  `THEME` text COLLATE cp1251_ukrainian_ci,
  `OTVET` int(250) DEFAULT '0',
  `SENDDEL` int(1) DEFAULT '0',
  `READDEL` int(1) DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=3351 DEFAULT CHARSET=cp1251 COLLATE=cp1251_ukrainian_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_tube`
--

CREATE TABLE IF NOT EXISTS `tbl_tube` (
  `CODE` int(11) NOT NULL,
  `BASECODE` int(11) DEFAULT NULL,
  `TYPERCODE` int(11) DEFAULT NULL,
  `LINKS` varchar(255) DEFAULT NULL,
  `RN` varchar(255) DEFAULT NULL,
  `NAZV` varchar(255) DEFAULT NULL,
  `ISMODER` int(1) DEFAULT NULL,
  `WHOADD` int(11) DEFAULT NULL,
  `DATEADD` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7361 DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `CODE` int(11) NOT NULL,
  `ISREGISTER` int(1) DEFAULT NULL,
  `LOGIN` char(40) DEFAULT NULL,
  `ISAVATAR` int(1) DEFAULT NULL,
  `POSTER` char(200) DEFAULT NULL,
  `PASS` char(200) DEFAULT NULL,
  `USERIP` char(15) DEFAULT NULL,
  `STATUS` int(1) DEFAULT '1',
  `DATEREG` datetime DEFAULT NULL,
  `LASTVISIT` datetime DEFAULT NULL,
  `LASTIP` char(15) DEFAULT NULL,
  `LASTNEWS` datetime DEFAULT NULL,
  `LASTNEWSCOMMENT` datetime DEFAULT NULL,
  `LASTNEWSPOSTER` datetime DEFAULT NULL,
  `MODERS` int(11) DEFAULT NULL,
  `RATINGS` int(11) DEFAULT NULL,
  `VOTES` int(11) DEFAULT '0',
  `LINKS` int(11) DEFAULT NULL,
  `DOWNUS` int(11) DEFAULT NULL,
  `DOWNHIM` int(11) DEFAULT NULL,
  `PREDUPR` int(1) DEFAULT NULL,
  `ISBAN` int(1) DEFAULT NULL,
  `WHOBAN` int(11) DEFAULT NULL,
  `WHYBAN` char(200) DEFAULT NULL,
  `BANADD` datetime DEFAULT NULL,
  `ISCOMMENT` int(1) DEFAULT NULL,
  `EMAIL` char(200) DEFAULT NULL,
  `HIDEEMAIL` int(1) NOT NULL DEFAULT '0',
  `ISVALIDEMAIL` int(1) DEFAULT NULL,
  `ISAVATARMODER` int(1) DEFAULT NULL,
  `BANEND` datetime DEFAULT NULL,
  `BADLINK` int(11) DEFAULT NULL,
  `COLOR` char(30) DEFAULT NULL,
  `PERSONA` int(11) DEFAULT '0',
  `PERSONAPHOTO` int(11) NOT NULL DEFAULT '0',
  `LASTNEWSPERSONA` datetime DEFAULT NULL,
  `LASTNEWSPERSONAPHOTO` datetime DEFAULT NULL,
  `ISCHAT` int(1) NOT NULL DEFAULT '1',
  `CHATUPD` int(3) DEFAULT NULL,
  `ALTLINKS` int(11) DEFAULT '0',
  `ICQ` varchar(15) DEFAULT NULL,
  `jabber` varchar(50) DEFAULT NULL,
  `skype` varchar(50) DEFAULT NULL,
  `site` varchar(60) DEFAULT NULL,
  `adress` varchar(50) DEFAULT NULL,
  `mainpage` int(1) NOT NULL DEFAULT '0',
  `OS` varchar(250) DEFAULT NULL,
  `BROWS` varchar(250) DEFAULT NULL,
  `vk` varchar(50) DEFAULT NULL,
  `mymr` varchar(50) DEFAULT NULL,
  `facebook` varchar(50) DEFAULT NULL,
  `name` varchar(20) DEFAULT NULL,
  `surname` varchar(20) DEFAULT NULL,
  `sex` int(1) DEFAULT NULL,
  `ref` int(20) DEFAULT '0',
  `refusers` int(20) DEFAULT '0',
  `birthday` date DEFAULT NULL,
  `odn` varchar(50) DEFAULT NULL,
  `twitter` varchar(50) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=996 DEFAULT CHARSET=cp1251 ROW_FORMAT=FIXED;

--
-- Дамп данных таблицы `tbl_user`
--

INSERT INTO `tbl_user` (`CODE`, `ISREGISTER`, `LOGIN`, `ISAVATAR`, `POSTER`, `PASS`, `USERIP`, `STATUS`, `DATEREG`, `LASTVISIT`, `LASTIP`, `LASTNEWS`, `LASTNEWSCOMMENT`, `LASTNEWSPOSTER`, `MODERS`, `RATINGS`, `VOTES`, `LINKS`, `DOWNUS`, `DOWNHIM`, `PREDUPR`, `ISBAN`, `WHOBAN`, `WHYBAN`, `BANADD`, `ISCOMMENT`, `EMAIL`, `HIDEEMAIL`, `ISVALIDEMAIL`, `ISAVATARMODER`, `BANEND`, `BADLINK`, `COLOR`, `PERSONA`, `PERSONAPHOTO`, `LASTNEWSPERSONA`, `LASTNEWSPERSONAPHOTO`, `ISCHAT`, `CHATUPD`, `ALTLINKS`, `ICQ`, `jabber`, `skype`, `site`, `adress`, `mainpage`, `OS`, `BROWS`, `vk`, `mymr`, `facebook`, `name`, `surname`, `sex`, `ref`, `refusers`, `birthday`, `odn`, `twitter`) VALUES
(2, 1, 'Admin', 1, '', '81dc9bdb52d04dc20036dbd8313ed055', '10.23.4.195', 5, '2009-07-30 10:46:56', '2012-10-21 13:23:02', '10.23.4.205', '2012-02-15 18:28:20', '2012-05-03 12:40:20', '2012-02-15 19:52:38', 630, 37227, 775, 1171, 7210, 316, 0, 0, NULL, NULL, NULL, 1, 'admin@test.local', 0, 1, NULL, NULL, NULL, 'green', 729, 112, '2011-08-10 21:31:51', '2011-05-28 19:16:30', 1, 60, 545, '', '', '', '', '', 0, 'Windows 7', 'Opera 12.02', '', '', '', '', '', 2, 0, 0, '0000-00-00', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_user_rang`
--

CREATE TABLE IF NOT EXISTS `tbl_user_rang` (
  `CODE` int(11) NOT NULL,
  `COUNTSUM` int(11) DEFAULT NULL,
  `RANG` char(200) COLLATE cp1251_ukrainian_ci DEFAULT NULL,
  `STARCOLOR` char(200) COLLATE cp1251_ukrainian_ci DEFAULT NULL,
  `STARCOEF` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=cp1251 COLLATE=cp1251_ukrainian_ci ROW_FORMAT=FIXED;

--
-- Дамп данных таблицы `tbl_user_rang`
--

INSERT INTO `tbl_user_rang` (`CODE`, `COUNTSUM`, `RANG`, `STARCOLOR`, `STARCOEF`) VALUES
(1, 0, 'Новичок', 'rating_1.png', 10),
(2, 50, 'Любитель', 'rating_2.png', 10),
(3, 100, 'Опытный', 'rating_3.png', 25),
(4, 250, 'Знаток', 'rating_4.png', 50),
(5, 500, 'Старожил', 'rating_5.png', 75),
(6, 850, 'Ветеран', 'rating_6.png', 100),
(7, 1500, 'Мастер', 'rating_7.png', 150),
(8, 2100, 'Профессионал', 'rating_8.png', 200),
(9, 3100, 'Гуру', 'rating_9.png', 250);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_user_status`
--

CREATE TABLE IF NOT EXISTS `tbl_user_status` (
  `CODE` int(11) NOT NULL,
  `NAZV` varchar(200) DEFAULT NULL,
  `COLOR` varchar(10) NOT NULL DEFAULT '0',
  `DO_SET` int(1) DEFAULT NULL,
  `DO_KAT` int(1) DEFAULT NULL,
  `DO_USER` int(1) DEFAULT NULL,
  `DO_MODER` int(1) DEFAULT NULL,
  `DO_PERS` int(1) DEFAULT NULL,
  `DO_ADD` int(1) DEFAULT NULL,
  `DO_VERADD` int(1) DEFAULT NULL,
  `DO_MODERATOR` int(1) DEFAULT NULL,
  `DO_NEWS` int(1) DEFAULT NULL,
  `DO_DELSVOI` int(1) DEFAULT NULL,
  `DO_DELRAZD` int(1) DEFAULT NULL,
  `DO_DELSUBKAT` int(1) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `tbl_user_status`
--

INSERT INTO `tbl_user_status` (`CODE`, `NAZV`, `COLOR`, `DO_SET`, `DO_KAT`, `DO_USER`, `DO_MODER`, `DO_PERS`, `DO_ADD`, `DO_VERADD`, `DO_MODERATOR`, `DO_NEWS`, `DO_DELSVOI`, `DO_DELRAZD`, `DO_DELSUBKAT`) VALUES
(1, 'Пользователь', '0', 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0),
(6, 'Супермодератор', '0000FF', 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0),
(3, 'Модератор', '008000', 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0),
(4, 'Администратор', 'FFA500', 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0),
(5, 'Администратор системы', '#FF0000', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(2, 'V.I.P. персона', 'FFFF00', 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_video`
--

CREATE TABLE IF NOT EXISTS `tbl_video` (
  `CODE` int(11) NOT NULL,
  `NAZV` char(100) DEFAULT NULL,
  `OPIS` char(200) DEFAULT NULL,
  `FILE` char(100) DEFAULT NULL,
  `WHOADD` int(11) DEFAULT NULL,
  `DATEADD` datetime DEFAULT NULL,
  `TIME` time DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=cp1251 ROW_FORMAT=FIXED;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_votes`
--

CREATE TABLE IF NOT EXISTS `tbl_votes` (
  `CODE` int(11) NOT NULL,
  `USERCODE` int(11) DEFAULT NULL,
  `BASECODE` int(11) DEFAULT NULL,
  `VOTES` int(1) DEFAULT NULL,
  `DATEVOTES` datetime DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3889 DEFAULT CHARSET=cp1251 COLLATE=cp1251_ukrainian_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_wanted`
--

CREATE TABLE IF NOT EXISTS `tbl_wanted` (
  `CODE` int(11) NOT NULL,
  `PRIMARYCODE` int(11) DEFAULT NULL,
  `KATCODE` int(11) DEFAULT NULL,
  `WHOADD` int(11) DEFAULT NULL,
  `DATEADD` datetime DEFAULT NULL,
  `NAZV` varchar(255) DEFAULT NULL,
  `ISDO` int(1) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=cp1251;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `tblf_comment`
--
ALTER TABLE `tblf_comment`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tblf_kat`
--
ALTER TABLE `tblf_kat`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tblf_post`
--
ALTER TABLE `tblf_post`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tblf_subkat`
--
ALTER TABLE `tblf_subkat`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_banip`
--
ALTER TABLE `tbl_banip`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_base`
--
ALTER TABLE `tbl_base`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_bots`
--
ALTER TABLE `tbl_bots`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_botslog`
--
ALTER TABLE `tbl_botslog`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_chat`
--
ALTER TABLE `tbl_chat`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_comment`
--
ALTER TABLE `tbl_comment`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_conf`
--
ALTER TABLE `tbl_conf`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_conf_dorab`
--
ALTER TABLE `tbl_conf_dorab`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_conf_shop`
--
ALTER TABLE `tbl_conf_shop`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_cross`
--
ALTER TABLE `tbl_cross`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_cross2`
--
ALTER TABLE `tbl_cross2`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_dopf`
--
ALTER TABLE `tbl_dopf`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_fav`
--
ALTER TABLE `tbl_fav`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_friends`
--
ALTER TABLE `tbl_friends`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_hublist`
--
ALTER TABLE `tbl_hublist`
  ADD PRIMARY KEY (`CODE`),
  ADD UNIQUE KEY `ADDRESS` (`ADDR`);

--
-- Индексы таблицы `tbl_kat`
--
ALTER TABLE `tbl_kat`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_link`
--
ALTER TABLE `tbl_link`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_logmoder`
--
ALTER TABLE `tbl_logmoder`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_menulinks`
--
ALTER TABLE `tbl_menulinks`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_modernote`
--
ALTER TABLE `tbl_modernote`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_news`
--
ALTER TABLE `tbl_news`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_news_comment`
--
ALTER TABLE `tbl_news_comment`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_page`
--
ALTER TABLE `tbl_page`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_persona`
--
ALTER TABLE `tbl_persona`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_persona_photo`
--
ALTER TABLE `tbl_persona_photo`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_poll`
--
ALTER TABLE `tbl_poll`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_polldata`
--
ALTER TABLE `tbl_polldata`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_polluser`
--
ALTER TABLE `tbl_polluser`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_poster`
--
ALTER TABLE `tbl_poster`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_qb`
--
ALTER TABLE `tbl_qb`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_ratbonus`
--
ALTER TABLE `tbl_ratbonus`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_search`
--
ALTER TABLE `tbl_search`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_shop`
--
ALTER TABLE `tbl_shop`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_stat`
--
ALTER TABLE `tbl_stat`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_stats`
--
ALTER TABLE `tbl_stats`
  ADD PRIMARY KEY (`PARAM`);

--
-- Индексы таблицы `tbl_subkat`
--
ALTER TABLE `tbl_subkat`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_tags`
--
ALTER TABLE `tbl_tags`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_trouble`
--
ALTER TABLE `tbl_trouble`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_tube`
--
ALTER TABLE `tbl_tube`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_user_rang`
--
ALTER TABLE `tbl_user_rang`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_user_status`
--
ALTER TABLE `tbl_user_status`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_video`
--
ALTER TABLE `tbl_video`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_votes`
--
ALTER TABLE `tbl_votes`
  ADD PRIMARY KEY (`CODE`);

--
-- Индексы таблицы `tbl_wanted`
--
ALTER TABLE `tbl_wanted`
  ADD PRIMARY KEY (`CODE`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `tblf_comment`
--
ALTER TABLE `tblf_comment`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `tblf_kat`
--
ALTER TABLE `tblf_kat`
  MODIFY `CODE` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `tblf_post`
--
ALTER TABLE `tblf_post`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `tblf_subkat`
--
ALTER TABLE `tblf_subkat`
  MODIFY `CODE` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `tbl_banip`
--
ALTER TABLE `tbl_banip`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT для таблицы `tbl_base`
--
ALTER TABLE `tbl_base`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5006;
--
-- AUTO_INCREMENT для таблицы `tbl_bots`
--
ALTER TABLE `tbl_bots`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT для таблицы `tbl_botslog`
--
ALTER TABLE `tbl_botslog`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `tbl_chat`
--
ALTER TABLE `tbl_chat`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1003;
--
-- AUTO_INCREMENT для таблицы `tbl_comment`
--
ALTER TABLE `tbl_comment`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1735;
--
-- AUTO_INCREMENT для таблицы `tbl_conf`
--
ALTER TABLE `tbl_conf`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=247;
--
-- AUTO_INCREMENT для таблицы `tbl_conf_dorab`
--
ALTER TABLE `tbl_conf_dorab`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `tbl_conf_shop`
--
ALTER TABLE `tbl_conf_shop`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `tbl_cross`
--
ALTER TABLE `tbl_cross`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1065;
--
-- AUTO_INCREMENT для таблицы `tbl_cross2`
--
ALTER TABLE `tbl_cross2`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4630;
--
-- AUTO_INCREMENT для таблицы `tbl_dopf`
--
ALTER TABLE `tbl_dopf`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `tbl_fav`
--
ALTER TABLE `tbl_fav`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=190;
--
-- AUTO_INCREMENT для таблицы `tbl_friends`
--
ALTER TABLE `tbl_friends`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT для таблицы `tbl_hublist`
--
ALTER TABLE `tbl_hublist`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `tbl_kat`
--
ALTER TABLE `tbl_kat`
  MODIFY `CODE` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT для таблицы `tbl_link`
--
ALTER TABLE `tbl_link`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21119;
--
-- AUTO_INCREMENT для таблицы `tbl_logmoder`
--
ALTER TABLE `tbl_logmoder`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4018;
--
-- AUTO_INCREMENT для таблицы `tbl_menulinks`
--
ALTER TABLE `tbl_menulinks`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT для таблицы `tbl_modernote`
--
ALTER TABLE `tbl_modernote`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=316;
--
-- AUTO_INCREMENT для таблицы `tbl_news`
--
ALTER TABLE `tbl_news`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT для таблицы `tbl_news_comment`
--
ALTER TABLE `tbl_news_comment`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `tbl_page`
--
ALTER TABLE `tbl_page`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT для таблицы `tbl_persona`
--
ALTER TABLE `tbl_persona`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1160;
--
-- AUTO_INCREMENT для таблицы `tbl_persona_photo`
--
ALTER TABLE `tbl_persona_photo`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=452;
--
-- AUTO_INCREMENT для таблицы `tbl_poll`
--
ALTER TABLE `tbl_poll`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT для таблицы `tbl_polldata`
--
ALTER TABLE `tbl_polldata`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=162;
--
-- AUTO_INCREMENT для таблицы `tbl_polluser`
--
ALTER TABLE `tbl_polluser`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=879;
--
-- AUTO_INCREMENT для таблицы `tbl_poster`
--
ALTER TABLE `tbl_poster`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4631;
--
-- AUTO_INCREMENT для таблицы `tbl_qb`
--
ALTER TABLE `tbl_qb`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT для таблицы `tbl_ratbonus`
--
ALTER TABLE `tbl_ratbonus`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `tbl_search`
--
ALTER TABLE `tbl_search`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4936;
--
-- AUTO_INCREMENT для таблицы `tbl_shop`
--
ALTER TABLE `tbl_shop`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `tbl_stat`
--
ALTER TABLE `tbl_stat`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19107;
--
-- AUTO_INCREMENT для таблицы `tbl_subkat`
--
ALTER TABLE `tbl_subkat`
  MODIFY `CODE` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=445;
--
-- AUTO_INCREMENT для таблицы `tbl_tags`
--
ALTER TABLE `tbl_tags`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=190;
--
-- AUTO_INCREMENT для таблицы `tbl_trouble`
--
ALTER TABLE `tbl_trouble`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3351;
--
-- AUTO_INCREMENT для таблицы `tbl_tube`
--
ALTER TABLE `tbl_tube`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7361;
--
-- AUTO_INCREMENT для таблицы `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=996;
--
-- AUTO_INCREMENT для таблицы `tbl_user_rang`
--
ALTER TABLE `tbl_user_rang`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `tbl_user_status`
--
ALTER TABLE `tbl_user_status`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `tbl_video`
--
ALTER TABLE `tbl_video`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT для таблицы `tbl_votes`
--
ALTER TABLE `tbl_votes`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3889;
--
-- AUTO_INCREMENT для таблицы `tbl_wanted`
--
ALTER TABLE `tbl_wanted`
  MODIFY `CODE` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
